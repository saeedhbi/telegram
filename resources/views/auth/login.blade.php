<html>
<head>
    <title>Login</title>
</head>
<body>

<form class="form-horizontal" action="{{route('login-post')}}" method="post">
    {{csrf_field()}}
<h1>Login</h1>

<!-- if there are login errors, show them here -->
<p>
    {{ $errors->first('email') }}
    {{ $errors->first('password') }}
</p>

<p>
    <input type="email" name="email" placeholder="awesome@awesome.com" value="{{request()->input('email')}}">
</p>

<p>
    <input type="password" name="password" placeholder="password" >
</p>

<p>
    <button class="btn btn-primary">Login</button>
</p>
</form>