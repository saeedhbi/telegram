@extends('v1.panel.layout')

@section('title')
    Panel | Gateways | New Gateway
@endsection

@section('content')
    <div class="col-sm-12">
        <form class="form-horizontal" action="{{route('panel-gateways-new-post')}}" method="POST">
            {{csrf_field()}}
            <div class="form-group">
                <div class="col-sm-4">
                    <label for="name">Name:</label>
                    <input class="form-control" name="name" id="name" value="{{old('name')}}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-4">
                    <label for="type">Type:</label>
                    <div id="type">
                        <select name="type" class="form-control" value="{{old('type')}}">
                            <option value="{{\Boloosh\Infrastructures\Models\Gateway::HOOK}}">{{\Boloosh\Infrastructures\Models\Gateway::HOOK}}</option>
                            <option value="{{\Boloosh\Infrastructures\Models\Gateway::REQUEST}}">{{\Boloosh\Infrastructures\Models\Gateway::REQUEST}}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-4">
                    <label for="type">Service:</label>
                    <div id="type">
                        <select name="service" class="form-control" value="{{old('service')}}">
                            @foreach($gatewayServices as $service)
                                <option value="{{$service->id}}">{{$service->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-4">
                    <input name="queue" id="queue" type="checkbox" value="{{true}}" value="{{old('queue')}}">
                    <label for="queue">Queue</label>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-4">
                    <label for="delay">Delay:</label>
                    <input class="form-control" name="delay" id="delay" value="{{old('delay')}}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-4">
                    <label for="token">Token:</label>
                    <input class="form-control" name="token" id="token" value="{{old('token')}}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-4">
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </div>
        </form>
    </div>
@endsection