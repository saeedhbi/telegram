@extends('v1.panel.layout')

@section('title')
    Panel | Gateways
@endsection

@section('content')
    <a href="{{route('panel-gateways-new')}}" class="btn btn-primary">New gateway</a>
    <table class="table table-responsive">
        <thead>
        <tr>
            <td>ID</td>
            <td>Name</td>
            <td>Type</td>
            <td>Service</td>
            <td>Queue</td>
            <td>Delay</td>
            <td>Status</td>
            <td>action</td>
        </tr>
        </thead>
        <tbody>
        @foreach($gateways as $gateway)
            <tr>
                <td>{{$gateway->id}}</td>
                <td>{{$gateway->name}}</td>
                <td>{{$gateway->type}}</td>
                <td>{{$gateway->service->name}}</td>
                <td>{{$gateway->queue ? 'Yes' : 'No'}}</td>
                <td>{{$gateway->delay}}</td>
                <td>@if($gateway->status == \Boloosh\Infrastructures\Models\Gateway::ACTIVE) <i class="fa fa-check"></i> @else <i class="fa fa-close"></i>@endif</td>
                <td>@if($gateway->status == \Boloosh\Infrastructures\Models\Gateway::ACTIVE)<a class="btn btn-sm btn-warning" href="{{route('panel-gateways-disable', ['id' => $gateway->id])}}">Disable</a>@else
                        <a class="btn btn-sm btn-primary" href="{{route('panel-gateways-enable', ['id' => $gateway->id])}}">Enable</a> @endif
                    <a class="btn btn-sm btn-danger" href="{{route('panel-gateways-delete', ['id' => $gateway->id])}}">Delete</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection