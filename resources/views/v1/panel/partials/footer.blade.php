<!-- To the right -->
<div class="pull-right hidden-xs">
    Telegram
</div>
<!-- Default to the left -->
<strong>Copyright &copy; {!! date('Y') !!} <a href="#">Boloosh</a>.</strong> All rights reserved.