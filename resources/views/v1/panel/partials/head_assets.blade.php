<!-- Twitter Bootstrap CSS -->
<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}"/>

<!-- Font Awesome CSS -->
<link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}"/>

<!-- Admin LTE CSS -->
<link rel="stylesheet"
      href="{{ asset('vendor/' . \HieuLe\LaravelDashboard\Dashboard::PLUGIN_NAME . '/dist/css/AdminLTE.min.css') }}"/>

<!-- Admin LTE skin CSS -->
<link rel="stylesheet"
      href="{{ asset('vendor/' . \HieuLe\LaravelDashboard\Dashboard::PLUGIN_NAME . '/dist/css/skins/skin-' . (app(\HieuLe\LaravelDashboard\Dashboard::PLUGIN_NAME)->getSkin()) . '.css') }}"/>

<!-- Modernizr script -->
<script src="{{asset('js/modernizr.min.js')}}"></script>