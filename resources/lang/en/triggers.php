<?php

use Carbon\Carbon;

return [
    \Boloosh\Infrastructures\Models\GatewayTriggerType::PICK_GATEWAY_TRIGGER => 'Please select workflow.',
    \Boloosh\Infrastructures\Models\GatewayTriggerType::SET_CANCEL => ':request are canceled right now.',
    \Boloosh\Infrastructures\Models\GatewayTriggerType::GET_CAPTION => 'Please type a caption.',
    \Boloosh\Infrastructures\Models\GatewayTriggerType::GET_SIGN => 'Please type a sign.',
    \Boloosh\Infrastructures\Models\GatewayTriggerType::GET_TIME => 'Select postpone time. Example: <pre>' . Carbon::now()->tz('Asia/Tehran')->format('Y-m-d H:i:s') . '</pre>',
    \Boloosh\Infrastructures\Models\GatewayTriggerType::GET_DELETE_TIME => 'Select delete time. Example: <pre>' . Carbon::now()->tz('Asia/Tehran')->format('Y-m-d H:i:s') . '</pre>',
    \Boloosh\Infrastructures\Models\GatewayTriggerType::GET_BUTTON_TITLE => 'Please type button title',
    \Boloosh\Infrastructures\Models\GatewayTriggerType::GET_BUTTON_LINK => 'Please type button link. Example: <pre>https://www.boloosh.com</pre>',
    \Boloosh\Infrastructures\Models\GatewayTriggerType::GET_AUDIO_CUT_START => 'Please type start of the audio in seconds. Minimum seconds is :minimum. Example: <pre>25</pre>',
    \Boloosh\Infrastructures\Models\GatewayTriggerType::GET_AUDIO_CUT_DURATION => 'Please type duration of the audio in seconds. Minimum seconds is :minimum. Example: <pre>60</pre>',
    \Boloosh\Infrastructures\Models\GatewayTriggerType::GET_AUDIO_PERFORM_TITLE => 'Please type track title',
    \Boloosh\Infrastructures\Models\GatewayTriggerType::GET_AUDIO_PERFORM_PERFORMER => 'Please type track performer',
    \Boloosh\Infrastructures\Models\GatewayTriggerType::GET_AUDIO_COVER => 'Please send audio cover.',
    \Boloosh\Infrastructures\Models\GatewayTriggerType::GET_ATTACH_UPDATE => 'Please send link based on your request.',
    \Boloosh\Infrastructures\Models\GatewayTriggerType::GET_DESTINATION => 'Please send your destination.',
    'failed' => [
        'restricted_type' => 'Can not set your request on this message. Message is canceled right now.',
        \Boloosh\Infrastructures\Models\GatewayTriggerType::SET_CANCEL => 'Can not cancel :request right now. Check your request and try later.',
    ],
];

