<?php

return [
    'all'            => 'All',
    'your_requests'  => 'your requests',
    'your_request'   => 'your request #:request',
    'new_request'    => "Your request id is <b>:requestId</b>. Send this command to cancel it: \n /cancel_:requestId \n\n If you want to cancel all your requests, Send this command:\n /cancel",
    'ending_request' => "Request sent to queue.",

    'gateway_service_update' => [
        \Boloosh\Infrastructures\Models\GatewayServiceUpdate::LOCKED => 'You should send data to save your last request. If you don\'t  want to continue please cancel request.'
    ]
];

