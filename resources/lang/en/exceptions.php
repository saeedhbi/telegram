<?php

return [
    'not_valid' => 'Please send valid input. <pre>:sample</pre>',
    'no_trigger' => 'You defined no workflow. You can create new workflow by <pre>/new_workflow</pre>.'
];

