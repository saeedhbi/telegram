<?php

namespace App\Console\Commands;

use Boloosh\Services\SDK\Twitter\TwitterStream;
use Illuminate\Console\Command;

class ConnectToStreamingAPI extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'connect_to_streaming_api';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Connect to the Twitter Streaming API';

    protected $twitterStream;


    /**
     * Create a new command instance.
     *
     * @param TwitterStream $twitterStream
     */
    public function __construct(TwitterStream $twitterStream)
    {
        $this->twitterStream = $twitterStream;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->twitterStream->consumerKey = env('TWITTER_CONSUMER_KEY');
        $this->twitterStream->consumerSecret = env('TWITTER_CONSUMER_SECRET');
        $this->twitterStream->setTrack(array('sepiiidegh'));
        $this->twitterStream->consume();
    }
}
