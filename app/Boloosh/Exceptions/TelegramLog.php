<?php

namespace Boloosh\Exceptions;

use Boloosh\Services\SDK\Telegram\ITelegramService;
use Illuminate\Support\Facades\Log;

class TelegramLog
{
    /**
     * @var null
     */
    private $data;

    /**
     * TelegramException constructor.
     * @param null $data
     */
    public function __construct($data)
    {
        $this->data = $data;
        $this->createTelegramLog();
    }

    private function createTelegramLog()
    {
        if (!config('boloosh.log.telegram.log')) {
            return false;
        }

        /** @var ITelegramService $telegramService */
        $telegramService = app(ITelegramService::class);

        $telegramService->make(config('boloosh.log.telegram.token'));

        $options = [
            'chat_id' => config('boloosh.log.telegram.destination'),
            'disable_notification' => true,
            'parse_mode' => 'Markdown',
            'text' => '*' .$this->data['exception'] . '*

_' . $this->data['message'] . '_
            ```' . $this->data['request']. '```'
        ];

        $telegramService->hideKeyboard();

        try {
            return $telegramService->sendMessage($options);
        } catch (\Exception $ex) {
            logger($ex->getMessage());
        }
    }
}