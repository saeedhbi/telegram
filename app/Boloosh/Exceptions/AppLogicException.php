<?php

namespace Boloosh\Exceptions;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;

class AppLogicException extends \LogicException
{

    /*
     * @var MessageBag
     */
    protected $_errors;

    protected $_params = [ ];

    protected $defaultMessage = null;

    protected $data = null;

    /**
     * DomainLogicException constructor.
     *
     * @param null $message
     * @param null $line
     * @param null $errors
     * @param Exception|null $previous
     * @param null $data
     */
    public function __construct($message = null, $line = null, $errors = null, Exception $previous = null, $data = null)
    {
        $code = isset( $this->code ) ? $this->code : null;

        $this->_set_errors($errors);

        if (is_null($message) && ! is_null($this->defaultMessage)) {
            $message = $this->defaultMessage;
        }

        if ( ! is_null($previous)) {
            $log_errors[] = [ 'DomainLogic Error : ' . get_called_class() . ' - ' . Carbon::now()->toDateTimeString() . ' | ' . $previous->getMessage() . ' inFile: ' . $previous->getFile() . ':' . $previous->getLine() ];
        }

        $log_errors[] = [ 'DomainLogic Error : ' . get_called_class() . ' - ' . Carbon::now()->toDateTimeString() . ' | ' . $this->getMessage() . ' inFile: ' . $this->getFile() . ':' . $this->getLine() ];

        if (env('DOMAIN_LOGIC_EXCEPTION_LOG')) {
            if ( ! isset( $log_errors )) {
                $log_errors = [ ];
            }

            if ( ! is_null($errors)) {
                array_push($log_errors, $errors);
            }

            Log::error($log_errors);
        }

        if(!is_null($data)) {
            $this->data = $data;
        }

        if(!is_null($line)) {
            $this->line = $line;
        }

        if(env('DOMAIN_LOGIC_EXCEPTION_LOG') && is_null($this->getPrevious())) {
            $data = [
                'exception' => $this->getFile() . '
                
' . (is_null($previous) ? '' : $previous->getFile()) . ' ' . $this->getLine(),
                'message' => is_null($message) ? $this->getMessage() : $message,
                'request' => is_null($this->data) ? '' : $this->data
            ];

            new TelegramLog($data);
        }

        parent::__construct($message, $code, $previous);
    }


    /**
     * @param $errors
     */
    protected function _set_errors($errors)
    {
        if (is_string($errors)) {
            $errors = [
                'errors' => $errors,
            ];
        }

        if (is_array($errors)) {
            $errors = new MessageBag($errors);
        }

        if (is_null($errors)) {
            $errors = new MessageBag();
        }

        $this->_errors = $errors;
    }


    /**
     * @param $key
     * @param $message
     */
    public function addError($key, $message)
    {
        $this->_errors->add($key, $message);
    }


    /**
     * @return string
     */
    public function getTransMessage()
    {
        return trans($this->message);
    }


    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->_errors;
    }


    /**
     * @return array
     */
    protected function getTransErrors()
    {
        if (is_null($this->_errors)) {
            return null;
        }

        $translatedErrors = [ ];
        foreach ($this->_errors->toArray() as $key => $errors) {
            foreach ($errors as $error) {
                $translatedErrors[$key][] = trans($error);
            }
        }
        if (empty( $translatedErrors )) {
            return null;
        }

        return $translatedErrors;
    }


    /**
     * @return array
     */
    public function getFailResponse()
    {
        return [ 'message' => $this->getTransMessage(), 'errors' => $this->getTransErrors() ];
    }
}