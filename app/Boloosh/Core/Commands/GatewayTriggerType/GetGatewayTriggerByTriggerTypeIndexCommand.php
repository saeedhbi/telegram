<?php

namespace Boloosh\Core\Commands\GatewayTriggerType;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerTypeRepository;
use Boloosh\Infrastructures\Models\GatewayTriggerIndexes;
use Boloosh\Infrastructures\Models\GatewayTriggerType;

class GetGatewayTriggerByTriggerTypeIndexCommand extends BaseCommand
{
    /**
     * @var
     */
    private $gatewayTriggerTypeIndex;

    /**
     * GetGatewayTriggerByTriggerTypeIndexCommand constructor.
     * @param $gatewayTriggerTypeIndex
     */
    public function __construct($gatewayTriggerTypeIndex)
    {

        $this->gatewayTriggerTypeIndex = $gatewayTriggerTypeIndex;
    }

    /**
     * @param IGatewayTriggerTypeRepository $gatewayTriggerTypeRepository
     *
     * @return GatewayTriggerType
     */
    public function handle(IGatewayTriggerTypeRepository $gatewayTriggerTypeRepository)
    {
        try {
            $gatewayTriggerTypeId = (new GatewayTriggerIndexes())->getTriggerType($this->gatewayTriggerTypeIndex);

            return $gatewayTriggerTypeRepository->find($gatewayTriggerTypeId);
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}