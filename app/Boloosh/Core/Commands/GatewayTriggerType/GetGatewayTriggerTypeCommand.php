<?php

namespace Boloosh\Core\Commands\GatewayTriggerType;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\InvalidRequestException;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerTypeRepository;
use Boloosh\Infrastructures\Models\GatewayTrigger;
use Boloosh\Infrastructures\Models\GatewayTriggerType;

class GetGatewayTriggerTypeCommand extends BaseCommand
{

    /**
     * @var
     */
    private $gatewayTriggerTypeId;

    /**
     * GetGatewayTriggerTypeCommand constructor.
     *
     * @param $gatewayTriggerTypeId
     */
    public function __construct($gatewayTriggerTypeId)
    {

        $this->gatewayTriggerTypeId = $gatewayTriggerTypeId;
    }


    /**
     * @param IGatewayTriggerTypeRepository $gatewayTriggerTypeRepository
     *
     * @return GatewayTriggerType
     */
    public function handle(IGatewayTriggerTypeRepository $gatewayTriggerTypeRepository)
    {
        try {
            return $gatewayTriggerTypeRepository->findWhere([ 'id' => $this->gatewayTriggerTypeId ])->first();
        } catch (\Exception $ex) {
            throw new InvalidRequestException;
        }
    }

}