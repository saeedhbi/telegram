<?php

namespace Boloosh\Core\Commands\GatewayTriggerType;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Exceptions\RestrictedGatewayTriggerTypeException;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTriggerType;

class CheckCanTriggerGatewayServiceUpdateCommand extends BaseCommand
{
    /**
     * @var GatewayTriggerType
     */
    private $gatewayTriggerType;
    /**
     * @var GatewayServiceUpdate
     */
    private $gatewayServiceUpdate;

    /**
     * CheckCanTriggerGatewayServiceUpdateCommand constructor.
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     * @param GatewayTriggerType $gatewayTriggerType
     */
    public function __construct(GatewayServiceUpdate $gatewayServiceUpdate, GatewayTriggerType $gatewayTriggerType)
    {

        $this->gatewayTriggerType = $gatewayTriggerType;
        $this->gatewayServiceUpdate = $gatewayServiceUpdate;
    }

    public function handle()
    {
        try {
            if ($this->gatewayServiceUpdate->type == GatewayServiceUpdate::TEXT) {
                if (!in_array($this->gatewayTriggerType->id, [GatewayTriggerType::DESTINATION, GatewayTriggerType::SIGN, GatewayTriggerType::TIME, GatewayTriggerType::DELETE_TIME, GatewayTriggerType::BUTTON, GatewayTriggerType::ATTACH_UPDATE])) {
                    throw new RestrictedGatewayTriggerTypeException;
                }
            }

            if ($this->gatewayServiceUpdate->type == GatewayServiceUpdate::PHOTO) {
                if (!in_array($this->gatewayTriggerType->id, [GatewayTriggerType::DESTINATION, GatewayTriggerType::CAPTION, GatewayTriggerType::SIGN, GatewayTriggerType::TIME, GatewayTriggerType::DELETE_TIME, GatewayTriggerType::BUTTON, GatewayTriggerType::ATTACH_UPDATE])) {
                    throw new RestrictedGatewayTriggerTypeException;
                }
            }

            if ($this->gatewayServiceUpdate->type == GatewayServiceUpdate::AUDIO) {
                if (!in_array($this->gatewayTriggerType->id, [GatewayTriggerType::DESTINATION, GatewayTriggerType::CAPTION, GatewayTriggerType::SIGN, GatewayTriggerType::TIME, GatewayTriggerType::DELETE_TIME, GatewayTriggerType::BUTTON, GatewayTriggerType::ATTACH_UPDATE, GatewayTriggerType::AUDIO_CUT, GatewayTriggerType::AUDIO_PERFORM, GatewayTriggerType::AUDIO_COVER])) {
                    throw new RestrictedGatewayTriggerTypeException;
                }
            }
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}