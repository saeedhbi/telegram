<?php

namespace Boloosh\Core\Commands\GatewayServiceUpdate;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Interfaces\IGatewayServiceUpdateRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerIndexRepository;
use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Infrastructures\Models\GatewayService;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTriggerIndex;
use Boloosh\Services\Gateway\GatewayServiceManager;
use Boloosh\Services\Gateway\Services\IGatewayService;
use Illuminate\Support\Facades\DB;

class HandleLockedGatewayServiceUpdate extends BaseCommand
{

    /**
     * @var GatewayServiceUpdate
     */
    private $gatewayServiceUpdate;

    /**
     * @var
     */
    private $request;

    /**
     * @var GatewayService
     */
    private $gatewayService;

    /**
     * @var Gateway
     */
    private $gateway;

    /**
     * HandleLockedGatewayServiceUpdate constructor.
     *
     * @param Gateway $gateway
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     * @param GatewayService $gatewayService
     * @param                      $request
     */
    public function __construct(Gateway $gateway, GatewayServiceUpdate $gatewayServiceUpdate, GatewayService $gatewayService, $request)
    {

        $this->gateway = $gateway;
        $this->gatewayService = $gatewayService;
        $this->gatewayServiceUpdate = $gatewayServiceUpdate;
        $this->request = $request;
    }

    /**
     * @param GatewayServiceManager $gatewayServiceManager
     * @param IGatewayServiceUpdateRepository $gatewayServiceUpdateRepository
     * @param IGatewayTriggerIndexRepository $gatewayTriggerResultRepository
     *
     * @return mixed
     */
    public function handle(
        GatewayServiceManager $gatewayServiceManager,
        IGatewayServiceUpdateRepository $gatewayServiceUpdateRepository,
        IGatewayTriggerIndexRepository $gatewayTriggerResultRepository
    )
    {
        try {
            /** @var IGatewayService $gatewayService */
            $gatewayService = $gatewayServiceManager->getGatewayService($this->gatewayService);

            $gatewayService->setRequest($this->request);

            if ($gatewayService->isCommand()) {
                return $gatewayService->createLockedExceptionResponse();
            }

            DB::beginTransaction();

            $this->gatewayServiceUpdate->status = GatewayServiceUpdate::PENDING;

            $gatewayServiceUpdateRepository->save($this->gatewayServiceUpdate);

            /** @var GatewayTriggerIndex $lastTriggerResult */
            $lastTriggerResult = $gatewayTriggerResultRepository->findWhere([
                ['gateway_id', '=', $this->gateway->id],
                ['update_id', '=', $this->gatewayServiceUpdate->id],
                ['value', '=', null]
            ])->first();

            $lastTriggerResult->value = $gatewayService->parseUpdate($this->request)->getBody();

            $gatewayTriggerResultRepository->save($lastTriggerResult);

            DB::commit();
        } catch (AppLogicException $ex) {
            DB::rollback();
            throw $ex;
        } catch (\Exception $ex) {
            DB::rollback();
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}