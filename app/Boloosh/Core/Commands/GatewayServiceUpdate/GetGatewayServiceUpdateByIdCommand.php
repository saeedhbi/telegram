<?php

namespace Boloosh\Core\Commands\GatewayServiceUpdate;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Exceptions\InvalidRequestException;
use Boloosh\Infrastructures\Interfaces\IGatewayServiceUpdateRepository;
use Illuminate\Support\Facades\Log;

class GetGatewayServiceUpdateByIdCommand extends BaseCommand
{

    /**
     * @var
     */
    private $gatewayServiceUpdateId;


    /**
     * GetGatewayServiceUpdateByIdCommand constructor.
     *
     * @param $gatewayServiceUpdateId
     */
    public function __construct($gatewayServiceUpdateId)
    {

        $this->gatewayServiceUpdateId = $gatewayServiceUpdateId;
    }


    /**
     * @param IGatewayServiceUpdateRepository $gatewayServiceUpdateRepository
     *
     * @return mixed
     */
    public function handle(IGatewayServiceUpdateRepository $gatewayServiceUpdateRepository)
    {
        try {
            $gatewayServiceUpdate = $gatewayServiceUpdateRepository->findWhere([ 'id' => $this->gatewayServiceUpdateId ])->first();

            if (is_null($gatewayServiceUpdate)) {
                throw new InvalidRequestException;
            }

            return $gatewayServiceUpdate;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }
}