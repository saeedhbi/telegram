<?php

namespace Boloosh\Core\Commands\GatewayServiceUpdate;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;

class DeleteGatewayServiceUpdateJobsCommand extends BaseCommand
{
    /**
     * @var GatewayServiceUpdate
     */
    private $gatewayServiceUpdate;

    /**
     * DeleteGatewayServiceUpdateJobsCommand constructor.
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     */
    public function __construct(GatewayServiceUpdate $gatewayServiceUpdate)
    {

        $this->gatewayServiceUpdate = $gatewayServiceUpdate;
    }

    public function handle()
    {
        try {
            //
        } catch (\Exception $ex) {
            throw new InvalidLogicException;
        }
    }

}