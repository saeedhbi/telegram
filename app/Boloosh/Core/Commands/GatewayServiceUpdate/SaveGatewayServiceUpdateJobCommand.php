<?php

namespace Boloosh\Core\Commands\GatewayServiceUpdate;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerResultRepository;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTriggerResult;

class SaveGatewayServiceUpdateJobCommand extends BaseCommand
{
    /**
     * @var GatewayServiceUpdate
     */
    private $gatewayServiceUpdate;
    /**
     * @var
     */
    private $jobId;

    /**
     * SaveGatewayServiceUpdateJobCommand constructor.
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     * @param $jobId
     */
    public function __construct(GatewayServiceUpdate $gatewayServiceUpdate, $jobId)
    {

        $this->gatewayServiceUpdate = $gatewayServiceUpdate;
        $this->jobId = $jobId;
    }

    public function handle(IGatewayTriggerResultRepository $gatewayTriggerResultRepository)
    {
        try {
            $payload = [
                'index' => GatewayTriggerResult::JOB_ID,
                'body' => $this->jobId
            ];

            return $gatewayTriggerResultRepository->saveGatewayTriggerResult($this->gatewayServiceUpdate, $payload);
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException;
        }
    }

}