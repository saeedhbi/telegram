<?php

namespace Boloosh\Core\Commands\GatewayServiceUpdate;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Models\GatewayService;
use Boloosh\Services\Gateway\GatewayServiceManager;
use Boloosh\Services\Gateway\Services\IGatewayService;

class ParseGatewayServiceUpdateCommand extends BaseCommand
{

    /**
     * @var
     */
    private $input;

    /**
     * @var GatewayService
     */
    private $gatewayService;


    /**
     * ParseGatewayServiceUpdateCommand constructor.
     *
     * @param GatewayService $gatewayService
     * @param                $input
     */
    public function __construct(GatewayService $gatewayService, $input)
    {
        $this->gatewayService = $gatewayService;
        $this->input          = $input;
    }


    /**
     * @param GatewayServiceManager $gatewayServiceManager
     *
     * @return mixed
     */
    public function handle(GatewayServiceManager $gatewayServiceManager)
    {
        try {
            /** @var IGatewayService $gatewayService */
            $gatewayService = $gatewayServiceManager->getGatewayService($this->gatewayService);

            return $gatewayService->parseUpdate($this->input);
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}