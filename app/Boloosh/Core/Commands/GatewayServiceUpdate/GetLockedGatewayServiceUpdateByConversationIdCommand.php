<?php

namespace Boloosh\Core\Commands\GatewayServiceUpdate;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Exceptions\InvalidRequestException;
use Boloosh\Infrastructures\Interfaces\IGatewayServiceUpdateRepository;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;

class GetLockedGatewayServiceUpdateByConversationIdCommand extends BaseCommand
{

    /**
     * @var
     */
    private $conversationId;


    /**
     * GetLockedGatewayServiceUpdateByConversationIdCommand constructor.
     *
     * @param $conversationId
     */
    public function __construct($conversationId)
    {

        $this->conversationId = $conversationId;
    }


    /**
     * @param IGatewayServiceUpdateRepository $gatewayServiceUpdateRepository
     */
    public function handle(IGatewayServiceUpdateRepository $gatewayServiceUpdateRepository)
    {
        try {
            $gatewayServiceUpdate = $gatewayServiceUpdateRepository->findWhere([ [ 'conversation_id', '=', $this->conversationId ], [ 'status', '=', GatewayServiceUpdate::LOCKED ] ])->first();

            if (is_null($gatewayServiceUpdate)) {
                throw new InvalidRequestException;
            }

            return $gatewayServiceUpdate;
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}