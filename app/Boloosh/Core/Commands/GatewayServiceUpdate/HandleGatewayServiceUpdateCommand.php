<?php

namespace Boloosh\Core\Commands\GatewayServiceUpdate;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Exceptions\InvalidRequestException;
use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Infrastructures\Models\GatewayService;
use Boloosh\Services\Gateway\GatewayServiceManager;
use Boloosh\Services\Gateway\Services\Exceptions\GatewayServiceTriggerCommandException;
use Boloosh\Services\Gateway\Services\IGatewayService;
use Illuminate\Support\Collection;

class HandleGatewayServiceUpdateCommand extends BaseCommand
{

    /**
     * @var GatewayService
     */
    private $gatewayService;

    /**
     * @var
     */
    private $request;

    /**
     * @var Collection
     */
    private $gatewayTriggers;
    /**
     * @var Gateway
     */
    private $gateway;

    /**
     * HandleGatewayServiceUpdateCommand constructor.
     *
     * @param Gateway $gateway
     * @param GatewayService $gatewayService
     * @param $gatewayTriggers
     * @param $request
     */
    public function __construct(Gateway $gateway, GatewayService $gatewayService, $gatewayTriggers, $request)
    {

        $this->gateway = $gateway;
        $this->gatewayService = $gatewayService;
        $this->gatewayTriggers = $gatewayTriggers;
        $this->request = $request;
    }

    /**
     * @param GatewayServiceManager $gatewayServiceManager
     *
     * @return mixed
     */
    public function handle(GatewayServiceManager $gatewayServiceManager)
    {
        try {
            /** @var IGatewayService $gatewayService */
            $gatewayService = $gatewayServiceManager->getGatewayService($this->gatewayService);

            $gatewayService->setRequest($this->request);

            $gatewayService->checkGatewayServiceUpdateType();

            if ($gatewayService->shouldCheckAccess()) {
                $gatewayService->processGatewayServiceUserAuthentication($this->gateway);
            }

            $gatewayService->setGatewayTriggerIndex();

            try {
                $gatewayService->checkServiceTriggerCommands();
            } catch (GatewayServiceTriggerCommandException $ex) {
                $triggerTypeIndex = $gatewayService->getGatewayTriggerIndex();

                return $gatewayService->trigger($triggerTypeIndex);
            }

            if ($gatewayService->hasAnyLockedRequest()) {

                return $gatewayService->getLockedGatewayServiceUpdate();
            }

            if (!is_null($gatewayService->getTriggerIndex())) {
                $triggerTypeIndex = $gatewayService->getGatewayTriggerIndex();

                return $gatewayService->trigger($triggerTypeIndex);
            }
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}