<?php

namespace Boloosh\Core\Commands\GatewayServiceUpdate;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Interfaces\IGatewayServiceUpdateRepository;
use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Infrastructures\Models\GatewayService;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Services\Gateway\GatewayServiceManager;
use Boloosh\Services\Gateway\Services\IGatewayServiceUpdateParser;
use Illuminate\Support\Facades\Log;

class SaveUserGatewayServiceUpdateCommand extends BaseCommand
{

    /**
     * @var Gateway
     */
    private $gateway;

    /**
     * @var
     */
    private $update;

    /**
     * @var GatewayService
     */
    private $gatewayService;


    /**
     * SaveUserGatewayServiceMessageCommand constructor.
     *
     * @param Gateway        $gateway
     * @param GatewayService $gatewayService
     * @param                $update
     */
    public function __construct(Gateway $gateway, GatewayService $gatewayService, $update)
    {
        $this->gateway        = $gateway;
        $this->gatewayService = $gatewayService;
        $this->update         = $update;
    }


    /**
     * @param IGatewayServiceUpdateRepository $gatewayServiceUpdateRepository
     *
     * @param GatewayServiceManager           $gatewayServiceManager
     *
     * @return mixed
     */
    public function handle(IGatewayServiceUpdateRepository $gatewayServiceUpdateRepository, GatewayServiceManager $gatewayServiceManager)
    {
        try {
            /** @var IGatewayServiceUpdateParser $gatewayService */
            $gatewayService = $gatewayServiceManager->getGatewayService($this->gatewayService)->parseUpdate($this->update);

            $gatewayServiceUpdate = new GatewayServiceUpdate();

            $gatewayServiceUpdate->gateway_id      = $this->gateway->id;
            $gatewayServiceUpdate->service_id      = $this->gateway->service_id;
            $gatewayServiceUpdate->message_id      = $gatewayService->getMessageId();
            $gatewayServiceUpdate->conversation_id = $gatewayService->getConversationId();
            $gatewayServiceUpdate->from_id         = $gatewayService->getSenderId();
            $gatewayServiceUpdate->type            = $gatewayService->getType();
            $gatewayServiceUpdate->body            = $gatewayService->getBody();
            $gatewayServiceUpdate->payloads        = $gatewayService->getPayloads();

            return $gatewayServiceUpdateRepository->save($gatewayServiceUpdate);
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }
}