<?php

namespace Boloosh\Core\Commands\GatewayServiceUpdate;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Interfaces\IGatewayServiceUpdateRepository;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;

class DetectConversationHasLockedUpdateCommand extends BaseCommand
{

    /**
     * @var
     */
    private $conversationId;


    /**
     * DetectConversationHasLockedUpdateCommand constructor.
     *
     * @param $conversationId
     */
    public function __construct($conversationId)
    {

        $this->conversationId = $conversationId;
    }


    /**
     * @param IGatewayServiceUpdateRepository $gatewayServiceUpdateRepository
     */
    public function handle(IGatewayServiceUpdateRepository $gatewayServiceUpdateRepository)
    {
        try {
            return $gatewayServiceUpdateRepository->findWhere([ [ 'conversation_id' , '=', $this->conversationId ], [ 'status', '=', GatewayServiceUpdate::LOCKED ] ])->first();
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}