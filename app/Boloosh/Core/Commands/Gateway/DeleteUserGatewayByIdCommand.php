<?php

namespace Boloosh\Core\Commands\Gateway;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AccessDeniedException;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Interfaces\IGatewayRepository;
use Boloosh\Infrastructures\Models\User;

class DeleteUserGatewayByIdCommand extends BaseCommand
{

    /**
     * @var User
     */
    private $user;

    /**
     * @var
     */
    private $gatewayId;


    /**
     * DeleteUserGatewayByIdCommand constructor.
     *
     * @param User $user
     * @param      $gatewayId
     */
    public function __construct(User $user, $gatewayId)
    {

        $this->user      = $user;
        $this->gatewayId = $gatewayId;
    }


    /**
     * @param IGatewayRepository $gatewayRepository
     *
     * @return int
     */
    public function handle(IGatewayRepository $gatewayRepository)
    {
        try {
            $gateway = $gatewayRepository->find($this->gatewayId);

            if ($gateway->user_id != $this->user->id) {
                throw new AccessDeniedException;
            }

            return $gatewayRepository->delete($this->gatewayId);
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}