<?php

namespace Boloosh\Core\Commands\Gateway;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\InvalidRequestException;
use Boloosh\Infrastructures\Interfaces\IGatewayRepository;
use Boloosh\Infrastructures\Models\User;

class GetUserGatewaysCommand extends BaseCommand
{

    /**
     * @var User
     */
    private $user;


    /**
     * GetUserGatewaysCommand constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {

        $this->user = $user;
    }


    /**
     * @param IGatewayRepository $gatewayRepository
     *
     * @return mixed
     */
    public function handle(IGatewayRepository $gatewayRepository)
    {
        try {
            return $gatewayRepository->with(['service'])->findWhere([ 'user_id' => $this->user->id ]);
        } catch (\Exception $ex) {
            throw new InvalidRequestException;
        }
    }

}