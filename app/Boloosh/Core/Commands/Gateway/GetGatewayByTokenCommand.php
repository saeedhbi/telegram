<?php

namespace Boloosh\Core\Commands\Gateway;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Interfaces\IGatewayRepository;
use Boloosh\Infrastructures\Models\Gateway;

class GetGatewayByTokenCommand extends BaseCommand
{

    /**
     * @var
     */
    private $token;


    /**
     * GetGatewayByTokenCommand constructor.
     *
     * @param $token
     */
    public function __construct($token)
    {
        $this->token = $token;
    }


    /**
     * @param IGatewayRepository $gatewayRepository
     *
     * @return Gateway
     */
    public function handle(IGatewayRepository $gatewayRepository)
    {
        try {
            return $gatewayRepository->findWhere([ 'token' => $this->token ])->first();
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}