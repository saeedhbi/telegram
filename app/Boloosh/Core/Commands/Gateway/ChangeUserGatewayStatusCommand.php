<?php

namespace Boloosh\Core\Commands\Gateway;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AccessDeniedException;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Interfaces\IGatewayRepository;
use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Infrastructures\Models\User;

class ChangeUserGatewayStatusCommand extends BaseCommand
{

    /**
     * @var User
     */
    private $user;

    /**
     * @var
     */
    private $gatewayId;

    /**
     * @var
     */
    private $status;


    /**
     * ChangeUserGatewayStatusCommand constructor.
     *
     * @param User $user
     * @param      $gatewayId
     * @param      $status
     */
    public function __construct(User $user, $gatewayId, $status)
    {

        $this->user      = $user;
        $this->gatewayId = $gatewayId;
        $this->status    = $status;
    }


    public function handle(IGatewayRepository $gatewayRepository)
    {
        try {
            $gateway = $gatewayRepository->find($this->gatewayId);

            if ($gateway->user_id != $this->user->id) {
                throw new AccessDeniedException;
            }

            $gateway->status = $this->status ? Gateway::ACTIVE : Gateway::DISABLED;

            return $gatewayRepository->save($gateway);
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}