<?php

namespace Boloosh\Core\Commands\Gateway;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\InvalidRequestException;
use Boloosh\Infrastructures\Interfaces\IGatewayRepository;
use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Infrastructures\Models\User;

class SaveUserGatewayCommand extends BaseCommand
{

    /**
     * @var User
     */
    private $user;

    /**
     * @var
     */
    private $type;

    /**
     * @var
     */
    private $service;

    /**
     * @var
     */
    private $name;

    /**
     * @var
     */
    private $payload;

    /**
     * @var
     */
    private $token;


    /**
     * SaveUserGatewayCommand constructor.
     *
     * @param User $user
     * @param      $type
     * @param      $service
     * @param      $name
     * @param      $queue
     * @param      $delay
     * @param      $token
     * @param      $payload
     */
    public function __construct(User $user, $type, $service, $name, $queue, $delay, $token, $payload)
    {

        $this->user    = $user;
        $this->type    = $type;
        $this->service = $service;
        $this->name    = $name;
        $this->queue   = $queue;
        $this->delay   = $delay;
        $this->token = $token;
        $this->payload = $payload;
    }


    /**
     * @param IGatewayRepository $gatewayRepository
     *
     * @return mixed
     */
    public function handle(IGatewayRepository $gatewayRepository)
    {
        try {
            $gateway             = new Gateway();
            $gateway->user_id    = $this->user->id;
            $gateway->service_id = $this->service;
            $gateway->name       = $this->name;
            $gateway->type       = $this->type ?: Gateway::HOOK;
            $gateway->queue      = $this->queue ?: false;
            $gateway->delay      = $this->delay ?: null;
            $gateway->token      = $this->token ?: null;
            $gateway->payload    = $this->payload ? json_encode($this->payload) : null;

            $gatewayRepository->save($gateway);

            return $gateway;
        } catch (\Exception $ex) {
            throw new InvalidRequestException;
        }
    }

}