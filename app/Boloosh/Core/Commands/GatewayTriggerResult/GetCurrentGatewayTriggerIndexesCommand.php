<?php

namespace Boloosh\Core\Commands\GatewayTriggerResult;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Exceptions\UnknownGatewayTriggerException;
use Boloosh\Infrastructures\Interfaces\IGatewayServiceUpdateRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerIndexRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerTypeRepository;
use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTrigger;
use Boloosh\Infrastructures\Models\GatewayTriggerIndexes;
use Boloosh\Infrastructures\Models\GatewayTriggerIndex;
use Boloosh\Infrastructures\Models\GatewayTriggerType;
use Boloosh\Services\Cache\ICacheService;
use Boloosh\Services\Gateway\Services\IGatewayServiceUpdateParser;

class GetCurrentGatewayTriggerIndexesCommand extends BaseCommand
{
    /**
     * @var
     */
    private $triggers;
    /**
     * @var IGatewayServiceUpdateParser
     */
    private $request;

    /**
     * GetCurrentGatewayTriggerIndexesCommand constructor.
     * @param $triggers
     * @param IGatewayServiceUpdateParser $request
     */
    public function __construct($triggers, IGatewayServiceUpdateParser $request)
    {

        $this->triggers = $triggers;
        $this->request = $request;
    }

    /**
     * @param IGatewayTriggerIndexRepository $gatewayTriggerIndexRepository
     * @param IGatewayTriggerTypeRepository $gatewayTriggerTypeRepository
     * @param ICacheService $cacheService
     * @param IGatewayServiceUpdateRepository $gatewayServiceUpdateRepository
     * @param IGatewayTriggerRepository $gatewayTriggerRepository
     *
     * @return \Illuminate\Support\Collection|null
     */
    public function handle(IGatewayTriggerIndexRepository $gatewayTriggerIndexRepository, IGatewayTriggerTypeRepository $gatewayTriggerTypeRepository, ICacheService $cacheService, IGatewayServiceUpdateRepository $gatewayServiceUpdateRepository, IGatewayTriggerRepository $gatewayTriggerRepository)
    {
        try {
            if (count($this->triggers) == 0) {
                throw new UnknownGatewayTriggerException;
            }

            /** @var Gateway $gateway */
            $gateway = $cacheService->get('gateway');

            $currentGatewayTriggerIndexesCount = $gatewayTriggerIndexRepository->count(['gateway_id', '=', $gateway->id]);

            if ($currentGatewayTriggerIndexesCount > 0) {
                /** @var GatewayServiceUpdate $currentGatewayServiceUpdate */
                $currentGatewayServiceUpdate = $gatewayServiceUpdateRepository->findWhere([['gateway_id', '=', $gateway->id], ['conversation_id', '=', $this->request->getConversationId()]])->first();

                /** @var GatewayTriggerIndex $currentGatewayTriggerIndex */
                $currentGatewayTriggerIndex = $gatewayTriggerIndexRepository->findWhere([['gateway_id', '=', $gateway->id], ['update_id', '=', $currentGatewayServiceUpdate->id]])->first();

                /** @var GatewayTrigger $currentGatewayTrigger */
                $currentGatewayTrigger = $gatewayTriggerRepository->findWhere(['id' => $currentGatewayTriggerIndex->trigger_id])->first();

                $gatewayTriggerTypes = $gatewayTriggerTypeRepository->findWhereIn('id', json_decode($currentGatewayTrigger->types));

                $triggers = collect();

                /** @var GatewayTriggerType $type */
                foreach ($gatewayTriggerTypes as $type) {
                    $gatewayTriggerIndexes = new GatewayTriggerIndexes($type->key);

                    $triggers->push($gatewayTriggerIndexes);
                }

                return $triggers;
            }

            return null;
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}