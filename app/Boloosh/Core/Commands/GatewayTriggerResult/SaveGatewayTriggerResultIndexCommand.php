<?php

namespace Boloosh\Core\Commands\GatewayTriggerResult;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerIndexRepository;
use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTriggerIndex;
use Boloosh\Infrastructures\Models\GatewayTriggerType;
use Boloosh\Services\Cache\ICacheService;

class SaveGatewayTriggerResultIndexCommand extends BaseCommand
{
    /**
     * @var
     */
    private $gatewayTriggerTypeIndex;
    /**
     * @var
     */
    private $triggerId;
    /**
     * @var GatewayServiceUpdate
     */
    private $gatewayServiceUpdate;
    /**
     * @var
     */
    private $shouldTrigger;

    /**
     * SaveGatewayTriggerResultIndexCommand constructor.
     * @param $triggerId
     * @param $gatewayTriggerTypeIndex
     * @param $shouldTrigger
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     */
    public function __construct($triggerId, $gatewayTriggerTypeIndex, GatewayServiceUpdate $gatewayServiceUpdate, $shouldTrigger = false)
    {

        $this->gatewayTriggerTypeIndex = $gatewayTriggerTypeIndex;
        $this->triggerId = $triggerId;
        $this->gatewayServiceUpdate = $gatewayServiceUpdate;
        $this->shouldTrigger = $shouldTrigger;
    }

    /**
     * @param ICacheService $cacheService
     * @param IGatewayTriggerIndexRepository $gatewayTriggerResultRepository
     * @return mixed
     */
    public function handle(ICacheService $cacheService, IGatewayTriggerIndexRepository $gatewayTriggerResultRepository)
    {
        try {
            /** @var Gateway $gateway */
            $gateway = $cacheService->get('gateway');

            $gatewayTriggerIndex = $gatewayTriggerResultRepository->findWhere(['update_id' => $this->gatewayServiceUpdate->id])->first();

            if(is_null($gatewayTriggerIndex)) {
                $gatewayTriggerIndex = new GatewayTriggerIndex();
            }

            $gatewayTriggerIndex->gateway_id = $gateway->id;
            $gatewayTriggerIndex->trigger_id = $this->triggerId;
            $gatewayTriggerIndex->index_id = $this->gatewayTriggerTypeIndex;
            $gatewayTriggerIndex->update_id = $this->gatewayServiceUpdate->id;
            $gatewayTriggerIndex->type = $this->shouldTrigger ? GatewayTriggerIndex::FIRE_OBSERVE : GatewayTriggerIndex::MUTE_OBSERVE;

            return $gatewayTriggerResultRepository->save($gatewayTriggerIndex);
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}