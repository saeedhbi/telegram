<?php

namespace Boloosh\Core\Commands\GatewayTriggerResult;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Interfaces\IGatewayServiceUpdateRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerIndexRepository;
use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTriggerIndex;
use Boloosh\Services\Cache\ICacheService;

class GetCurrentGatewayTriggerIndexCommand extends BaseCommand
{
    /**
     * @var
     */
    private $conversationId;

    /**
     * GetCurrentGatewayTriggerIndexCommand constructor.
     * @param $conversationId
     */
    public function __construct($conversationId)
    {

        $this->conversationId = $conversationId;
    }

    /**
     * @param ICacheService $cacheService
     * @param IGatewayServiceUpdateRepository $gatewayServiceUpdateRepository
     * @param IGatewayTriggerIndexRepository $gatewayTriggerIndexRepository
     *
     * @return int
     */
    public function handle(ICacheService $cacheService, IGatewayServiceUpdateRepository $gatewayServiceUpdateRepository, IGatewayTriggerIndexRepository $gatewayTriggerIndexRepository)
    {
        try {
            /** @var Gateway $gateway */
            $gateway = $cacheService->get('gateway');

            /** @var GatewayServiceUpdate $gatewayServiceUpdate */
            $gatewayServiceUpdate = $gatewayServiceUpdateRepository->findWhere([['gateway_id', '=', $gateway->id], ['conversation_id', '=', $this->conversationId], ['status', '=', GatewayServiceUpdate::PENDING]])->first();

            if(is_null($gatewayServiceUpdate)) {
                return null;
            }

            /** @var GatewayTriggerIndex $gatewayTriggerIndex */
            $gatewayTriggerIndex = $gatewayTriggerIndexRepository->findWhere([['gateway_id', '=', $gateway->id], ['update_id', '=', $gatewayServiceUpdate->id]])->first();

            if(is_null($gatewayTriggerIndex)) {
               return null;
            }

            return $gatewayTriggerIndex->index_id;
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }
}