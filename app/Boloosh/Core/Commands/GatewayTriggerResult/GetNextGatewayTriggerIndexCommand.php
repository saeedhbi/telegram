<?php

namespace Boloosh\Core\Commands\GatewayTriggerResult;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerIndexRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerTypeRepository;
use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTrigger;
use Boloosh\Infrastructures\Models\GatewayTriggerIndex;
use Boloosh\Infrastructures\Models\GatewayTriggerIndexes;
use Boloosh\Infrastructures\Models\GatewayTriggerType;
use Boloosh\Services\Cache\ICacheService;

class GetNextGatewayTriggerIndexCommand extends BaseCommand
{
    /**
     * @var GatewayServiceUpdate
     */
    private $gatewayServiceUpdate;

    /**
     * GetNextGatewayTriggerIndexCommand constructor.
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     */
    public function __construct(GatewayServiceUpdate $gatewayServiceUpdate)
    {

        $this->gatewayServiceUpdate = $gatewayServiceUpdate;
    }

    /**
     * @param ICacheService $cacheService
     * @param IGatewayTriggerIndexRepository $gatewayTriggerIndexRepository
     * @param IGatewayTriggerRepository $gatewayTriggerRepository
     * @param IGatewayTriggerTypeRepository $gatewayTriggerTypeRepository
     */
    public function handle(ICacheService $cacheService, IGatewayTriggerIndexRepository $gatewayTriggerIndexRepository, IGatewayTriggerRepository $gatewayTriggerRepository, IGatewayTriggerTypeRepository $gatewayTriggerTypeRepository)
    {
        try {
            /** @var Gateway $gateway */
            $gateway = $cacheService->get('gateway');

            /** @var GatewayTriggerIndex $gatewayTriggerIndex */
            $gatewayTriggerIndex = $gatewayTriggerIndexRepository->findWhere([['gateway_id', '=', $gateway->id], ['update_id', '=', $this->gatewayServiceUpdate->id]])->first();

            /** @var GatewayTrigger $gatewayTrigger */
            $gatewayTrigger = $gatewayTriggerRepository->find($gatewayTriggerIndex->trigger_id);

            $gatewayTriggerTypes = $gatewayTriggerTypeRepository->findWhereIn('id', json_decode($gatewayTrigger->types));

            $indexes = collect();

            foreach (json_decode($gatewayTrigger->types) as $type) {

                /** @var GatewayTriggerType $triggerType */
                $triggerType = $gatewayTriggerTypes->where('id', $type)->first();

                $triggerTypeIndexes = new GatewayTriggerIndexes($triggerType->id);

                foreach ($triggerTypeIndexes->indexes as $index) {
                    $indexes->push($index);
                }
            }

            if(!in_array($gatewayTriggerIndex->index_id, $indexes->toArray())) {
                return $indexes->first();
            }

            if($indexes->last() == $gatewayTriggerIndex->index_id) {
                return null;
            }

            $currentIndexKey = array_search($gatewayTriggerIndex->index_id, $indexes->toArray());

            return $indexes->get($currentIndexKey + 1);
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}