<?php

namespace Boloosh\Core\Commands\GatewayService;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Interfaces\IGatewayServiceRepository;
use Boloosh\Infrastructures\Models\GatewayService;

class GetGatewayServiceByIdCommand extends BaseCommand
{

    /**
     * @var
     */
    private $gatewayServiceId;


    /**
     * GetGatewayServiceByIdCommand constructor.
     *
     * @param $gatewayServiceId
     */
    public function __construct($gatewayServiceId)
    {

        $this->gatewayServiceId = $gatewayServiceId;
    }


    /**
     * @param IGatewayServiceRepository $gatewayServiceRepository
     *
     * @return GatewayService
     */
    public function handle(IGatewayServiceRepository $gatewayServiceRepository)
    {
        try {
            return $gatewayServiceRepository->findWhere([ 'id' => $this->gatewayServiceId ])->first();
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}