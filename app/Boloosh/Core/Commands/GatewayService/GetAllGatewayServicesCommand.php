<?php

namespace Boloosh\Core\Commands\GatewayService;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Interfaces\IGatewayServiceRepository;

class GetAllGatewayServicesCommand extends BaseCommand
{

    public function handle(IGatewayServiceRepository $gatewayServiceRepository)
    {
        try {
            return $gatewayServiceRepository->all();
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}