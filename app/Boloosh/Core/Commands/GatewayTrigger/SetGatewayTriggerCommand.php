<?php

namespace Boloosh\Core\Commands\GatewayTrigger;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidGatewayServiceTriggerException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerIndexRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerResultRepository;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTrigger;
use Boloosh\Infrastructures\Models\GatewayTriggerIndex;
use Boloosh\Infrastructures\Models\GatewayTriggerType;

class SetGatewayTriggerCommand extends BaseCommand
{
    /**
     * @var GatewayServiceUpdate
     */
    private $gatewayServiceUpdate;
    /**
     * @var
     */
    private $trigger;

    /**
     * SetGatewayTriggerCommand constructor.
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     * @param $trigger
     */
    public function __construct(GatewayServiceUpdate $gatewayServiceUpdate, $trigger)
    {

        $this->gatewayServiceUpdate = $gatewayServiceUpdate;
        $this->trigger = $trigger;
    }

    /**
     * @param IGatewayTriggerRepository $gatewayTriggerRepository
     * @param IGatewayTriggerIndexRepository $gatewayTriggerIndexRepository
     *
     * @param IGatewayTriggerResultRepository $gatewayTriggerResultRepository
     * @return GatewayTriggerIndex
     */
    public function handle(IGatewayTriggerRepository $gatewayTriggerRepository, IGatewayTriggerIndexRepository $gatewayTriggerIndexRepository, IGatewayTriggerResultRepository $gatewayTriggerResultRepository)
    {
        try {
            $gatewayTriggers = $gatewayTriggerRepository->findWhere(['gateway_id' => $this->gatewayServiceUpdate->gateway_id]);

            $trigger = null;

            /** @var GatewayTrigger $gatewayTrigger */
            foreach ($gatewayTriggers as $gatewayTrigger) {
                if ($gatewayTrigger->name == $this->trigger) {
                    $trigger = $gatewayTrigger;
                }
            }

            if (is_null($trigger)) {
                throw new InvalidGatewayServiceTriggerException;
            }

            /** @var GatewayTriggerIndex $gatewayTriggerIndex */
            $gatewayTriggerIndex = $gatewayTriggerIndexRepository->findWhere(['update_id' => $this->gatewayServiceUpdate->id])->first();

            $gatewayTriggerIndex->trigger_id = $trigger->id;

            $gatewayTriggerIndexRepository->save($gatewayTriggerIndex);

            $payload = [
                'index' => GatewayTriggerType::PICK_GATEWAY_TRIGGER,
                'body' => null
            ];

            $gatewayTriggerResultRepository->saveGatewayTriggerResult($this->gatewayServiceUpdate, $payload);

            return $gatewayTriggerIndex;
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}