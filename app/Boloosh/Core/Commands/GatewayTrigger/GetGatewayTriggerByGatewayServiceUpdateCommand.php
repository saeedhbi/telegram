<?php

namespace Boloosh\Core\Commands\GatewayTrigger;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerIndexRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerRepository;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTriggerIndex;

class GetGatewayTriggerByGatewayServiceUpdateCommand extends BaseCommand
{
    /**
     * @var GatewayServiceUpdate
     */
    private $gatewayServiceUpdate;

    /**
     * GetGatewayTriggerByGatewayServiceUpdateCommand constructor.
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     */
    public function __construct(GatewayServiceUpdate $gatewayServiceUpdate)
    {

        $this->gatewayServiceUpdate = $gatewayServiceUpdate;
    }

    /**
     * @param IGatewayTriggerIndexRepository $gatewayTriggerIndexRepository
     * @param IGatewayTriggerRepository $gatewayTriggerRepository
     * @return mixed
     */
    public function handle(IGatewayTriggerIndexRepository $gatewayTriggerIndexRepository, IGatewayTriggerRepository $gatewayTriggerRepository)
    {
        try {
            /** @var GatewayTriggerIndex $gatewayTriggerIndex */
            $gatewayTriggerIndex = $gatewayTriggerIndexRepository->findWhere([['update_id', '=', $this->gatewayServiceUpdate->id]])->first();

            if (is_null($gatewayTriggerIndex)) {
                return null;
            }

            return $gatewayTriggerRepository->find($gatewayTriggerIndex->trigger_id);
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}