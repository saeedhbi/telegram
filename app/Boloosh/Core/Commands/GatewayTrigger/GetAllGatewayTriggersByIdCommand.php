<?php

namespace Boloosh\Core\Commands\GatewayTrigger;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerTypeRepository;
use Boloosh\Infrastructures\Models\GatewayTrigger;
use Boloosh\Infrastructures\Models\GatewayTriggerIndexes;
use Boloosh\Infrastructures\Models\GatewayTriggerType;
use Illuminate\Support\Collection;

class GetAllGatewayTriggersByIdCommand extends BaseCommand
{

    /**
     * @var
     */
    private $gatewayId;


    /**
     * GetAllGatewayTriggersByIdCommand constructor.
     *
     * @param $gatewayId
     */
    public function __construct($gatewayId)
    {

        $this->gatewayId = $gatewayId;
    }

    /**
     * @param IGatewayTriggerRepository $gatewayTriggerRepository
     *
     * @param IGatewayTriggerTypeRepository $gatewayTriggerTypeRepository
     * @return Collection
     */
    public function handle(IGatewayTriggerRepository $gatewayTriggerRepository, IGatewayTriggerTypeRepository $gatewayTriggerTypeRepository)
    {
        try {
            $gatewayTriggers = $gatewayTriggerRepository->findWhere([ 'gateway_id' => $this->gatewayId ]);

            $gatewayTriggersCollection = collect([]);

            /** @var GatewayTrigger $gatewayTrigger */
            foreach ($gatewayTriggers as $gatewayTrigger) {
                $gatewayTriggerTypes = $gatewayTriggerTypeRepository->findWhereIn('id', json_decode($gatewayTrigger->types));

                $indexes = collect();
                /** @var GatewayTriggerType $type */
                foreach ($gatewayTriggerTypes as $type) {
                    $gatewayTriggerIndexes = new GatewayTriggerIndexes($type->id);

                    $indexes->push($gatewayTriggerIndexes);
                }

                $gatewayTriggersCollection->put($gatewayTrigger->id, $indexes);
            }

            return $gatewayTriggersCollection;
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}