<?php

namespace Boloosh\Infrastructures\Models;

use Carbon\Carbon;

/**
 * @property int id
 * @property int gateway_id
 * @property int gateway_trigger_type_id
 * @property string body
 * @property Carbon created_at
 * @property Carbon updated_at
 */
class GatewayTriggerDefault extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}