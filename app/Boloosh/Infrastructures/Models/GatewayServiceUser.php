<?php

namespace Boloosh\Infrastructures\Models;

/**
 * @property int    id
 * @property int    service_id
 * @property int    service_user_id
 * @property int    service_user_username
 * @property int    status
 */
class GatewayServiceUser extends BaseModel
{

    /*
     * Status
     */
    const PENDING = 0;
    const ACTIVE = 1;
    const DISABLED = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [ ];
}