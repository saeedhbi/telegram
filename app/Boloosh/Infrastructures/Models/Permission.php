<?php

namespace Boloosh\Infrastructures\Models;

use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{

    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [];
}