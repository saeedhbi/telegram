<?php

namespace Boloosh\Infrastructures\Models;

/**
 * @property int    id
 * @property int    gateway_id
 * @property int    trigger_id
 * @property int    index_id
 * @property int    update_id
 * @property int    type
 * @property string value
 */
class GatewayTriggerIndex extends BaseMongoModel
{

    protected $collection = 'gateway_trigger_index';

    /*
     * Type
     */
    const MUTE_OBSERVE = 0;
    const FIRE_OBSERVE = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [ ];
}