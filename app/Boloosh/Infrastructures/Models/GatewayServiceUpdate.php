<?php

namespace Boloosh\Infrastructures\Models;
use Boloosh\Services\SDK\Telegram\ITelegramService;

/**
 * @property int id
 * @property int gateway_id
 * @property int service_id
 * @property int message_id
 * @property int conversation_id
 * @property int from_id
 * @property int type
 * @property Mixed body
 * @property string payloads
 * @property int status
 *
 * @property Gateway gateway
 */
class GatewayServiceUpdate extends BaseModel
{

    /*
     * Status
     */
    const PENDING = 1;
    const SENT = 2;
    const CANCELED = 3;
    const LOCKED = 4;

    /*
     * Type
     */
    const TEXT = 'text';
    const DOCUMENT = 'document';
    const PHOTO = 'photo';
    const AUDIO = 'audio';
    const VIDEO = 'video';
    const VOICE = 'voice';
    const GAME = 'game';
    const STICKER = 'sticker';
    const LOCATION = 'location';
    const CONTACT = 'contact';
    const VENUE = 'venue';

    private $intToType = [
        10 => self::TEXT,
        11 => self::DOCUMENT,
        12 => self::PHOTO,
        13 => self::AUDIO,
        14 => self::VIDEO,
        15 => self::VOICE,
        16 => self::GAME,
        17 => self::STICKER,
        18 => self::LOCATION,
        19 => self::CONTACT,
        20 => self::VENUE,
    ];

    private $typeToInt = [
        self::TEXT => 10,
        self::DOCUMENT => 11,
        self::PHOTO => 12,
        self::AUDIO => 13,
        self::VIDEO => 14,
        self::VOICE => 15,
        self::GAME => 16,
        self::STICKER => 17,
        self::LOCATION => 18,
        self::CONTACT => 19,
        self::VENUE => 20,
    ];

    public function getTypeAttribute($value)
    {
        return $this->intToType[$value];
    }

    public function setTypeAttribute($value)
    {
        $this->attributes['type'] = $this->typeToInt[$value];
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gateway()
    {
        return $this->belongsTo(Gateway::class, 'gateway_id');
    }
}