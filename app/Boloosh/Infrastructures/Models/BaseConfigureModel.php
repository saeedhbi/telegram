<?php namespace Boloosh\Infrastructures\Models;

use Illuminate\Database\Eloquent\Collection;

class BaseConfigureModel
{

    /**
     * Plan constructor.
     *
     * @param $id
     */
    public function __construct($id = null)
    {
        if ($id) {
            $this->loadConfig($id);
            $this->id = Config('boloosh.' . static::_configName . '.' . $id) == null ? null : $id;
        }
    }


    public function __toString()
    {
        return json_encode($this);
    }


    private function loadConfig($id)
    {
        $configs = Config('boloosh.' . static::_configName . '.' . $id);
        foreach (get_class_vars(get_class($this)) as $key => $value) {
            if ($key != 'id' && substr($key, 0, 1) != '_') {
                $this->$key = $configs[$key];
            }
        }

    }


    /**
     * @return Collection
     */
    public static function getAll()
    {
        $collection = [ ];
        foreach (Config('boloosh.' . static::_configName) as $key => $config) {
            $class            = static::class;
            $collection[$key] = new $class($key);
        }

        return collect($collection);
    }

}