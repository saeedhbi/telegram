<?php

namespace Boloosh\Infrastructures\Models;

/**
 * @property mixed  id
 * @property string name
 * @property string preferred_url
 * @property int    can_secure
 */
class GatewayService extends BaseModel
{

	const TELEGRAM = 1;
	const PINTEREST = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [ ];
}