<?php namespace Boloosh\Infrastructures\Models;

use Illuminate\Database\Eloquent\Collection;

class GatewayTriggerIndexes extends BaseConfigureModel
{

    const _configName = 'gateway_trigger_indexes';

    /**
     * @var Collection
     */
    public $_items;

    /**
     * @var
     */
    public $type;

    /**
     * @var
     */
    public $indexes;

    /**
     * @param $index
     * @return int|mixed|null|string
     */
    public function getTriggerType($index)
    {
        $triggers = self::getAll();

        foreach ($triggers as $trigger) {
            if(in_array($index, $trigger->indexes)) {
                return $trigger->type;
            }
        }

        return null;
    }

}