<?php

namespace Boloosh\Infrastructures\Models;

/**
 * @property integer id
 * @property string key
 * @property integer can_skip
 * @property integer has_keyboard
 * @property integer keyboard_type
 * @property string keyboard_layout
 */
class GatewayTriggerType extends BaseModel
{

    /*
     * @key Type
     */
    const CANCEL = 1;
    const CAPTION = 2;
    const SIGN = 3;
    const BUTTON = 4;
    const TIME = 5;
    const DELETE_TIME = 6;
    const AUDIO_PERFORM = 7;
    const AUDIO_CUT = 8;
    const AUDIO_COVER = 9;
    const ATTACH_UPDATE = 10;
    const DESTINATION = 11;

    /*
     * @keyboard_type Type
     */
    const KEYBOARD = 1;
    const INLINE_KEYBOARD = 2;

    /*
     * GatewayTriggerType commands
     */
    const PICK_GATEWAY_TRIGGER = 1;
    const SET_CANCEL = 2;


    // DESTINATION
    const GET_DESTINATION = 6;
    const SET_DESTINATION = 7;

    // CAPTION
    const GET_CAPTION = 10;
    const SET_CAPTION = 11;

    // SIGN
    const GET_SIGN = 16;
    const SET_SIGN = 17;

    // TIME_HOUR
    const GET_TIME = 21;
    const SET_TIME = 22;
    const GET_DELETE_TIME = 23;
    const SET_DELETE_TIME = 24;

    // BUTTON
    const GET_BUTTON_TITLE = 31;
    const SET_BUTTON_TITLE = 32;
    const GET_BUTTON_LINK = 33;
    const SET_BUTTON_LINK = 34;

    // AUDIO_PERFORM
    const GET_AUDIO_PERFORM_TITLE = 41;
    const SET_AUDIO_PERFORM_TITLE = 42;
    const GET_AUDIO_PERFORM_PERFORMER = 43;
    const SET_AUDIO_PERFORM_PERFORMER = 44;
    const GET_AUDIO_COVER = 45;
    const SET_AUDIO_COVER = 46;
    const GET_AUDIO_CUT_START = 47;
    const SET_AUDIO_CUT_START = 48;
    const GET_AUDIO_CUT_DURATION = 49;
    const SET_AUDIO_CUT_DURATION = 50;

    // BUTTON
    const GET_ATTACH_UPDATE = 60;
    const SET_ATTACH_UPDATE = 61;

    /*
     * Commands
     */
    const COMMAND_CANCEL = 'cancel';
    const COMMAND_SKIP = 'skip';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function isSetter()
    {
        return starts_with($this->key, 'set');
    }
}