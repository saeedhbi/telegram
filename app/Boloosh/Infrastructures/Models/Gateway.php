<?php

namespace Boloosh\Infrastructures\Models;

/**
 * @property integer id
 * @property mixed   user_id
 * @property integer service_id
 * @property integer destination_id
 * @property string  name
 * @property integer type
 * @property integer queue
 * @property integer delay
 * @property string  token
 * @property string  payload
 * @property integer status
 */
class Gateway extends BaseModel
{

    const ACTIVE = 10;
    const DISABLED = 0;

    /*
     * Type
     */
    const HOOK = 'hook';
    const REQUEST = 'request';

    private $intToType = [
        1 => self::HOOK,
        2 => self::REQUEST,
    ];

    private $typeToInt = [
        self::HOOK    => 1,
        self::REQUEST => 2,
    ];


    public function getTypeAttribute($value)
    {
        return $this->intToType[$value];
    }


    public function setTypeAttribute($value)
    {
        $this->attributes['type'] = $this->typeToInt[$value];
    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [ ];

    /**
     * Retrieve gateway service
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function service()
    {
        return $this->belongsTo(GatewayService::class, 'service_id');
    }

    /**
     * Retrieve gateway destination service
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function destination()
    {
        return $this->belongsTo(GatewayService::class, 'destination_id');
    }
}