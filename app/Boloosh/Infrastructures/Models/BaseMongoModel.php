<?php

namespace Boloosh\Infrastructures\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class BaseMongoModel extends Eloquent
{
    protected $connection = 'mongodb';
}