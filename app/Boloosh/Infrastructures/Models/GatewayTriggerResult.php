<?php

namespace Boloosh\Infrastructures\Models;

/**
 * Class GatewayTriggerResults
 * @package Boloosh\Infrastructures\Models
 *
 * @property int _id
 * @property int gateway_id
 * @property int gateway_service_id
 * @property int gateway_service_update_id
 * @property array payload
 * @property int status
 */
class GatewayTriggerResult extends BaseMongoModel
{
    /**
     * The collection associated with the model.
     *
     * @var string
     */
    protected $collection = 'gateway_trigger_results';

    /*
     * Status
     */
    const PENDING = 0;
    const PUBLISH = 1;
    const CANCELED = 2;

    /*
     * Non trigger type indexes
     */
    const PUBLISH_CONVERSATION_ID = 'publish_conversation_id';
    const PUBLISH_MESSAGE_ID = 'publish_message_id';
    const PUBLISH_MESSAGE_ATTACH_ID = 'publish_message_attach_id';
    const PUBLISH_FILE_PATH = 'publish_file_path';
    const JOB_ID = 'job_id';

    /**
     * Retrieve GatewayServiceUpdate publish job id
     */
    public function getJobId()
    {
        foreach ($this->payload as $item) {
            if ($item['index'] == GatewayTriggerResult::JOB_ID) {
                return $item['body'];
            }
        }

        return null;
    }
}