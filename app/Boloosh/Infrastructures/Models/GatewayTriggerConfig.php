<?php namespace Boloosh\Infrastructures\Models;

use Illuminate\Database\Eloquent\Collection;

class GatewayTriggerConfig extends BaseConfigureModel
{

    const _configName = 'gateway_trigger_config';

    /**
     * @var Collection
     */
    public $_items;

    /**
     * @var
     */
    public $data;
}