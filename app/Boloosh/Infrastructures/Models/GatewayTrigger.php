<?php

namespace Boloosh\Infrastructures\Models;

/**
 * @property integer id
 * @property integer gateway_id
 * @property string  name
 * @property string types
 * @property integer status
 */

class GatewayTrigger extends BaseModel
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [ ];

    /**
     * Check current model status
     *
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1 ? true : false;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gateway()
    {
        return $this->belongsTo(Gateway::class, 'gateway_id');
    }
}