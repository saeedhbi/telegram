<?php

namespace Boloosh\Infrastructures\Interfaces;

use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTriggerResult;

interface IGatewayTriggerResultRepository extends IBaseRepository
{

    /**
     * @param $gatewayServiceUpdateId
     * @return mixed
     */
    public function findByUpdateId($gatewayServiceUpdateId);

    /**
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     * @param $payload
     * @return mixed
     */
    public function saveGatewayTriggerResult(GatewayServiceUpdate $gatewayServiceUpdate, $payload);

    /**
     * @param GatewayTriggerResult $gatewayTriggerResult
     * @param $index
     * @return mixed
     */
    public function getPayload(GatewayTriggerResult $gatewayTriggerResult, $index);
}