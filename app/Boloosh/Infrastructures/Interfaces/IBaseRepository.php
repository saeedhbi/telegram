<?php

namespace Boloosh\Infrastructures\Interfaces;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\RepositoryInterface;

interface IBaseRepository extends RepositoryInterface
{

    /**
     * @param       $field
     * @param null  $value
     * @param array $columns
     *
     * @return mixed
     */
    public function findOneByField($field, $value = null, $columns = [ '*' ]);


    /**
     * @param       $id
     * @param array $columns
     *
     * @return mixed
     */
    public function findOrNull($id, $columns = [ '*' ]);


    /**
     * @param Model $model
     *
     * @return mixed
     */
    public function save(Model &$model);


    /**
     * @param     $number
     * @param int $skip
     *
     * @return mixed
     */
    public function take($number, $skip = 0);


    /**
     * @param null $where
     *
     * @return int
     */
    public function count($where = null);


    /**
     * @param $number
     *
     * @return mixed
     */
    public function takeRandom($number);
}