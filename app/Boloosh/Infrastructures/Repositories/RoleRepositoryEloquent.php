<?php

namespace Boloosh\Infrastructures\Repositories;

use Boloosh\Infrastructures\Models\Role;
use Boloosh\Infrastructures\Interfaces\IRoleRepository;

class RoleRepositoryEloquent extends BaseRepositoryEloquent implements IRoleRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Role::class;
    }
}