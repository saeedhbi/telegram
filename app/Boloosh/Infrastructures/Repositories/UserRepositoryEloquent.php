<?php

namespace Boloosh\Infrastructures\Repositories;

use Boloosh\Infrastructures\Interfaces\IUserRepository;
use Boloosh\Infrastructures\Models\User;

class UserRepositoryEloquent extends BaseRepositoryEloquent implements IUserRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }
}