<?php

namespace Boloosh\Infrastructures\Repositories;

use Boloosh\Infrastructures\Interfaces\IGatewayServiceUserRepository;
use Boloosh\Infrastructures\Models\GatewayServiceUser;

class GatewayServiceUserRepositoryEloquent extends BaseRepositoryEloquent implements IGatewayServiceUserRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return GatewayServiceUser::class;
    }
}