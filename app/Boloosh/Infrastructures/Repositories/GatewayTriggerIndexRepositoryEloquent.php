<?php

namespace Boloosh\Infrastructures\Repositories;

use Boloosh\Infrastructures\Models\GatewayTriggerIndex;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerIndexRepository;

class GatewayTriggerIndexRepositoryEloquent extends BaseRepositoryEloquent implements IGatewayTriggerIndexRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return GatewayTriggerIndex::class;
    }
}