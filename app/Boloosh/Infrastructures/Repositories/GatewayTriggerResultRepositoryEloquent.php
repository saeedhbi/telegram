<?php

namespace Boloosh\Infrastructures\Repositories;

use Boloosh\Infrastructures\Interfaces\IGatewayTriggerResultRepository;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTriggerResult;

class GatewayTriggerResultRepositoryEloquent extends BaseRepositoryEloquent implements IGatewayTriggerResultRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return GatewayTriggerResult::class;
    }

    /**
     * @param $gatewayServiceUpdateId
     * @return mixed
     */
    public function findByUpdateId($gatewayServiceUpdateId)
    {
        return GatewayTriggerResult::where('gateway_service_update_id', '=', $gatewayServiceUpdateId)->first();
    }

    /**
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     * @param $payload
     * @return bool
     */
    public function saveGatewayTriggerResult(GatewayServiceUpdate $gatewayServiceUpdate, $payload)
    {
        /** @var GatewayTriggerResult $gatewayTriggerResult */
        $gatewayTriggerResult = $this->findByUpdateId($gatewayServiceUpdate->id);

        if (is_null($gatewayTriggerResult)) {
            $gatewayTriggerResult = new GatewayTriggerResult();
            $gatewayTriggerResult->gateway_id = $gatewayServiceUpdate->gateway_id;
            $gatewayTriggerResult->gateway_service_update_id = $gatewayServiceUpdate->id;
            $gatewayTriggerResult->status = GatewayTriggerResult::PENDING;
            $gatewayTriggerResult->payload = [$payload];
        } else {
            $gatewayTriggerResultPayload = $gatewayTriggerResult->payload;
            array_push($gatewayTriggerResultPayload, $payload);
            $gatewayTriggerResult->payload = $gatewayTriggerResultPayload;
        }

        return $gatewayTriggerResult->save();
    }

    /**
     * @param GatewayTriggerResult $gatewayTriggerResult
     * @param $index
     * @return mixed
     */
    public function getPayload(GatewayTriggerResult $gatewayTriggerResult, $index)
    {
        $result = null;

        foreach (collect($gatewayTriggerResult->payload) as $item) {
            if ($item['index'] == $index) {
                $result = $item['body'];
                break;
            }
        }

        return $result;
    }
}