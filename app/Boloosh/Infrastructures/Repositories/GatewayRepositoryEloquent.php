<?php

namespace Boloosh\Infrastructures\Repositories;

use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Infrastructures\Interfaces\IGatewayRepository;

class GatewayRepositoryEloquent extends BaseRepositoryEloquent implements IGatewayRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Gateway::class;
    }
}