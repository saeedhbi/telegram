<?php

namespace Boloosh\Infrastructures\Repositories;

use Boloosh\Infrastructures\Interfaces\IBaseRepository;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Eloquent\BaseRepository as PrettusBaseRepository;

abstract class BaseRepositoryEloquent extends PrettusBaseRepository implements IBaseRepository
{

    /**
     * @param       $field
     * @param null  $value
     * @param array $columns
     *
     * @return mixed
     */
    public function findOneByField($field, $value = null, $columns = [ '*' ])
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->where($field, '=', $value)->first($columns);
        $this->resetModel();

        return $this->parserResult($model);
    }


    /**
     * @param       $id
     * @param array $columns
     *
     * @return mixed
     */
    public function findOrNull($id, $columns = [ '*' ])
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->find($id, $columns);
        $this->resetModel();

        return $this->parserResult($model);
    }

    /**
     * @param Model $model
     * @param array $options
     *
     * @return bool
     */
    public function save(Model &$model, array $options = [ ])
    {
        return $model->save($options);
    }


    /**
     * @param      $number
     * @param int  $skip
     *
     * @param bool $parse
     *
     * @return mixed
     */
    public function take($number, $skip = 0, $parse = true)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->skip($skip)->take($number);

        if ($parse) {
            $this->resetModel();

            return $this->parserResult($model);
        } else {
            return $this;
        }
    }


    /**
     * @param null $where
     *
     * @return mixed
     */
    public function count($where = null)
    {
        $this->applyCriteria();
        $this->applyScope();
        if ($where) {
            $count = $this->model->where($where[0], $where[1], $where[2])->count();
            $this->resetModel();

            return $count;
        }
        $count = $this->model->count();
        $this->resetModel();

        return $count;
    }


    /**
     * @param $number
     *
     * @return mixed
     */
    public function takeRandom($number)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->orderByRaw("RAND()")->limit($number)->get();
        $this->resetModel();

        return $this->parserResult($model);
    }
}