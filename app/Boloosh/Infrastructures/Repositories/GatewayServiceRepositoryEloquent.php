<?php

namespace Boloosh\Infrastructures\Repositories;

use Boloosh\Infrastructures\Models\GatewayService;
use Boloosh\Infrastructures\Interfaces\IGatewayServiceRepository;

class GatewayServiceRepositoryEloquent extends BaseRepositoryEloquent implements IGatewayServiceRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return GatewayService::class;
    }
}