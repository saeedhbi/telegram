<?php

namespace Boloosh\Infrastructures\Repositories;

use Boloosh\Infrastructures\Interfaces\IGatewayTriggerDefaultRepository;
use Boloosh\Infrastructures\Models\GatewayTriggerDefault;

class GatewayTriggerDefaultRepositoryEloquent extends BaseRepositoryEloquent implements IGatewayTriggerDefaultRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return GatewayTriggerDefault::class;
    }
}