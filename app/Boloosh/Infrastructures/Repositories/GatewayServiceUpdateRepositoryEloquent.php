<?php

namespace Boloosh\Infrastructures\Repositories;

use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Interfaces\IGatewayServiceUpdateRepository;

class GatewayServiceUpdateRepositoryEloquent extends BaseRepositoryEloquent implements IGatewayServiceUpdateRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return GatewayServiceUpdate::class;
    }
}