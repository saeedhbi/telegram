<?php

namespace Boloosh\Infrastructures\Repositories;

use Boloosh\Infrastructures\Criterias\GatewayTrigger\GatewayTriggerIsActiveCriteria;
use Boloosh\Infrastructures\Criterias\GatewayTrigger\GatewayTriggerOrderCriteria;
use Boloosh\Infrastructures\Models\GatewayTrigger;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerRepository;

class GatewayTriggerRepositoryEloquent extends BaseRepositoryEloquent implements IGatewayTriggerRepository
{

    /**
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function boot()
    {
        $this->pushCriteria(new GatewayTriggerIsActiveCriteria());
    }


    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return GatewayTrigger::class;
    }
}