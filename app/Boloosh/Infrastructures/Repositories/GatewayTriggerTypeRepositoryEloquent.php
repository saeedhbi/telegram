<?php

namespace Boloosh\Infrastructures\Repositories;

use Boloosh\Infrastructures\Models\GatewayTriggerType;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerTypeRepository;

class GatewayTriggerTypeRepositoryEloquent extends BaseRepositoryEloquent implements IGatewayTriggerTypeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return GatewayTriggerType::class;
    }
}