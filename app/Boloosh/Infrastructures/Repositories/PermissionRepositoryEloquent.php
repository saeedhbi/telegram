<?php

namespace Boloosh\Infrastructures\Repositories;

use Boloosh\Infrastructures\Models\Permission;
use Boloosh\Infrastructures\Interfaces\IPermissionRepository;

class PermissionRepositoryEloquent extends BaseRepositoryEloquent implements IPermissionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Permission::class;
    }
}