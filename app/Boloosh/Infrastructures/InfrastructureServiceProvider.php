<?php

namespace Boloosh\Infrastructures;

use Boloosh\Infrastructures\Interfaces\IGatewayRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayServiceRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayServiceUpdateRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayServiceUserRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerDefaultRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerIndexRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerResultRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerTypeRepository;
use Boloosh\Infrastructures\Interfaces\IUserRepository;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTriggerIndex;
use Boloosh\Infrastructures\Models\GatewayTriggerResult;
use Boloosh\Infrastructures\Observers\GatewayServiceUpdateObserver;
use Boloosh\Infrastructures\Observers\GatewayTriggerIndexObserver;
use Boloosh\Infrastructures\Observers\GatewayTriggerResultObserver;
use Boloosh\Infrastructures\Repositories\GatewayRepositoryEloquent;
use Boloosh\Infrastructures\Repositories\GatewayServiceRepositoryEloquent;
use Boloosh\Infrastructures\Repositories\GatewayServiceUpdateRepositoryEloquent;
use Boloosh\Infrastructures\Repositories\GatewayServiceUserRepositoryEloquent;
use Boloosh\Infrastructures\Repositories\GatewayTriggerDefaultRepositoryEloquent;
use Boloosh\Infrastructures\Repositories\GatewayTriggerRepositoryEloquent;
use Boloosh\Infrastructures\Repositories\GatewayTriggerResultRepositoryEloquent;
use Boloosh\Infrastructures\Repositories\GatewayTriggerTypeRepositoryEloquent;
use Boloosh\Infrastructures\Repositories\GatewayTriggerIndexRepositoryEloquent;
use Boloosh\Infrastructures\Repositories\UserRepositoryEloquent;
use Illuminate\Support\ServiceProvider;

class InfrastructureServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap application services.
     *
     * @return void
     */
    public function boot()
    {
        GatewayServiceUpdate::observe(GatewayServiceUpdateObserver::class);
        GatewayTriggerIndex::observe(GatewayTriggerIndexObserver::class);
        GatewayTriggerResult::observe(GatewayTriggerResultObserver::class);
    }

    /**
     * Register the service provider
     */
    public function register()
    {
        $this->app->bind(IUserRepository::class, UserRepositoryEloquent::class);
        $this->app->bind(IGatewayRepository::class, GatewayRepositoryEloquent::class);
        $this->app->bind(IGatewayServiceRepository::class, GatewayServiceRepositoryEloquent::class);
        $this->app->bind(IGatewayServiceUserRepository::class, GatewayServiceUserRepositoryEloquent::class);
        $this->app->bind(IGatewayServiceUpdateRepository::class, GatewayServiceUpdateRepositoryEloquent::class);
        $this->app->bind(IGatewayTriggerRepository::class, GatewayTriggerRepositoryEloquent::class);
        $this->app->bind(IGatewayTriggerTypeRepository::class, GatewayTriggerTypeRepositoryEloquent::class);
        $this->app->bind(IGatewayTriggerIndexRepository::class, GatewayTriggerIndexRepositoryEloquent::class);
        $this->app->bind(IGatewayTriggerResultRepository::class, GatewayTriggerResultRepositoryEloquent::class);
        $this->app->bind(IGatewayTriggerDefaultRepository::class, GatewayTriggerDefaultRepositoryEloquent::class);
    }
}