<?php

namespace Boloosh\Infrastructures\Observers;

use Boloosh\Core\Commands\GatewayService\GetGatewayServiceByIdCommand;
use Boloosh\Core\Commands\GatewayServiceUpdate\GetGatewayServiceUpdateByIdCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTriggerIndex;
use Boloosh\Services\Gateway\GatewayServiceManager;
use Boloosh\Services\Gateway\Services\IGatewayService;
use Illuminate\Foundation\Bus\DispatchesJobs;

class GatewayTriggerIndexObserver
{

    use DispatchesJobs;


    /**
     * Listen to the GatewayTriggerResult created event.
     *
     * @param GatewayTriggerIndex $gatewayTriggerIndex
     *
     * @return mixed
     */
    public function created(GatewayTriggerIndex $gatewayTriggerIndex)
    {
        try {
            if ($gatewayTriggerIndex->type != GatewayTriggerIndex::FIRE_OBSERVE) {
                return false;
            }

            /** @var GatewayServiceManager $gatewayServiceManager */
            $gatewayServiceManager = app(GatewayServiceManager::class);

            /** @var GatewayServiceUpdate $gatewayServiceUpdate */
            $gatewayServiceUpdate = $this->dispatch(new GetGatewayServiceUpdateByIdCommand($gatewayTriggerIndex->update_id));

            $gatewayService = $this->dispatch(new GetGatewayServiceByIdCommand($gatewayServiceUpdate->service_id));

            /** @var IGatewayService $service */
            $service = $gatewayServiceManager->getGatewayService($gatewayService);

            return $service->handleNextServiceResponse($gatewayTriggerIndex);
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }


    /**
     * Listen to the GatewayTriggerResult updated event.
     *
     * @param GatewayTriggerIndex $gatewayTriggerIndex
     *
     * @return mixed
     */
    public function updated(GatewayTriggerIndex $gatewayTriggerIndex)
    {
        try {
            if($gatewayTriggerIndex->type != GatewayTriggerIndex::FIRE_OBSERVE) {
                return false;
            }

            /** @var GatewayServiceManager $gatewayServiceManager */
            $gatewayServiceManager = app(GatewayServiceManager::class);

            /** @var GatewayServiceUpdate $gatewayServiceUpdate */
            $gatewayServiceUpdate = $this->dispatch(new GetGatewayServiceUpdateByIdCommand($gatewayTriggerIndex->update_id));

            $gatewayService = $this->dispatch(new GetGatewayServiceByIdCommand($gatewayServiceUpdate->service_id));

            /** @var IGatewayService $service */
            $service = $gatewayServiceManager->getGatewayService($gatewayService);

            return $service->handleNextServiceResponse($gatewayTriggerIndex);
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }
}