<?php

namespace Boloosh\Infrastructures\Observers;

use Boloosh\Core\Commands\GatewayService\GetGatewayServiceByIdCommand;
use Boloosh\Core\Commands\GatewayServiceUpdate\DeleteGatewayServiceUpdateJobsCommand;
use Boloosh\Core\Commands\GatewayServiceUpdate\GetGatewayServiceUpdateByIdCommand;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerResultRepository;
use Boloosh\Infrastructures\Models\GatewayService;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTriggerResult;
use Boloosh\Infrastructures\Models\GatewayTriggerType;
use Boloosh\Services\Gateway\GatewayServiceManager;
use Boloosh\Services\Gateway\Services\IGatewayService;
use Boloosh\Services\Gateway\Services\Telegram\TelegramDeleteUpdateCommand;
use DateTime;
use DateTimeZone;
use Illuminate\Foundation\Bus\DispatchesJobs;

class GatewayTriggerResultObserver
{

    use DispatchesJobs;

    /**
     * Listen to the GatewayServiceUpdate updated event.
     *
     * @param GatewayTriggerResult $gatewayTriggerResult
     * @return mixed
     */
    public function updated(GatewayTriggerResult $gatewayTriggerResult)
    {
        if ($gatewayTriggerResult->status == GatewayTriggerResult::PUBLISH) {

            /** @var IGatewayTriggerResultRepository $gatewayTriggerResultRepository */
            $gatewayTriggerResultRepository = app(IGatewayTriggerResultRepository::class);

            /** @var GatewayServiceUpdate $gatewayServiceUpdate */
            $gatewayServiceUpdate = $this->dispatch(new GetGatewayServiceUpdateByIdCommand($gatewayTriggerResult->gateway_service_update_id));

            if ($gatewayTriggerResultRepository->getPayload($gatewayTriggerResult, GatewayTriggerType::SET_DELETE_TIME)) {

                $delayTime = DateTime::createFromFormat(
                    'Y-m-d H:i:s',
                    $gatewayTriggerResultRepository->getPayload($gatewayTriggerResult, GatewayTriggerType::SET_DELETE_TIME),
                    new DateTimeZone('Asia/Tehran')
                )->setTimezone(new DateTimeZone('UTC'));

                if ($gatewayServiceUpdate->gateway->destination_id == GatewayService::TELEGRAM) {
                    $job = (new TelegramDeleteUpdateCommand($gatewayServiceUpdate, $gatewayTriggerResultRepository->getPayload($gatewayTriggerResult, GatewayTriggerResult::PUBLISH_CONVERSATION_ID), $gatewayTriggerResultRepository->getPayload($gatewayTriggerResult, GatewayTriggerResult::PUBLISH_MESSAGE_ID), $gatewayTriggerResultRepository->getPayload($gatewayTriggerResult, GatewayTriggerResult::PUBLISH_MESSAGE_ATTACH_ID)))->delay($delayTime);

                    $this->dispatch($job);
                }

            }
        }
    }
}