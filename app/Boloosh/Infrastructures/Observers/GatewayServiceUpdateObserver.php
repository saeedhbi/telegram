<?php

namespace Boloosh\Infrastructures\Observers;

use Boloosh\Core\Commands\GatewayService\GetGatewayServiceByIdCommand;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Services\Gateway\GatewayServiceManager;
use Boloosh\Services\Gateway\Services\IGatewayService;
use Illuminate\Foundation\Bus\DispatchesJobs;

class GatewayServiceUpdateObserver
{

    use DispatchesJobs;


    /**
     * Listen to the GatewayServiceUpdate created event.
     *
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     *
     * @return mixed
     */
    public function created(GatewayServiceUpdate $gatewayServiceUpdate)
    {
        /** @var GatewayServiceManager $gatewayServiceManager */
        $gatewayServiceManager = app(GatewayServiceManager::class);

        $gatewayService = $this->dispatch(new GetGatewayServiceByIdCommand($gatewayServiceUpdate->service_id));

        /** @var IGatewayService $service */
        $service = $gatewayServiceManager->getGatewayService($gatewayService);

        return $service->createNewRequestResponse($gatewayServiceUpdate);
    }


    /**
     * Listen to the GatewayServiceUpdate updated event.
     *
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     *
     * @return mixed
     */
    public function updated(GatewayServiceUpdate $gatewayServiceUpdate)
    {
        if ($gatewayServiceUpdate->status == GatewayServiceUpdate::SENT) {
            // start workflow

            /** @var GatewayServiceManager $gatewayServiceManager */
            $gatewayServiceManager = app(GatewayServiceManager::class);

            $gatewayService = $this->dispatch(new GetGatewayServiceByIdCommand($gatewayServiceUpdate->service_id));

            /** @var IGatewayService $service */
            $service = $gatewayServiceManager->getGatewayService($gatewayService);

            return $service->handleEndingRequestResponse($gatewayServiceUpdate);
        }
    }
}