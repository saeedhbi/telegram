<?php
namespace Boloosh\Services\CommandGenerator;

use Illuminate\Support\ServiceProvider;

class CommandGeneratorServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishConfig();
    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerCommandGeneratorRepository();
    }


    /**
     * Register Repoist Commands.
     *
     * @return void
     */
    private function registerCommandGeneratorRepository()
    {
        $this->app->singleton('command.boloosh.generatorRepository', function ($app) {
            return $app['Boloosh\Services\CommandGenerator\Commands\RepositoryMakeCommand'];
        });
        $this->commands('command.boloosh.generatorRepository');
    }


    private function publishConfig()
    {
        $this->publishes([
            __DIR__ . '/config/commandGenerator.php' => config_path('commandGenerator.php'),
        ]);
    }
}

