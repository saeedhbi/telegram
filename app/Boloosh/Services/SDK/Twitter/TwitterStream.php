<?php

namespace Boloosh\Services\SDK\Twitter;

use App\Jobs\ProcessTweet;
use Illuminate\Foundation\Bus\DispatchesJobs;
use OauthPhirehose;

class TwitterStream extends OauthPhirehose
{

    use DispatchesJobs;

    /**
     * This is the one and only method that must be implemented additionally. As per the streaming API documentation,
     * statuses should NOT be processed within the same process that is performing collection
     *
     * @param string $status
     */
    public function enqueueStatus($status)
    {
        $this->dispatch(new ProcessTweet($status));
    }
}