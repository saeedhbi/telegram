<?php

namespace Boloosh\Services\SDK\Telegram;

use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTriggerType;
use Telegram\Bot\Api as TelegramApi;
use Telegram\Bot\Keyboard\Keyboard;

class TelegramService implements ITelegramService
{

    /**
     * @var TelegramApi $api
     */
    public $api;

    /**
     * @var string
     */
    private $token;

    /**
     * @param $token
     *
     * @return mixed
     */
    public function make($token)
    {
        $this->token = $token;
        $this->api = new TelegramApi($token);
    }

    /**
     * @param $options
     *
     * @return mixed|\Telegram\Bot\Objects\Message
     */
    public function sendMessage($options)
    {
        return $this->api->sendMessage($options);
    }

    /**
     * @param $options
     * @return \Telegram\Bot\Objects\Message
     */
    public function sendPhoto($options)
    {
        return $this->api->sendPhoto($options);
    }

    /**
     * @param $options
     * @return \Telegram\Bot\Objects\Message
     */
    public function sendVoice($options)
    {
        return $this->api->sendVoice($options);
    }

    /**
     * @param $options
     * @return \Telegram\Bot\Objects\Message
     */
    public function sendAudio($options)
    {
        return $this->api->sendAudio($options);
    }

    /**
     * @param $options
     * @return \Telegram\Bot\Objects\Message
     */
    public function sendSticker($options)
    {
        return $this->api->sendSticker($options);
    }

    /**
     * @param $options
     * @return \Telegram\Bot\Objects\Message
     */
    public function sendDocument($options)
    {
        return $this->api->sendDocument($options);
    }

    /**
     * @description Current TelegramApi has not this feature
     *
     * @param $options
     * @return mixed
     */
    public function deleteMessage($options)
    {
        try {
            curl_request('https://api.telegram.org/bot' . $this->token . '/deleteMessage?chat_id=' . $options['chat_id'] . '&message_id=' . $options['message_id']);
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $layout
     * @param bool $forceReply
     * @return Keyboard
     */
    public function inlineKeyboard($layout, $forceReply = false)
    {
        $keyboard = Keyboard::make()->inline();

        foreach ($layout as $item) {
            $keyboard = $keyboard->row(Keyboard::inlineButton($item));
        }

        return $keyboard;
    }

    /**
     * @param $layout
     * @param bool $forceReply
     * @return Keyboard
     */
    public function keyboard($layout, $forceReply = false)
    {
        return Keyboard::make([
            'keyboard' => json_decode($layout),
            'resize_keyboard' => true,
            'one_time_keyboard' => true,
            'selective' => $forceReply
        ]);
    }

    /**
     * @param GatewayTriggerType $trigger_type
     *
     * @return mixed
     */
    public function detectKeyboard(GatewayTriggerType $trigger_type)
    {
        if ($trigger_type->has_keyboard) {
            if ($trigger_type->keyboard_type == GatewayTriggerType::INLINE_KEYBOARD) {
                $reply_markup = Keyboard::make()->inline()->row(Keyboard::inlineButton(['text' => 'Cancel', 'callback_data' => 'cancel']));
            } else {
                $reply_markup = $this->keyboard($trigger_type->keyboard_layout);
            }

        } else {
            $reply_markup = $this->hideKeyboard();
        }

        return $reply_markup;
    }

    /**
     * @return mixed
     */
    public function hideKeyboard()
    {
        return Keyboard::hide();
    }

    /**
     * @param $fileId
     * @return \Telegram\Bot\Objects\File
     */
    public function getFile($fileId)
    {
        return $this->api->getFile(['file_id' => $fileId]);
    }

    /**
     * @param $token
     * @param $filePath
     * @return string
     */
    public static function getFilePath($token, $filePath)
    {
        return 'https://api.telegram.org/file/bot' . $token . '/' . $filePath;
    }

    /**
     * @param $command
     * @return string
     */
    public function command($command)
    {
        return '/' . $command;
    }

    /**
     * @param $url
     *
     * @return string
     */
    public function getMessageIdByPostLink($url)
    {
        if (strpos($url, 'http') > -1) {
            $segments = explode('/', $url);

            return end($segments);
        }

        return $url;
    }

    /**
     * @param $body
     *
     * @return mixed
     */
    public function parseHtml($body)
    {
        $body = $this->linkifyTextMessage($body);

        $italicReg = '/\~~(.*?)\~~/';
        $boldReg = '/\##(.*?)\##/';

        $array = collect();

        if (preg_match_all($boldReg, $body, $match)) {
            foreach ($match[0] as $index => $item) {
                    $array->put($item, '<b>' . $match[1][$index] . '</b>');
            }
        }

        if (preg_match_all($italicReg, $body, $match)) {
            foreach ($match[0] as $index => $item) {
                    $array->put($item, '<i>' . $match[1][$index] . '</i>');
            }
        }

        $explode = explode('%%%', $body);

        $result = strtr($explode[0], $array->toArray());

        return $result;
    }

    /**
     * @param $body
     *
     * @return string
     */
    public function linkifyTextMessage($body)
    {
        $strReg = '/\*(.*?)\*/';
        $urlReg = '"\b((http|https|ftp|ftps)?://\S+)"';

        $array = collect();

        if (preg_match_all($strReg, $body, $strRegMatches)) {
            preg_match_all($urlReg, $body, $urlRegMatches);
            foreach ($strRegMatches[1] as $index => $string) {
                $array->put("*" . $string . "*", "<a href='" . $urlRegMatches[1][$index] . "'>" . $string . "</a>");
                $array->put("" . $urlRegMatches[1][$index] . "", "");
            }
        }

        return strtr($body, $array->toArray());
    }
}