<?php

namespace Boloosh\Services\SDK\Telegram;

use Illuminate\Support\ServiceProvider;

class TelegramServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider
     */
    public function register()
    {
        $this->app->singleton(ITelegramService::class, TelegramService::class);
    }
}