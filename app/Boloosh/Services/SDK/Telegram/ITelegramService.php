<?php

namespace Boloosh\Services\SDK\Telegram;

use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTriggerType;

interface ITelegramService
{

    /**
     * @param $token
     *
     * @return mixed
     */
    public function make($token);

    /**
     * @param $options
     *
     * @return mixed
     */
    public function sendMessage($options);

    /**
     * @param $options
     *
     * @return mixed
     */
    public function sendPhoto($options);

    /**
     * @param $options
     * @return mixed
     */
    public function sendVoice($options);

    /**
     * @param $options
     * @return mixed
     */
    public function sendAudio($options);

    /**
     * @param $options
     * @return mixed
     */
    public function sendSticker($options);

    /**
     * @param $options
     * @return mixed
     */
    public function sendDocument($options);

    /**
     * @param $options
     * @return mixed
     */
    public function deleteMessage($options);

    /**
     * @param $layout
     * @param bool $forceReply
     * @return mixed
     */
    public function inlineKeyboard($layout, $forceReply = false);

    /**
     * @param $layout
     * @param bool $forceReply
     * @return mixed
     */
    public function keyboard($layout, $forceReply = false);

    /**
     * @param GatewayTriggerType $trigger_type
     *
     * @return mixed
     */
    public function detectKeyboard(GatewayTriggerType $trigger_type);

    /**
     * @return mixed
     */
    public function hideKeyboard();

    /**
     * @param $fileId
     * @return mixed
     */
    public function getFile($fileId);

    /**
     * @param $command
     * @return mixed
     */
    public function command($command);

    /**
     * @param $url
     * @return mixed
     */
    public function getMessageIdByPostLink($url);

    /**
     * @param $body
     * @return mixed
     */
    public function parseHtml($body);

    /**
     * @param $body
     * @return mixed
     */
    public function linkifyTextMessage($body);

}