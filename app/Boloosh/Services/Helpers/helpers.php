<?php

if (!function_exists('bAbort')) {
    function bAbort($message = null, $error = null)
    {
        return view('v1.errors.error', compact('message', 'error'));
    }
}

if (!function_exists('telegram_logger')) {
    function telegram_logger($message = null)
    {
        try {
            $data = [
                'exception' => '',
                'message' => $message,
                'request' => ''
            ];

            return new \Boloosh\Exceptions\TelegramLog($data);
        } catch (\Exception $ex) {
            logger($ex->getMessage());
        }
    }
}

if (!function_exists('curl_request')) {
    function curl_request($url)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url
        ));

        curl_exec($curl);
        curl_close($curl);
    }
}