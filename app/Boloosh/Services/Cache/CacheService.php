<?php

namespace Boloosh\Services\Cache;

use Boloosh\Exceptions\CacheNotFoundException;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Cache;

class CacheService implements ICacheService
{

    protected $old = '120';


    /**
     * Determine if an item exists in the cache.
     *
     * @param  string $key
     *
     * @return bool
     */
    public function has($key)
    {
        return Cache::has($key);
    }


    /**
     * Retrieve an item from the cache by key.
     *
     * @param  string $key
     * @param  mixed  $default
     *
     * @throws CacheNotFoundException
     * @return mixed
     */
    public function get($key, $default = null)
    {
        if ( ! $this->has($key)) {
            throw new CacheNotFoundException;
        }

        return Cache::get($key, $default);
    }


    /**
     * Retrieve an item from the cache and delete it.
     *
     * @param  string $key
     * @param  mixed  $default
     *
     * @return mixed
     */
    public function pull($key, $default = null)
    {
        return Cache::pull($key, $default);
    }


    /**
     * Store an item in the cache.
     *
     * @param  string              $key
     * @param  mixed               $value
     * @param  \DateTime|float|int $minutes
     *
     * @return void
     */
    public function put($key, $value, $minutes = null)
    {
        if (is_null($minutes)) {
            $minutes = Carbon::now()->addSeconds($this->old);
        }

        return Cache::put($key, $value, $minutes);
    }


    /**
     * Store an item in the cache if the key does not exist.
     *
     * @param  string              $key
     * @param  mixed               $value
     * @param  \DateTime|float|int $minutes
     *
     * @return bool
     */
    public function add($key, $value, $minutes = null)
    {
        if (is_null($minutes)) {
            $minutes = Carbon::now()->addSeconds($this->old);
        }

        return Cache::add($key, $value, $minutes);
    }


    /**
     * Increment the value of an item in the cache.
     *
     * @param  string $key
     * @param  mixed  $value
     *
     * @return int|bool
     */
    public function increment($key, $value = 1)
    {
        return Cache::increment($key, $value);
    }


    /**
     * Decrement the value of an item in the cache.
     *
     * @param  string $key
     * @param  mixed  $value
     *
     * @return int|bool
     */
    public function decrement($key, $value = 1)
    {
        return Cache::decrement($key, $value);
    }


    /**
     * Store an item in the cache indefinitely.
     *
     * @param  string $key
     * @param  mixed  $value
     *
     * @return void
     */
    public function forever($key, $value)
    {
        return Cache::forever($key, $value);
    }


    /**
     * Get an item from the cache, or store the default value.
     *
     * @param  string              $key
     * @param  \DateTime|float|int $minutes
     * @param  \Closure            $callback
     *
     * @return mixed
     */
    public function remember($key, $minutes = null, Closure $callback)
    {
        if (is_null($minutes)) {
            $minutes = Carbon::now()->addSeconds($this->old);
        }

        return Cache::remember($key, $minutes, $callback);
    }


    /**
     * Get an item from the cache, or store the default value forever.
     *
     * @param  string   $key
     * @param  \Closure $callback
     *
     * @return mixed
     */
    public function sear($key, Closure $callback)
    {
        return Cache::sear($key, $callback);
    }


    /**
     * Get an item from the cache, or store the default value forever.
     *
     * @param  string   $key
     * @param  \Closure $callback
     *
     * @return mixed
     */
    public function rememberForever($key, Closure $callback)
    {
        return Cache::rememberForever($key, $callback);
    }


    /**
     * Remove an item from the cache.
     *
     * @param  string $key
     *
     * @return bool
     */
    public function forget($key)
    {
        return Cache::forget($key);
    }
}