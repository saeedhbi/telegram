<?php

namespace Boloosh\Services\Cache;

use Illuminate\Support\ServiceProvider;

class CacheServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider
     */
    public function register()
    {
        $this->app->singleton(ICacheService::class, CacheService::class);
    }
}