<?php

namespace Boloosh\Services\Queue;

use Illuminate\Support\Facades\Redis;

class QueueService implements IQueueService
{
    /**
     * @var
     */
    private $index;

    /**
     * Set delayed jobs index
     *
     * @return QueueService
     */
    public function delayed()
    {
        $this->index = 'queues:default:delayed';

        return $this;
    }

    /**
     * Retrieve all queued jobs
     *
     * @return mixed
     */
    public function all()
    {
        return Redis::connection()->zrange($this->index, 0, -1);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findJobById($id)
    {
        $jobs = $this->all();

        foreach ($jobs as $job) {
            $decodedJob = json_decode($job);

            if ($decodedJob->id == $id) {
                return $job;
            }
        }
    }

    /**
     * @param $job
     * @return mixed
     */
    public function delete($job)
    {
        return !is_null($job) ? Redis::connection()->zrem($this->index, $job) : false;
    }

}