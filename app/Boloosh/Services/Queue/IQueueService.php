<?php

namespace Boloosh\Services\Queue;

interface IQueueService
{

    /**
     * Set delayed jobs index
     *
     * @return IQueueService
     */
    public function delayed();

    /**
     * Retrieve all queued jobs
     *
     * @return mixed
     */
    public function all();
    /**
     * @param $id
     * @return mixed
     */
    public function findJobById($id);

    /**
     * @param $job
     * @return mixed
     */
    public function delete($job);

}