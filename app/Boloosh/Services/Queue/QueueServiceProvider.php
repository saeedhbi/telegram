<?php

namespace Boloosh\Services\Queue;

use Illuminate\Support\ServiceProvider;

class QueueServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider
     */
    public function register()
    {
        $this->app->singleton(IQueueService::class, QueueService::class);
    }
}