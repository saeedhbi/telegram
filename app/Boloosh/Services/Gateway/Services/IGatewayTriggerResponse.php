<?php

namespace Boloosh\Services\Gateway\Services;

interface IGatewayTriggerResponse
{

    /**
     * @return mixed
     */
    public function callTrigger();
}