<?php

namespace Boloosh\Services\Gateway\Services\Telegram;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Services\Cache\ICacheService;
use Boloosh\Services\SDK\Telegram\ITelegramService;

class SendRequestInformationToConversation extends BaseCommand
{

    /**
     * @var GatewayServiceUpdate
     */
    private $gatewayServiceUpdate;


    /**
     * SendRequestInformationToConversation constructor.
     *
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     */
    public function __construct(GatewayServiceUpdate $gatewayServiceUpdate)
    {

        $this->gatewayServiceUpdate = $gatewayServiceUpdate;
    }


    /**
     * @param ITelegramService $telegramService
     *
     * @param ICacheService    $cacheService
     *
     * @return mixed
     */
    public function handle(ITelegramService $telegramService, ICacheService $cacheService)
    {
        try {
            /** @var Gateway $gateway */
            $gateway = $cacheService->get('gateway');

            $telegramService->make($gateway->token);

            $options = [
                'chat_id'             => $this->gatewayServiceUpdate->conversation_id,
                'text'                => trans('common.new_request', [ 'requestId' => $this->gatewayServiceUpdate->id ]),
                'parse_mode'          => 'HTML',
                'reply_to_message_id' => $this->gatewayServiceUpdate->message_id
            ];

            return $telegramService->sendMessage($options);
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}