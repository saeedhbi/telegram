<?php

namespace Boloosh\Services\Gateway\Services\Telegram;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\GatewayServiceUpdateTypeException;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Illuminate\Support\Collection;
use Telegram\Bot\Objects\Message;

class TelegramFindUpdateTypeCommand extends BaseCommand
{

    /**
     * @var
     */
    private $input;

    /**
     * TelegramFindUpdateTypeCommand constructor.
     *
     * @param Message $input
     */
    public function __construct(Message $input)
    {
        $this->input = $input;
    }

    /**
     * @return string
     */
    public function handle()
    {
        if ($this->input->isType('text')) {
            return GatewayServiceUpdate::TEXT;
        }

        if ($this->input->isType('document')) {
            return GatewayServiceUpdate::DOCUMENT;
        }

        if ($this->input->isType('photo')) {
            return GatewayServiceUpdate::PHOTO;
        }

        if ($this->input->isType('audio')) {
            return GatewayServiceUpdate::AUDIO;
        }

        if ($this->input->isType('video')) {
            return GatewayServiceUpdate::VIDEO;
        }

        if ($this->input->isType('voice')) {
            return GatewayServiceUpdate::VOICE;
        }

        if ($this->input->isType('game')) {
            return GatewayServiceUpdate::GAME;
        }

        if ($this->input->isType('sticker')) {
            return GatewayServiceUpdate::STICKER;
        }

        if ($this->input->isType('location')) {
            return GatewayServiceUpdate::LOCATION;
        }

        if ($this->input->isType('contact')) {
            return GatewayServiceUpdate::CONTACT;
        }

        if ($this->input->isType('venue')) {
            return GatewayServiceUpdate::VENUE;
        }

        throw new GatewayServiceUpdateTypeException;
    }

}

