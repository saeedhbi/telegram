<?php

namespace Boloosh\Services\Gateway\Services\Telegram;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AccessDeniedException;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Exceptions\TelegramBotAccessDeniedException;
use Boloosh\Infrastructures\Interfaces\IGatewayServiceRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayServiceUserRepository;
use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Infrastructures\Models\GatewayService;
use Boloosh\Infrastructures\Models\GatewayServiceUser;
use Boloosh\Services\Gateway\Services\IGatewayServiceUpdateParser;
use Boloosh\Services\Gateway\Services\Telegram\TelegramUpdateParser;
use Boloosh\Services\SDK\Telegram\ITelegramService;

class TelegramUserAuthenticationCommand extends BaseCommand
{

    /**
     * @var IGatewayServiceUpdateParser
     */
    private $request;
    /**
     * @var Gateway
     */
    private $gateway;

    /**
     * TelegramUserAuthenticationCommand constructor.
     *
     * @param Gateway $gateway
     * @param IGatewayServiceUpdateParser $request
     */
    public function __construct(Gateway $gateway, IGatewayServiceUpdateParser $request)
    {

        $this->request = $request;
        $this->gateway = $gateway;
    }

    /**
     * @param IGatewayServiceUserRepository $gatewayServiceUserRepository
     * @param ITelegramService $telegramService
     * @return mixed
     */
    public function handle(IGatewayServiceUserRepository $gatewayServiceUserRepository, ITelegramService $telegramService)
    {
        try {
            /** @var GatewayServiceUser $user */
            $user = $gatewayServiceUserRepository->findWhere([['service_id', '=', $this->gateway->service_id], ['service_user_id', '=', $this->request->getSenderId()]])->first();

            $text = null;

            if(!is_null($user)) {
                if($user->status == GatewayServiceUser::ACTIVE) {
                    return true;
                }

                if($user->status == GatewayServiceUser::PENDING) {
                    $text = 'You have requested to access bot before. Bot owner will overview your request.';
                }

                if($user->status == GatewayServiceUser::DISABLED) {
                    $text = 'Your request to access bot is suspended. You can not send any data.';
                }
            }

            if(is_null($user)) {
                $gatewayServiceUser = new GatewayServiceUser();

                $gatewayServiceUser->service_id = $this->gateway->service_id;
                $gatewayServiceUser->service_user_id = $this->request->getSenderId();
                $gatewayServiceUser->service_user_username = $this->request->getSender()->getUsername();
                $gatewayServiceUser->status = GatewayServiceUser::PENDING;

                $gatewayServiceUserRepository->save($gatewayServiceUser);

                $text = 'You have not access to this gateway. Bot owner will overview your request to get access.';
            }

            $telegramService->make($this->gateway->token);

            $options = [
                'chat_id' => $this->request->getConversationId(),
                'text'    => $text
            ];

            $telegramService->sendMessage($options);

            throw new TelegramBotAccessDeniedException($text, null, null, $this->request->getPayloads());
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}