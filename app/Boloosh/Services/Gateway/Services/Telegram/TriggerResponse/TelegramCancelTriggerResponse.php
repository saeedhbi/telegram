<?php

namespace Boloosh\Services\Gateway\Services\Telegram\TriggerResponse;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Infrastructures\Interfaces\IGatewayServiceUpdateRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerResultRepository;
use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTriggerResult;
use Boloosh\Infrastructures\Models\GatewayTriggerType;
use Boloosh\Services\Cache\ICacheService;
use Boloosh\Services\Gateway\Services\Telegram\TelegramTriggerCommand;
use Boloosh\Services\Queue\IQueueService;
use Boloosh\Services\SDK\Telegram\ITelegramService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

class TelegramCancelTriggerResponse extends TelegramTriggerCommand
{

    /**
     * @var
     */
    private $conversationId;

    /**
     * @var GatewayTriggerType
     */
    private $gatewayTriggerType;

    /**
     * @var
     */
    private $requestId;

    /**
     * TelegramCancelTriggerResponse constructor.
     *
     * @param GatewayTriggerType $gatewayTriggerType
     * @param                    $conversationId
     * @param                    $requestId
     */
    public function __construct(GatewayTriggerType $gatewayTriggerType, $conversationId, $requestId)
    {
        $this->gatewayTriggerType = $gatewayTriggerType;
        $this->conversationId = $conversationId;
        $this->requestId = $requestId;
    }

    /**
     * @param ICacheService $cacheService
     * @param ITelegramService $telegramService
     * @param IQueueService $queueService
     * @param IGatewayServiceUpdateRepository $gatewayServiceUpdateRepository
     *
     * @param IGatewayTriggerResultRepository $gatewayTriggerResultRepository
     * @return \Telegram\Bot\Objects\Message
     */
    public function handle(ICacheService $cacheService, ITelegramService $telegramService, IQueueService $queueService, IGatewayServiceUpdateRepository $gatewayServiceUpdateRepository, IGatewayTriggerResultRepository $gatewayTriggerResultRepository)
    {
        /** @var Gateway $gateway */
        $gateway = $cacheService->get('gateway');

        /** @var ITelegramService $telegram */
        $telegramService->make($gateway->token);

        $options = [
            'chat_id' => $this->conversationId,
            'text' => trans('triggers.' . GatewayTriggerType::SET_CANCEL,
                ['request' => is_null($this->requestId) ? trans('common.your_requests') : trans('common.your_request', ['request' => $this->requestId])]),
        ];

        DB::beginTransaction();
        try {
            if (!is_null($this->requestId)) {
                $gatewayServiceUpdates = $gatewayServiceUpdateRepository->findWhere([['id', '=', $this->requestId], ['service_id', '=', $gateway->service_id]], ['id', 'status']);

                $gatewayTriggerResult = $gatewayTriggerResultRepository->findByUpdateId($gatewayServiceUpdates->first()->id);

                if (!is_null($gatewayTriggerResult)) {
                    $job = $queueService->delayed()->findJobById($gatewayTriggerResult->getJobId());

                    $queueService->delayed()->delete($job);
                }
            } else {
                $pendingGatewayTriggerResults = $gatewayTriggerResultRepository->findWhere([['status', '=', GatewayTriggerResult::PENDING], ['gateway_id', '=', $gateway->id]]);

                /** @var GatewayTriggerResult $gatewayTriggerResult */
                foreach ($pendingGatewayTriggerResults as $gatewayTriggerResult) {
                    $gatewayTriggerResult->status = GatewayTriggerResult::CANCELED;
                    $gatewayTriggerResultRepository->save($gatewayTriggerResult);

                    if (!is_null($gatewayTriggerResult)) {
                        $job = $queueService->delayed()->findJobById($gatewayTriggerResult->getJobId());

                        $queueService->delayed()->delete($job);
                    }
                }

                $gatewayServiceUpdatesId = $pendingGatewayTriggerResults->pluck('gateway_service_update_id')->toArray();

                $gatewayServiceUpdates = $gatewayServiceUpdateRepository->findWhereIn('id', $gatewayServiceUpdatesId);
            }

            foreach ($gatewayServiceUpdates as $gatewayServiceUpdate) {
                if ($gatewayServiceUpdate->status == GatewayServiceUpdate::PENDING) {
                    $gatewayServiceUpdate->status = GatewayServiceUpdate::CANCELED;
                }

                $gatewayServiceUpdateRepository->save($gatewayServiceUpdate);
            }

            $options['reply_markup'] = $telegramService->detectKeyboard($this->gatewayTriggerType);

            $telegramService->sendMessage($options);

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();

            $options = [
                'chat_id' => $this->conversationId,
                'text' => trans('triggers.failed.' . GatewayTriggerType::SET_CANCEL,
                    ['request' => is_null($this->requestId) ? trans('common.your_requests') : trans('common.your_request', ['request' => $this->requestId])]),
            ];

            $options['reply_markup'] = $telegramService->detectKeyboard($this->gatewayTriggerType);

            return $telegramService->sendMessage($options);
        }
    }

}