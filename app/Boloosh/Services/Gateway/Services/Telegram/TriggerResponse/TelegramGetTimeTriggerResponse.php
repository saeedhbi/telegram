<?php

namespace Boloosh\Services\Gateway\Services\Telegram\TriggerResponse;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Infrastructures\Models\GatewayTrigger;
use Boloosh\Infrastructures\Models\GatewayTriggerType;
use Boloosh\Services\Cache\ICacheService;
use Boloosh\Services\Gateway\Services\Telegram\TelegramTriggerCommand;
use Boloosh\Services\SDK\Telegram\ITelegramService;
use Illuminate\Contracts\Queue\ShouldQueue;

class TelegramGetTimeTriggerResponse extends TelegramTriggerCommand
{

    /**
     * @var
     */
    private $conversationId;

    /**
     * @var GatewayTriggerType
     */
    private $gatewayTriggerType;


    /**
     * TelegramGetTimeTriggerResponse constructor.
     *
     * @param GatewayTriggerType $gatewayTriggerType
     * @param                    $conversationId
     */
    public function __construct(GatewayTriggerType $gatewayTriggerType, $conversationId)
    {
        $this->gatewayTriggerType = $gatewayTriggerType;
        $this->conversationId     = $conversationId;
    }


    /**
     * @param ICacheService    $cacheService
     * @param ITelegramService $telegramService
     *
     * @return \Telegram\Bot\Objects\Message
     */
    public function handle(ICacheService $cacheService, ITelegramService $telegramService)
    {
        /** @var Gateway $gateway */
        $gateway = $cacheService->get('gateway');

        $telegramService->make($gateway->token);

        $options = [
            'chat_id' => $this->conversationId,
            'text'    => trans('triggers.' . GatewayTriggerType::GET_TIME),
            'parse_mode' => 'HTML'
        ];

        $options['reply_markup'] = $this->processKeyboard($this->gatewayTriggerType, $telegramService);

        return $telegramService->sendMessage($options);
    }

}