<?php

namespace Boloosh\Services\Gateway\Services\Telegram\TriggerResponse;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Services\Cache\ICacheService;
use Boloosh\Services\Gateway\Services\Telegram\TelegramTriggerCommand;
use Boloosh\Services\SDK\Telegram\ITelegramService;

class TelegramCreateGatewayTriggerResponse extends TelegramTriggerCommand
{
    /**
     * @var
     */
    private $conversationId;

    /**
     * TelegramCreateGatewayTriggerResponse constructor.
     * @param $conversationId
     */
    public function __construct($conversationId)
    {

        $this->conversationId = $conversationId;
    }

    /**
     * @param ICacheService $cacheService
     * @param ITelegramService $telegramService
     *
     * @return mixed
     */
    public function handle(ICacheService $cacheService, ITelegramService $telegramService)
    {
        try {
            /** @var Gateway $gateway */
            $gateway = $cacheService->get('gateway');

            $telegramService->make($gateway->token);

            $options = [
                'chat_id' => $this->conversationId,
                'text' => trans('exceptions.no_trigger'),
                'parse_mode'          => 'HTML',
                'reply_markup' => $telegramService->hideKeyboard()
            ];

            return $telegramService->sendMessage($options);
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}