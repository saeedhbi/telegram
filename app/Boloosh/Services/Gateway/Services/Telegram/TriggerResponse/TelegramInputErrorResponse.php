<?php

namespace Boloosh\Services\Gateway\Services\Telegram\TriggerResponse;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\GatewayServiceInvalidReplyException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Exceptions\InvalidRequestException;
use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Services\Cache\ICacheService;
use Boloosh\Services\Gateway\Services\Telegram\TelegramTriggerCommand;
use Boloosh\Services\SDK\Telegram\ITelegramService;

class TelegramInputErrorResponse extends TelegramTriggerCommand
{
    /**
     * @var GatewayServiceUpdate
     */
    private $gatewayServiceUpdate;
    /**
     * @var
     */
    private $conversationId;
    /**
     * @var null
     */
    private $sample;
    /**
     * @var bool
     */
    private $hideKeyboard;

    /**
     * TelegramInputErrorResponse constructor.
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     * @param $conversationId
     * @param null $sample
     * @param bool $hideKeyboard
     */
    public function __construct(GatewayServiceUpdate $gatewayServiceUpdate, $conversationId, $sample = null, $hideKeyboard = false)
    {

        $this->gatewayServiceUpdate = $gatewayServiceUpdate;
        $this->conversationId = $conversationId;
        $this->sample = $sample;
        $this->hideKeyboard = $hideKeyboard;
    }

    /**
     * @param ICacheService $cacheService
     * @param ITelegramService $telegramService
     *
     * @return mixed
     */
    public function handle(ICacheService $cacheService, ITelegramService $telegramService)
    {
        try {
            /** @var Gateway $gateway */
            $gateway = $cacheService->get('gateway');

            $telegramService->make($gateway->token);

            $options = [
                'chat_id' => $this->conversationId,
                'text' => trans('exceptions.not_valid', ['sample' => $this->sample]),
                'parse_mode' => 'HTML'
            ];

            if ($this->hideKeyboard) {
                $options['reply_markup'] = $telegramService->hideKeyboard();
            }

            $telegramService->sendMessage($options);

            throw new InvalidRequestException();
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}