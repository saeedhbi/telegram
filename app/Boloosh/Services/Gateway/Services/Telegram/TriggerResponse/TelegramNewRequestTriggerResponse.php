<?php

namespace Boloosh\Services\Gateway\Services\Telegram\TriggerResponse;

use Boloosh\Core\Commands\GatewayTrigger\GetAllGatewayTriggersByIdCommand;
use Boloosh\Core\Commands\GatewayTrigger\GetGatewayTriggerByGatewayServiceUpdateCommand;
use Boloosh\Core\Commands\GatewayTriggerResult\GetCurrentGatewayTriggerIndexesCommand;
use Boloosh\Core\Commands\GatewayTriggerResult\SaveGatewayTriggerResultIndexCommand;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTriggerType;
use Boloosh\Services\Gateway\Services\IGatewayService;
use Boloosh\Services\Gateway\Services\IGatewayTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\SendRequestInformationToConversation;
use Boloosh\Services\Gateway\Services\Telegram\TelegramGatewayService;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Log;

class TelegramNewRequestTriggerResponse
{

    use DispatchesJobs;

    /**
     * @var GatewayServiceUpdate
     */
    private $gatewayServiceUpdate;

    /**
     * TelegramNewRequestTriggerResponse constructor.
     *
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     */
    public function __construct(GatewayServiceUpdate $gatewayServiceUpdate)
    {
        $this->gatewayServiceUpdate = $gatewayServiceUpdate;

        $this->_createResponse();
    }

    /**
     * Create new reply request to start conversation
     */
    private function _createResponse()
    {
        $triggers = $this->dispatch(new GetAllGatewayTriggersByIdCommand($this->gatewayServiceUpdate->gateway_id));

        $this->dispatch(new SendRequestInformationToConversation($this->gatewayServiceUpdate));

        /** @var IGatewayService $service */
        $service = app(TelegramGatewayService::class);

        $service->setRequest(json_decode($this->gatewayServiceUpdate->payloads, true));

        $currentGatewayServiceUpdateTrigger = $this->dispatch(new GetGatewayTriggerByGatewayServiceUpdateCommand($this->gatewayServiceUpdate));

        if (is_null($currentGatewayServiceUpdateTrigger)) {
            if (count($triggers) > 1) {
                $this->dispatch(new SaveGatewayTriggerResultIndexCommand(null, GatewayTriggerType::PICK_GATEWAY_TRIGGER, $this->gatewayServiceUpdate));
                return $this->dispatch(new TelegramPickGatewayTriggerResponse($this->gatewayServiceUpdate));
            } else {
                $triggerId = array_keys($triggers->toArray())[0];
                $triggerIndex = (($triggers->first())[0]->indexes)[0];
                $this->dispatch(new SaveGatewayTriggerResultIndexCommand($triggerId, $triggerIndex, $this->gatewayServiceUpdate));
            }
        }

        $service->setGatewayServiceUpdate($this->gatewayServiceUpdate);

        $service->setGatewayTriggerIndex();

        $trigger = $service->trigger();

        if ($trigger instanceof IGatewayTriggerResponse) {
            /** @var IGatewayTriggerResponse $updateHandler */
            $trigger->callTrigger();
        }
    }

}