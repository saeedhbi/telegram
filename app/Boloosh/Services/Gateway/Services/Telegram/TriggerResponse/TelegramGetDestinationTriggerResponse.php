<?php

namespace Boloosh\Services\Gateway\Services\Telegram\TriggerResponse;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerDefaultRepository;
use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Infrastructures\Models\GatewayTrigger;
use Boloosh\Infrastructures\Models\GatewayTriggerDefault;
use Boloosh\Infrastructures\Models\GatewayTriggerType;
use Boloosh\Services\Cache\ICacheService;
use Boloosh\Services\Gateway\Services\Telegram\TelegramTriggerCommand;
use Boloosh\Services\SDK\Telegram\ITelegramService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Telegram\Bot\Keyboard\Keyboard;

class TelegramGetDestinationTriggerResponse extends TelegramTriggerCommand
{

    /**
     * @var
     */
    private $conversationId;

    /**
     * @var GatewayTriggerType
     */
    private $gatewayTriggerType;

    /**
     * TelegramGetDestinationTriggerResponse constructor.
     *
     * @param GatewayTriggerType $gatewayTriggerType
     * @param                    $conversationId
     */
    public function __construct(GatewayTriggerType $gatewayTriggerType, $conversationId)
    {
        $this->gatewayTriggerType = $gatewayTriggerType;
        $this->conversationId = $conversationId;
    }

    /**
     * @param ICacheService $cacheService
     * @param ITelegramService $telegramService
     *
     * @param IGatewayTriggerDefaultRepository $gatewayTriggerDefaultRepository
     * @return \Telegram\Bot\Objects\Message
     */
    public function handle(ICacheService $cacheService, ITelegramService $telegramService, IGatewayTriggerDefaultRepository $gatewayTriggerDefaultRepository)
    {
        try {
            /** @var Gateway $gateway */
            $gateway = $cacheService->get('gateway');

            $telegramService->make($gateway->token);

            $options = [
                'chat_id' => $this->conversationId,
                'text' => trans('triggers.' . GatewayTriggerType::GET_DESTINATION),
            ];

            $gatewayTriggerDefaults = $gatewayTriggerDefaultRepository->findWhere([['gateway_id', '=', $gateway->id], ['gateway_trigger_type_id', '=', GatewayTriggerType::DESTINATION]]);

            if (count($gatewayTriggerDefaults) > 0) {
                $keyboard = [];

                if ($this->gatewayTriggerType->can_skip) {
                    array_push($keyboard, ['text' => GatewayTriggerType::COMMAND_SKIP, 'callback_data' => '/' . GatewayTriggerType::COMMAND_SKIP]);
                }

                /** @var GatewayTriggerDefault $gatewayTriggerDefault */
                foreach ($gatewayTriggerDefaults as $gatewayTriggerDefault) {
                    array_push($keyboard, ['text' => urldecode($gatewayTriggerDefault->body), 'callback_data' => urldecode($gatewayTriggerDefault->body)]);
                }

                $options['reply_markup'] = $telegramService->inlineKeyboard($keyboard);
            } else {
                $options['reply_markup'] = $this->processKeyboard($this->gatewayTriggerType, $telegramService);
            }

            $telegramService->sendMessage($options);
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }
}