<?php

namespace Boloosh\Services\Gateway\Services\Telegram\TriggerResponse;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerResultRepository;
use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTriggerConfig;
use Boloosh\Infrastructures\Models\GatewayTriggerType;
use Boloosh\Services\Cache\ICacheService;
use Boloosh\Services\Gateway\Services\IGatewayServiceUpdateParser;
use Boloosh\Services\Gateway\Services\Telegram\TelegramTriggerCommand;
use Boloosh\Services\SDK\Telegram\ITelegramService;
use Boloosh\Services\SDK\Telegram\TelegramService;

class TelegramSetAudioCoverTriggerResponse extends TelegramTriggerCommand
{
    /**
     * @var GatewayServiceUpdate
     */
    private $gatewayServiceUpdate;

    /**
     * @var GatewayTriggerType
     */
    private $gatewayTriggerType;
    /**
     * @var IGatewayServiceUpdateParser
     */
    private $request;

    /**
     * TelegramSetAudioCoverTriggerResponse constructor.
     * @param GatewayTriggerType $gatewayTriggerType
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     * @param IGatewayServiceUpdateParser $request
     */
    public function __construct(GatewayTriggerType $gatewayTriggerType, GatewayServiceUpdate $gatewayServiceUpdate, IGatewayServiceUpdateParser $request)
    {

        $this->gatewayServiceUpdate = $gatewayServiceUpdate;
        $this->gatewayTriggerType = $gatewayTriggerType;
        $this->request = $request;
    }

    /**
     * @param IGatewayTriggerResultRepository $gatewayTriggerResultRepository
     * @param ICacheService $cacheService
     * @param ITelegramService $telegramService
     * @return bool
     */
    public function handle(IGatewayTriggerResultRepository $gatewayTriggerResultRepository, ICacheService $cacheService, ITelegramService $telegramService)
    {
        try {
            $this->processBody($this->gatewayTriggerType, $this->request);

            $body = $this->body;

            if ($this->hasBody()) {
                /** @var Gateway $gateway */
                $gateway = $cacheService->get('gateway');

                $telegramService->make($gateway->token);

                $photo = $telegramService->getFile($body[count($body) - 1]['file_id'])->getFilePath();

                $body = TelegramService::getFilePath($gateway->token, $photo);
            }

            if (!$this->hasBody()) {
                $body = null;
            }

            $payload = [
                'index' => GatewayTriggerType::SET_AUDIO_COVER,
                'body' => $body
            ];

            return $gatewayTriggerResultRepository->saveGatewayTriggerResult($this->gatewayServiceUpdate, $payload);
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}