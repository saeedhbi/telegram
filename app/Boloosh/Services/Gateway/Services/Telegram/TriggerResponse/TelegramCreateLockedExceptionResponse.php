<?php

namespace Boloosh\Services\Gateway\Services\Telegram\TriggerResponse;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Services\Cache\ICacheService;
use Boloosh\Services\Gateway\Services\Telegram\TelegramTriggerCommand;
use Boloosh\Services\SDK\Telegram\ITelegramService;

class TelegramCreateLockedExceptionResponse extends TelegramTriggerCommand
{

    /**
     * @var
     */
    private $conversationId;


    /**
     * TelegramCreateLockedExceptionResponse constructor.
     *
     * @param $conversationId
     */
    public function __construct($conversationId)
    {
        $this->conversationId = $conversationId;
    }


    /**
     * @param ICacheService    $cacheService
     * @param ITelegramService $telegramService
     *
     * @return mixed
     */
    public function handle(ICacheService $cacheService, ITelegramService $telegramService)
    {
        /** @var Gateway $gateway */
        $gateway = $cacheService->get('gateway');

        $telegramService->make($gateway->token);

        $options = [
            'chat_id' => $this->conversationId,
            'text'    => trans('exceptions.not_valid'),
        ];

        $options['reply_markup'] = $telegramService->hideKeyboard();

        return $telegramService->sendMessage($options);
    }

}