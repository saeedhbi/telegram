<?php

namespace Boloosh\Services\Gateway\Services\Telegram\TriggerResponse;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerResultRepository;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTriggerType;
use Boloosh\Services\Gateway\Services\IGatewayServiceUpdateParser;
use Boloosh\Services\Gateway\Services\Telegram\TelegramTriggerCommand;

class TelegramSetAudioPerformPerformerTriggerResponse extends TelegramTriggerCommand
{
    /**
     * @var GatewayServiceUpdate
     */
    private $gatewayServiceUpdate;

    /**
     * @var GatewayTriggerType
     */
    private $gatewayTriggerType;
    /**
     * @var IGatewayServiceUpdateParser
     */
    private $request;

    /**
     * TelegramSetAudioPerformPerformerTriggerResponse constructor.
     * @param GatewayTriggerType $gatewayTriggerType
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     * @param IGatewayServiceUpdateParser $request
     */
    public function __construct(GatewayTriggerType $gatewayTriggerType, GatewayServiceUpdate $gatewayServiceUpdate, IGatewayServiceUpdateParser $request)
    {

        $this->gatewayServiceUpdate = $gatewayServiceUpdate;
        $this->gatewayTriggerType = $gatewayTriggerType;
        $this->request = $request;
    }

    /**
     * @param IGatewayTriggerResultRepository $gatewayTriggerResultRepository
     * @return bool
     */
    public function handle(IGatewayTriggerResultRepository $gatewayTriggerResultRepository)
    {
        try {
            $this->processBody($this->gatewayTriggerType, $this->request);

            $body = $this->body;

            if(!$this->hasBody()) {
                $body = json_decode($this->gatewayServiceUpdate->body)->performer;
            }

            $payload = [
                'index' => GatewayTriggerType::SET_AUDIO_PERFORM_PERFORMER,
                'body' => $body
            ];

            return $gatewayTriggerResultRepository->saveGatewayTriggerResult($this->gatewayServiceUpdate, $payload);
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}