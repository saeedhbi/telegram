<?php

namespace Boloosh\Services\Gateway\Services\Telegram\TriggerResponse;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerDefaultRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerResultRepository;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTriggerDefault;
use Boloosh\Infrastructures\Models\GatewayTriggerType;
use Boloosh\Services\Gateway\Services\IGatewayServiceUpdateParser;
use Boloosh\Services\Gateway\Services\Telegram\TelegramGatewayService;
use Boloosh\Services\Gateway\Services\Telegram\TelegramTriggerCommand;
use Illuminate\Support\Facades\DB;

class TelegramSetSignTriggerResponse extends TelegramTriggerCommand
{
    /**
     * @var GatewayServiceUpdate
     */
    private $gatewayServiceUpdate;

    /**
     * @var IGatewayServiceUpdateParser
     */
    private $request;
    /**
     * @var GatewayTriggerType
     */
    private $gatewayTriggerType;

    /**
     * TelegramSetSignTriggerResponse constructor.
     * @param GatewayTriggerType $gatewayTriggerType
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     * @param IGatewayServiceUpdateParser $request
     * @internal param $body
     */
    public function __construct(GatewayTriggerType $gatewayTriggerType, GatewayServiceUpdate $gatewayServiceUpdate, IGatewayServiceUpdateParser $request)
    {

        $this->gatewayServiceUpdate = $gatewayServiceUpdate;
        $this->request = $request;
        $this->gatewayTriggerType = $gatewayTriggerType;
    }

    /**
     * @param IGatewayTriggerResultRepository $gatewayTriggerResultRepository
     * @param IGatewayTriggerDefaultRepository $gatewayTriggerDefaultRepository
     * @return bool
     */
    public function handle(IGatewayTriggerResultRepository $gatewayTriggerResultRepository, IGatewayTriggerDefaultRepository $gatewayTriggerDefaultRepository)
    {
        DB::beginTransaction();
        try {
            $gatewayTriggerDefault = $gatewayTriggerDefaultRepository->findWhere([['body', '=', urlencode($this->request->getBody())], ['gateway_id', '=', $this->gatewayServiceUpdate->gateway_id], ['gateway_trigger_type_id', '=', GatewayTriggerType::SIGN]])->first();

            $this->processBody($this->gatewayTriggerType, $this->request);

            $body = $this->body;

            if (is_null($gatewayTriggerDefault) && $this->hasBody()) {
                $gatewayTriggerDefault = new GatewayTriggerDefault();
                $gatewayTriggerDefault->gateway_id = $this->gatewayServiceUpdate->gateway_id;
                $gatewayTriggerDefault->gateway_trigger_type_id = GatewayTriggerType::SIGN;
                $gatewayTriggerDefault->body = urlencode($body);

                $gatewayTriggerDefaultRepository->save($gatewayTriggerDefault);
            }

            $payload = [
                'index' => GatewayTriggerType::SET_SIGN,
                'body' => $body
            ];

            $gatewayTriggerResultRepository->saveGatewayTriggerResult($this->gatewayServiceUpdate, $payload);

            DB::commit();
        } catch (AppLogicException $ex) {
            DB::rollback();
            throw $ex;
        } catch (\Exception $ex) {
            DB::rollback();
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}