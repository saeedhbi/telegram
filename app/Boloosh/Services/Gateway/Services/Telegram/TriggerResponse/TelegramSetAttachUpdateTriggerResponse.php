<?php

namespace Boloosh\Services\Gateway\Services\Telegram\TriggerResponse;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerResultRepository;
use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTriggerConfig;
use Boloosh\Infrastructures\Models\GatewayTriggerType;
use Boloosh\Services\Cache\ICacheService;
use Boloosh\Services\Gateway\Services\IGatewayServiceUpdateParser;
use Boloosh\Services\Gateway\Services\Telegram\TelegramTriggerCommand;
use Boloosh\Services\SDK\Telegram\ITelegramService;
use Boloosh\Services\SDK\Telegram\TelegramService;
use Illuminate\Support\Facades\Log;

class TelegramSetAttachUpdateTriggerResponse extends TelegramTriggerCommand
{
    /**
     * @var GatewayServiceUpdate
     */
    private $gatewayServiceUpdate;

    /**
     * @var GatewayTriggerType
     */
    private $gatewayTriggerType;
    /**
     * @var IGatewayServiceUpdateParser
     */
    private $request;

    /**
     * TelegramSetAttachUpdateTriggerResponse constructor.
     * @param GatewayTriggerType $gatewayTriggerType
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     * @param IGatewayServiceUpdateParser $request
     */
    public function __construct(GatewayTriggerType $gatewayTriggerType, GatewayServiceUpdate $gatewayServiceUpdate, IGatewayServiceUpdateParser $request)
    {

        $this->gatewayServiceUpdate = $gatewayServiceUpdate;
        $this->gatewayTriggerType = $gatewayTriggerType;
        $this->request = $request;
    }

    /**
     * @param IGatewayTriggerResultRepository $gatewayTriggerResultRepository
     * @param ICacheService $cacheService
     * @param ITelegramService $telegramService
     * @return bool
     */
    public function handle(IGatewayTriggerResultRepository $gatewayTriggerResultRepository, ICacheService $cacheService, ITelegramService $telegramService)
    {
        try {
            $this->processBody($this->gatewayTriggerType, $this->request);

            $body = $this->body;

            if ($this->hasBody()) {
                $body = $telegramService->getMessageIdByPostLink($body);
            }

            if (!$this->hasBody()) {
                $body = null;
            }

            $payload = [
                'index' => GatewayTriggerType::SET_ATTACH_UPDATE,
                'body' => $body
            ];

            return $gatewayTriggerResultRepository->saveGatewayTriggerResult($this->gatewayServiceUpdate, $payload);
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}