<?php

namespace Boloosh\Services\Gateway\Services\Telegram\TriggerResponse;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Infrastructures\Models\GatewayTrigger;
use Boloosh\Infrastructures\Models\GatewayTriggerConfig;
use Boloosh\Infrastructures\Models\GatewayTriggerType;
use Boloosh\Services\Cache\ICacheService;
use Boloosh\Services\Gateway\Services\Telegram\TelegramTriggerCommand;
use Boloosh\Services\SDK\Telegram\ITelegramService;
use Illuminate\Contracts\Queue\ShouldQueue;

class TelegramGetAudioCutDurationTriggerResponse extends TelegramTriggerCommand
{

    /**
     * @var
     */
    private $conversationId;

    /**
     * @var GatewayTriggerType
     */
    private $gatewayTriggerType;

    /**
     * TelegramGetAudioCutDurationTriggerResponse constructor.
     *
     * @param GatewayTriggerType $gatewayTriggerType
     * @param                    $conversationId
     */
    public function __construct(GatewayTriggerType $gatewayTriggerType, $conversationId)
    {
        $this->gatewayTriggerType = $gatewayTriggerType;
        $this->conversationId = $conversationId;
    }

    /**
     * @param ICacheService $cacheService
     * @param ITelegramService $telegramService
     *
     * @return \Telegram\Bot\Objects\Message
     */
    public function handle(ICacheService $cacheService, ITelegramService $telegramService)
    {
        try {
            /** @var Gateway $gateway */
            $gateway = $cacheService->get('gateway');

            $telegramService->make($gateway->token);

            $config = (new GatewayTriggerConfig(GatewayTriggerType::SET_AUDIO_CUT_DURATION))->data;

            $options = [
                'chat_id' => $this->conversationId,
                'text' => trans('triggers.' . GatewayTriggerType::GET_AUDIO_CUT_DURATION, ['minimum' => $config[0]]),
                'parse_mode' => 'HTML'
            ];

            $options['reply_markup'] = $this->processKeyboard($this->gatewayTriggerType, $telegramService);

            $telegramService->sendMessage($options);
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }
}