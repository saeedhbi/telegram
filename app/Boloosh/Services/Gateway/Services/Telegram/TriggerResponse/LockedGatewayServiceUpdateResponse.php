<?php

namespace Boloosh\Services\Gateway\Services\Telegram\TriggerResponse;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Interfaces\IGatewayServiceUpdateRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerIndexRepository;
use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTrigger;
use Boloosh\Infrastructures\Models\GatewayTriggerIndex;
use Boloosh\Services\Cache\ICacheService;
use Boloosh\Services\Gateway\Services\Telegram\TelegramTriggerCommand;
use Boloosh\Services\SDK\Telegram\ITelegramService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;

class LockedGatewayServiceUpdateResponse extends TelegramTriggerCommand
{

    /**
     * @var
     */
    private $conversationId;

    /**
     * @var
     */
    private $requestId;

    /**
     * @var GatewayTrigger
     */
    private $gatewayTrigger;


    /**
     * LockedGatewayServiceUpdateResponse constructor.
     *
     * @param GatewayTrigger $gatewayTrigger
     * @param                $conversationId
     * @param                $requestId
     */
    public function __construct(GatewayTrigger $gatewayTrigger, $conversationId, $requestId)
    {

        $this->gatewayTrigger = $gatewayTrigger;
        $this->conversationId = $conversationId;
        $this->requestId      = $requestId;
    }


    /**
     * @param IGatewayServiceUpdateRepository $gatewayServiceUpdateRepository
     * @param IGatewayTriggerIndexRepository $gatewayTriggerResultRepository
     * @param ICacheService                   $cacheService
     * @param ITelegramService                $telegramService
     *
     * @return mixed
     */
    public function handle(
        IGatewayServiceUpdateRepository $gatewayServiceUpdateRepository,
        IGatewayTriggerIndexRepository $gatewayTriggerResultRepository,
        ICacheService $cacheService,
        ITelegramService $telegramService
    ) {
        $gatewayServiceUpdate = $gatewayServiceUpdateRepository->findWhere([ 'id' => $this->requestId ], [ 'id', 'status' ])->first();

        try {
            DB::beginTransaction();

            /*
             * Lock current gateway service update
             */
            $gatewayServiceUpdate->status = GatewayServiceUpdate::LOCKED;

            $gatewayServiceUpdateRepository->save($gatewayServiceUpdate);

            /*
             * Create new gateway trigger result with null value
             */
            $gatewayTriggerResult = new GatewayTriggerIndex();

            $gatewayTriggerResult->gateway_id = $this->gatewayTrigger->gateway_id;
            $gatewayTriggerResult->trigger_id = $this->gatewayTrigger->id;
            $gatewayTriggerResult->update_id  = $this->requestId;
            $gatewayTriggerResult->value      = null;

            $gatewayTriggerResultRepository->save($gatewayTriggerResult);

            /** @var Gateway $gateway */
            $gateway = $cacheService->get('gateway');

            $telegramService->make($gateway->token);

            $options = [
                'chat_id' => $this->conversationId,
                'text'    => trans('common.gateway_service_update.' . GatewayServiceUpdate::LOCKED),
            ];

            $options['reply_markup'] = $telegramService->hideKeyboard();

            $telegramService->sendMessage($options);
            DB::commit();
        } catch (AppLogicException $ex) {
            DB::rollback();
            throw $ex;
        } catch (\Exception $ex) {
            DB::rollback();
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}