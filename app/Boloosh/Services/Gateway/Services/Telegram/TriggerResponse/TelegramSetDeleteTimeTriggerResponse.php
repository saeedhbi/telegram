<?php

namespace Boloosh\Services\Gateway\Services\Telegram\TriggerResponse;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerResultRepository;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTriggerType;
use Boloosh\Services\Gateway\Services\IGatewayServiceUpdateParser;
use Boloosh\Services\Gateway\Services\Telegram\TelegramTriggerCommand;
use Carbon\Carbon;

class TelegramSetDeleteTimeTriggerResponse extends TelegramTriggerCommand
{
    /**
     * @var GatewayServiceUpdate
     */
    private $gatewayServiceUpdate;
    /**
     * @var GatewayTriggerType
     */
    private $gatewayTriggerType;
    /**
     * @var IGatewayServiceUpdateParser
     */
    private $request;

    /**
     * TelegramSetDeleteTimeTriggerResponse constructor.
     * @param GatewayTriggerType $gatewayTriggerType
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     * @param IGatewayServiceUpdateParser $request
     * @internal param $body
     */
    public function __construct(GatewayTriggerType $gatewayTriggerType, GatewayServiceUpdate $gatewayServiceUpdate, IGatewayServiceUpdateParser $request)
    {

        $this->gatewayServiceUpdate = $gatewayServiceUpdate;
        $this->gatewayTriggerType = $gatewayTriggerType;
        $this->request = $request;
    }

    /**
     * @param IGatewayTriggerResultRepository $gatewayTriggerResultRepository
     * @return mixed
     */
    public function handle(IGatewayTriggerResultRepository $gatewayTriggerResultRepository)
    {
        try {
            $this->processBody($this->gatewayTriggerType, $this->request);

            $body = $this->body;

            if (!$this->hasBody()) {
                $body = Carbon::now()->addMinutes(5)->tz('Asia/Tehran')->format('Y-m-d H:i:s');
            }

            $payload = [
                'index' => GatewayTriggerType::SET_DELETE_TIME,
                'body' => $body
            ];

            return $gatewayTriggerResultRepository->saveGatewayTriggerResult($this->gatewayServiceUpdate, $payload);
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }
}