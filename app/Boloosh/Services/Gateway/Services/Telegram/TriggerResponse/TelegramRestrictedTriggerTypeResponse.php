<?php

namespace Boloosh\Services\Gateway\Services\Telegram\TriggerResponse;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTrigger;
use Boloosh\Infrastructures\Models\GatewayTriggerType;
use Boloosh\Services\Cache\ICacheService;
use Boloosh\Services\Gateway\Services\Telegram\TelegramTriggerCommand;
use Boloosh\Services\SDK\Telegram\ITelegramService;
use Illuminate\Contracts\Queue\ShouldQueue;

class TelegramRestrictedTriggerTypeResponse extends TelegramTriggerCommand
{

    /**
     * @var
     */
    private $conversationId;

    /**
     * @var GatewayTriggerType
     */
    private $gatewayTriggerType;
    /**
     * @var
     */
    private $updateId;

    /**
     * TelegramGetTimeHourTrigger constructor.
     *
     * @param GatewayTriggerType $gatewayTriggerType
     * @param $conversationId
     * @param $updateId
     */
    public function __construct(GatewayTriggerType $gatewayTriggerType, $conversationId, $updateId)
    {
        $this->gatewayTriggerType = $gatewayTriggerType;
        $this->conversationId = $conversationId;
        $this->updateId = $updateId;
    }

    /**
     * @param ICacheService $cacheService
     * @param ITelegramService $telegramService
     *
     * @return \Telegram\Bot\Objects\Message
     */
    public function handle(ICacheService $cacheService, ITelegramService $telegramService)
    {
        try {
            /** @var Gateway $gateway */
            $gateway = $cacheService->get('gateway');

            $telegramService->make($gateway->token);

            $options = [
                'chat_id' => $this->conversationId,
                'text' => trans('triggers.failed.restricted_type'),
                'reply_markup' => $telegramService->hideKeyboard(),
                'parse_mode'          => 'HTML',
                'reply_to_message_id' => $this->updateId
            ];

            $telegramService->sendMessage($options);
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }
}