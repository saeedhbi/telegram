<?php

namespace Boloosh\Services\Gateway\Services\Telegram\TriggerResponse;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Exceptions\InvalidRequestException;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerRepository;
use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTrigger;
use Boloosh\Infrastructures\Models\GatewayTriggerType;
use Boloosh\Services\Cache\ICacheService;
use Boloosh\Services\Gateway\Services\Telegram\TelegramTriggerCommand;
use Boloosh\Services\SDK\Telegram\ITelegramService;
use Illuminate\Support\Collection;

class TelegramPickGatewayTriggerResponse extends TelegramTriggerCommand
{
    /**
     * @var GatewayServiceUpdate
     */
    private $gatewayServiceUpdate;

    /**
     * TelegramPickGatewayTriggerResponse constructor.
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     */
    public function __construct(GatewayServiceUpdate $gatewayServiceUpdate)
    {

        $this->gatewayServiceUpdate = $gatewayServiceUpdate;
    }

    /**
     * @param ITelegramService $telegramService
     * @param ICacheService $cacheService
     *
     * @param IGatewayTriggerRepository $gatewayTriggerRepository
     * @return mixed
     */
    public function handle(ITelegramService $telegramService, ICacheService $cacheService, IGatewayTriggerRepository $gatewayTriggerRepository)
    {
        try {
            /** @var Gateway $gateway */
            $gateway = $cacheService->get('gateway');

            /** @var ITelegramService $telegram */
            $telegramService->make($gateway->token);

            $layout = [];

            $triggers = $gatewayTriggerRepository->findWhere(['gateway_id' => $gateway->id]);

            /** @var GatewayTrigger $trigger */
            foreach ($triggers as $trigger) {
                array_push($layout, [$trigger->name]);
            }

            $options = [
                'chat_id' => $this->gatewayServiceUpdate->conversation_id,
                'text' => trans('triggers.' . GatewayTriggerType::PICK_GATEWAY_TRIGGER),
                'reply_markup' => $telegramService->keyboard(json_encode($layout), true),
            ];

            return $telegramService->sendMessage($options);
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}