<?php

namespace Boloosh\Services\Gateway\Services\Telegram\TriggerResponse;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerResultRepository;
use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTriggerResult;
use Boloosh\Services\Cache\ICacheService;
use Boloosh\Services\Gateway\Services\Telegram\TelegramTriggerCommand;
use Boloosh\Services\SDK\Telegram\ITelegramService;
use Illuminate\Contracts\Queue\ShouldQueue;

class TelegramCreateEndingRequestResponse extends TelegramTriggerCommand
{

    /**
     * @var GatewayServiceUpdate
     */
    private $gatewayServiceUpdate;

    /**
     * TelegramCreateEndingRequestResponse constructor.
     *
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     */
    public function __construct(GatewayServiceUpdate $gatewayServiceUpdate)
    {
        $this->gatewayServiceUpdate = $gatewayServiceUpdate;
    }

    /**
     * @param ICacheService $cacheService
     * @param ITelegramService $telegramService
     *
     * @param IGatewayTriggerResultRepository $gatewayTriggerResultRepository
     * @return \Telegram\Bot\Objects\Message
     */
    public function handle(ICacheService $cacheService, ITelegramService $telegramService, IGatewayTriggerResultRepository $gatewayTriggerResultRepository)
    {
        try {
            /** @var Gateway $gateway */
            $gateway = $cacheService->get('gateway');

            $telegramService->make($gateway->token);

            $options = [
                'chat_id' => $this->gatewayServiceUpdate->conversation_id,
                'text' => trans('common.ending_request', ['requestId' => $this->gatewayServiceUpdate->id]),
                'parse_mode' => 'HTML',
                'reply_to_message_id' => $this->gatewayServiceUpdate->message_id,
                'reply_markup' => $telegramService->hideKeyboard()
            ];

            return $telegramService->sendMessage($options);
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}