<?php

namespace Boloosh\Services\Gateway\Services\Telegram;

use Boloosh\Exceptions\GatewayServiceUpdateTypeException;
use Boloosh\Exceptions\InvalidRequestException;
use Boloosh\Services\Gateway\Services\IGatewayServiceUpdateParser;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Objects\Message;
use Telegram\Bot\Objects\Update;

class TelegramUpdateParser implements IGatewayServiceUpdateParser
{

    use DispatchesJobs;

    /*
     * Telegram update types
     */
    const MESSAGE = 'message';
    const EDITED_MESSAGE = 'edited_message';
    const CALLBACK_QUERY = 'callback_query';
    const INLINE_QUERY = 'inline_query';
    const CHOSEN_INLINE_RESULT = 'chosen_inline_result';

    /**
     * @var Update
     */
    private $update;

    /**
     * @var Message
     */
    private $message;

    /**
     * @var
     */
    private $commandData = '';


    /**
     * TelegramUpdateHandler constructor.
     *
     * @param $update
     */
    public function __construct($update)
    {
        $this->update = new Update($update);
        $this->_setUpdateContainer();
    }

    /**
     * @return Update
     */
    public function getUpdate()
    {
        return $this->update;
    }

    /**
     * @return int
     */
    public function getUpdateId()
    {
        return $this->update->getUpdateId();
    }

    /**
     * @return Message
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return int
     */
    public function getMessageId()
    {
        return $this->message->getMessageId();
    }


    /**
     * @return int
     */
    public function getConversationId()
    {
        return $this->message->getChat()->getId();
    }


    /**
     * @return int
     */
    public function getSenderId()
    {
        return $this->message->getFrom()->getId();
    }

    /**
     * @return \Telegram\Bot\Objects\User
     */
    public function getSender()
    {
        return $this->message->getFrom();
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        try {
            return $this->dispatch(new TelegramFindUpdateTypeCommand($this->message));
        } catch (\Exception $ex) {
            throw new GatewayServiceUpdateTypeException;
        }
    }


    /**
     * @return mixed
     */
    public function getBody()
    {
        if ($this->update->isType(TelegramUpdateParser::MESSAGE) || $this->update->isType(TelegramUpdateParser::EDITED_MESSAGE)) {
            $body = $this->message[$this->getType()];
        }

        if ($this->update->isType(TelegramUpdateParser::CALLBACK_QUERY)) {
            $body = $this->update->getCallbackQuery()->getData();
        }

        if (is_array($body)) {
            return json_encode($body);
        }

        return $body;
    }


    /**
     * @return mixed
     */
    public function getPayloads()
    {
        return json_encode($this->update);
    }


    /**
     * Retrieve request id for gatewayServiceUpdate
     *
     * @return null|int
     */
    public function getRequestId()
    {
        if (str_contains($this->getBody(), '_')) {
            $commandWithRequestId = explode(' ', $this->getBody())[0];

            return explode('_', $commandWithRequestId)[1];
        }

        return null;
    }


    /**
     * @return mixed
     */
    public function getCommandData()
    {
        if ( ! is_null($this->commandData)) {
            if (str_contains($this->getBody(), ' ')) {
                $this->commandData = explode(' ', $this->getBody())[1];
            } else {
                $this->commandData = null;
            }
        }

        return $this->commandData;
    }


    /**
     * @param $value
     *
     * @return mixed
     */
    public function setCommandData($value)
    {
        $this->commandData = $value;
    }

    /**
     * Fetch current message object
     */
    private function _setUpdateContainer()
    {
        if ($this->update->isType(TelegramUpdateParser::MESSAGE)) {
            $this->message = $this->update->getMessage();
        }

        if ($this->update->isType(TelegramUpdateParser::EDITED_MESSAGE)) {
            $this->message = $this->update->getMessage();
        }

        if ($this->update->isType(TelegramUpdateParser::CALLBACK_QUERY)) {
            $this->message = $this->update->getCallbackQuery()->getMessage();
        }

        if ($this->update->isType(TelegramUpdateParser::INLINE_QUERY)) {
            $this->message = $this->update->getInlineQuery();
        }

        if ($this->update->isType(TelegramUpdateParser::CHOSEN_INLINE_RESULT)) {
            $this->message = $this->update->getChosenInlineResult();
        }

        if(is_null($this->message)) {
            throw new InvalidRequestException;
        }
    }
}