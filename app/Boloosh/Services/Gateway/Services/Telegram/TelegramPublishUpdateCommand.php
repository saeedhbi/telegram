<?php

namespace Boloosh\Services\Gateway\Services\Telegram;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Interfaces\IGatewayRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayServiceUpdateRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerResultRepository;
use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTriggerResult;
use Boloosh\Infrastructures\Models\GatewayTriggerType;
use Boloosh\Services\Cache\ICacheService;
use Boloosh\Services\File\IFileService;
use Boloosh\Services\SDK\Telegram\ITelegramService;
use Carbon\Carbon;
use Illuminate\Auth\Access\Gate;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Objects\Message;

class TelegramPublishUpdateCommand extends BaseCommand implements ShouldQueue
{
    /**
     * @var GatewayServiceUpdate
     */
    private $gatewayServiceUpdate;
    /**
     * @var Collection
     */
    private $data;

    /**
     * TelegramPublishUpdateCommand constructor.
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     * @param $data
     */
    public function __construct(GatewayServiceUpdate $gatewayServiceUpdate, $data)
    {

        $this->gatewayServiceUpdate = $gatewayServiceUpdate;
        $this->data = $data;
    }

    /**
     * @param IGatewayRepository $gatewayRepository
     * @param ITelegramService $telegramService
     * @param IGatewayTriggerResultRepository $gatewayTriggerResultRepository
     * @param IFileService $fileService
     * @return mixed
     * @internal param IGatewayServiceUpdateRepository $gatewayServiceUpdateRepository
     */
    public function handle(IGatewayRepository $gatewayRepository, ITelegramService $telegramService, IGatewayTriggerResultRepository $gatewayTriggerResultRepository, IFileService $fileService)
    {
        try {
            /** @var Gateway $gateway */
            $gateway = $gatewayRepository->findWhere(['id' => $this->gatewayServiceUpdate->gateway_id])->first();

            $telegramService->make($gateway->token);

            $options = [
                'chat_id' => $this->data->get(GatewayTriggerType::SET_DESTINATION)
            ];

            if($this->data->get(GatewayTriggerType::SET_ATTACH_UPDATE)) {
                $options['reply_to_message_id'] = $this->data->get(GatewayTriggerType::SET_ATTACH_UPDATE);
            }

            $response = null;

            if ($this->gatewayServiceUpdate->type == GatewayServiceUpdate::STICKER) {
                $options['sticker'] = $this->data->get('sticker');

                /** @var Message $response */
                $response = $telegramService->sendSticker($options);
            }

            if ($this->gatewayServiceUpdate->type == GatewayServiceUpdate::TEXT) {
                $options['text'] = $this->data->get('text');
                $options['parse_mode'] = 'HTML';
                $options['disable_web_page_preview'] = true;

                if ($this->data->has(GatewayTriggerType::SET_SIGN)) {
                    $options['text'] = $options['text'] . PHP_EOL . $this->data->get(GatewayTriggerType::SET_SIGN);
                }

                if ($this->data->has(GatewayTriggerType::SET_BUTTON_TITLE)) {
                    $options['reply_markup'] = Keyboard::make()->inline()->row(Keyboard::inlineButton(['text' => $this->data->get(GatewayTriggerType::SET_BUTTON_TITLE), 'url' => $this->data->get(GatewayTriggerType::SET_BUTTON_LINK)]));
                }

                /** @var Message $response */
                $response = $telegramService->sendMessage($options);
            }

            if ($this->gatewayServiceUpdate->type == GatewayServiceUpdate::PHOTO) {
                $options['photo'] = $this->data->get('fileId');
                $options['caption'] = $this->data->get(GatewayTriggerType::SET_CAPTION);

                if ($this->data->has(GatewayTriggerType::SET_SIGN)) {
                    $options['caption'] = $options['caption'] . PHP_EOL . $this->data->get(GatewayTriggerType::SET_SIGN);
                }

                if ($this->data->has(GatewayTriggerType::SET_BUTTON_TITLE)) {
                    $options['reply_markup'] = Keyboard::make()->inline()->row(Keyboard::inlineButton(['text' => $this->data->get(GatewayTriggerType::SET_BUTTON_TITLE), 'url' => $this->data->get(GatewayTriggerType::SET_BUTTON_LINK)]));
                }

                /** @var Message $response */
                $response = $telegramService->sendPhoto($options);
            }

            if ($this->gatewayServiceUpdate->type == GatewayServiceUpdate::AUDIO) {
                if ($this->data->has(GatewayTriggerType::SET_AUDIO_CUT_DURATION)) {
                    $options2 = $options;
                    unset($options2['reply_to_message_id']);

                    $options2['duration'] = (int)$this->data->get(GatewayTriggerType::SET_AUDIO_CUT_DURATION);

                    $file = $fileService->audio()->voice($this->data->get('path'), (int)$this->data->get(GatewayTriggerType::SET_AUDIO_CUT_START), $options2['duration']);
                    $options2['voice'] = asset($file);

                    $options2['caption'] = '#Demo';

                    $response = $telegramService->sendVoice($options2);

                    $gatewayTriggerResultRepository->saveGatewayTriggerResult($this->gatewayServiceUpdate, [
                        'index' => GatewayTriggerResult::PUBLISH_MESSAGE_ATTACH_ID,
                        'body' => $response->getMessageId()
                    ]);

                    unlink(public_path() . '/' . $file);
                }

                if ($this->data->has(GatewayTriggerType::SET_AUDIO_COVER)) {
                    $mp3Options = [
                        'artwork' => $this->data->get(GatewayTriggerType::SET_AUDIO_COVER),
                        'title' => $this->data->get(GatewayTriggerType::SET_AUDIO_PERFORM_TITLE),
                        'artist' => $this->data->get(GatewayTriggerType::SET_AUDIO_PERFORM_PERFORMER),
                    ];

                    $options['audio'] = asset($fileService->audio()->mp3($this->data->get('path'), $mp3Options));
                } else {
                    $mp3Options = [
                        'title' => $this->data->get(GatewayTriggerType::SET_AUDIO_PERFORM_TITLE),
                        'artist' => $this->data->get(GatewayTriggerType::SET_AUDIO_PERFORM_PERFORMER),
                    ];

                    $options['audio'] = asset($fileService->audio()->mp3($this->data->get('path'), $mp3Options));
                    $options['title'] = $this->data->get(GatewayTriggerType::SET_AUDIO_PERFORM_TITLE);
                    $options['performer'] = $this->data->get(GatewayTriggerType::SET_AUDIO_PERFORM_PERFORMER);
                }

                $options['caption'] = $this->data->get(GatewayTriggerType::SET_CAPTION);

                if ($this->data->has(GatewayTriggerType::SET_SIGN)) {
                    $options['caption'] = $options['caption'] . PHP_EOL . $this->data->get(GatewayTriggerType::SET_SIGN);
                }


                if ($this->data->has(GatewayTriggerType::SET_BUTTON_TITLE)) {
                    $options['reply_markup'] = Keyboard::make()->inline()->row(Keyboard::inlineButton(['text' => $this->data->get(GatewayTriggerType::SET_BUTTON_TITLE), 'url' => $this->data->get(GatewayTriggerType::SET_BUTTON_LINK)]));
                }

                /** @var Message $response */
                $response = $telegramService->sendAudio($options);
            }

            if (!is_null($response)) {
                $gatewayTriggerResultRepository->saveGatewayTriggerResult($this->gatewayServiceUpdate, [
                    'index' => GatewayTriggerResult::PUBLISH_MESSAGE_ID,
                    'body' => $response->getMessageId()
                ]);

                $gatewayTriggerResultRepository->saveGatewayTriggerResult($this->gatewayServiceUpdate, [
                    'index' => GatewayTriggerResult::PUBLISH_CONVERSATION_ID,
                    'body' => $response->getChat()->getId()
                ]);
            }

            /** @var GatewayTriggerResult $gatewayTriggerResult */
            $gatewayTriggerResult = $gatewayTriggerResultRepository->findByUpdateId($this->gatewayServiceUpdate->id);
            $gatewayTriggerResult->status = GatewayTriggerResult::PUBLISH;
            $gatewayTriggerResult->save();

            return $response;
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}