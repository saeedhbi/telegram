<?php

namespace Boloosh\Services\Gateway\Services\Telegram;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Interfaces\IGatewayRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayServiceUpdateRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerResultRepository;
use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTriggerResult;
use Boloosh\Infrastructures\Models\GatewayTriggerType;
use Boloosh\Services\Cache\ICacheService;
use Boloosh\Services\SDK\Telegram\ITelegramService;
use Carbon\Carbon;
use Illuminate\Auth\Access\Gate;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Objects\Message;

class TelegramDeleteUpdateCommand extends BaseCommand implements ShouldQueue
{
    /**
     * @var GatewayServiceUpdate
     */
    private $gatewayServiceUpdate;
    /**
     * @var
     */
    private $conversationId;
    /**
     * @var
     */
    private $messageId;
    /**
     * @var null
     */
    private $messageAttachId;

    /**
     * TelegramDeleteUpdateCommand constructor.
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     * @param $conversationId
     * @param $messageId
     * @param null $messageAttachId
     */
    public function __construct(GatewayServiceUpdate $gatewayServiceUpdate, $conversationId, $messageId, $messageAttachId = null)
    {

        $this->gatewayServiceUpdate = $gatewayServiceUpdate;
        $this->conversationId = $conversationId;
        $this->messageId = $messageId;
        $this->messageAttachId = $messageAttachId;
    }

    /**
     * @param IGatewayRepository $gatewayRepository
     * @param ITelegramService $telegramService
     * @return mixed
     * @internal param IGatewayServiceUpdateRepository $gatewayServiceUpdateRepository
     */
    public function handle(IGatewayRepository $gatewayRepository, ITelegramService $telegramService)
    {
        try {
            /** @var Gateway $gateway */
            $gateway = $gatewayRepository->findWhere(['id' => $this->gatewayServiceUpdate->gateway_id])->first();

            $telegramService->make($gateway->token);

            $options = [
                'chat_id' => $this->conversationId,
                'message_id' => $this->messageId
            ];

            $telegramService->deleteMessage($options);

            if (!is_null($this->messageAttachId)) {
                $options = [
                    'chat_id' => $this->conversationId,
                    'message_id' => $this->messageAttachId
                ];

                $telegramService->deleteMessage($options);
            }
        } catch (\Exception $ex) {
            throw new InvalidLogicException;
        }
    }

}