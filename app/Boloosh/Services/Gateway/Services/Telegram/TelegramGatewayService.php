<?php

namespace Boloosh\Services\Gateway\Services\Telegram;

use Boloosh\Core\Commands\GatewayServiceUpdate\DeleteGatewayServiceUpdateJobsCommand;
use Boloosh\Core\Commands\GatewayServiceUpdate\DetectConversationHasLockedUpdateCommand;
use Boloosh\Core\Commands\GatewayServiceUpdate\GetGatewayServiceUpdateByIdCommand;
use Boloosh\Core\Commands\GatewayServiceUpdate\GetLockedGatewayServiceUpdateByConversationIdCommand;
use Boloosh\Core\Commands\GatewayServiceUpdate\GetPendingGatewayServiceUpdateByConversationIdCommand;
use Boloosh\Core\Commands\GatewayServiceUpdate\SaveGatewayServiceUpdateJobCommand;
use Boloosh\Core\Commands\GatewayTriggerResult\GetCurrentGatewayTriggerIndexCommand;
use Boloosh\Core\Commands\GatewayTriggerResult\GetNextGatewayTriggerIndexCommand;
use Boloosh\Exceptions\AccessDeniedException;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Infrastructures\Interfaces\IGatewayServiceUpdateRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerResultRepository;
use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTriggerIndex;
use Boloosh\Infrastructures\Models\GatewayTriggerType;
use Boloosh\Services\Gateway\Services\Exceptions\GatewayServiceTriggerCommandException;
use Boloosh\Services\Gateway\Services\IGatewayService;
use Boloosh\Services\Gateway\Services\IGatewayServiceUpdateParser;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramCreateEndingRequestResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramCreateLockedExceptionResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramNewRequestTriggerResponse;
use Carbon\Carbon;
use DateTime;
use DateTimeZone;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\DB;

class TelegramGatewayService implements IGatewayService
{

    use DispatchesJobs;

    /**
     * @var IGatewayServiceUpdateParser $request
     */
    private $request;

    /**
     * @var int $triggerIndex
     */
    private $triggerIndex = null;

    /**
     * @var bool
     */
    private $shouldCheckAccess = true;

    /**
     * @var null
     */
    private $serviceTriggerCommand = null;

    /**
     * @var GatewayServiceUpdate
     */
    private $serviceUpdate;

    /**
     * @param $update
     *
     * @return TelegramUpdateParser
     */
    public function parseUpdate($update)
    {
        return new TelegramUpdateParser($update);
    }

    /**
     * Check gateway service update type on this service
     */
    public function checkGatewayServiceUpdateType()
    {
        if (!in_array($this->request->getType(), [GatewayServiceUpdate::TEXT, GatewayServiceUpdate::AUDIO, GatewayServiceUpdate::VIDEO, GatewayServiceUpdate::PHOTO, GatewayServiceUpdate::DOCUMENT, GatewayServiceUpdate::STICKER, GatewayServiceUpdate::VOICE,])) {
            throw new AccessDeniedException();
        }
    }

    /**
     * @param $request
     *
     * @return mixed
     */
    public function setRequest($request)
    {
        $this->request = $this->parseUpdate($request);
    }

    /**
     * @return IGatewayServiceUpdateParser
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return bool
     */
    public function shouldCheckAccess()
    {
        return $this->shouldCheckAccess;
    }

    /**
     * Create new request response
     *
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     *
     * @return mixed
     */
    public function createNewRequestResponse(GatewayServiceUpdate $gatewayServiceUpdate)
    {
        return new TelegramNewRequestTriggerResponse($gatewayServiceUpdate);
    }

    /**
     * @return mixed
     */
    public function setGatewayTriggerIndex()
    {
        $this->triggerIndex = $this->dispatch(new GetCurrentGatewayTriggerIndexCommand($this->request->getConversationId()));

    }

    /**
     * @return Boolean
     */
    public function isCommand()
    {
        if ($this->request->getType() == GatewayServiceUpdate::TEXT) {
            $entities = collect($this->request->getMessage()->getEntities());

            $entitiesTypes = $entities->pluck('type')->toArray();

            if (in_array('bot_command', $entitiesTypes)) {
                return true;
            }

            if (starts_with($this->request->getBody(), '/')) {
                return true;
            }
        }

        return false;
    }

    /**
     *
     * @param null $triggerIndex
     * @return TelegramTriggerResponse
     */
    public function trigger($triggerIndex = null)
    {
        $index = is_null($triggerIndex) ? $this->triggerIndex : $triggerIndex;

        if (is_null($this->serviceUpdate)) {
            try {
                $gatewayServiceUpdate = $this->dispatch(new GetPendingGatewayServiceUpdateByConversationIdCommand($this->request->getConversationId()));

                $this->setGatewayServiceUpdate($gatewayServiceUpdate);
            } catch (AppLogicException $ex) {
                //
            }
        }

        return new TelegramTriggerResponse($index, $this->request, $this->serviceUpdate);
    }

    /**
     * @return int
     */
    public function getGatewayTriggerIndex()
    {
        return $this->triggerIndex;
    }

    /**
     * Detect next request response on workflow
     *
     * @param GatewayTriggerIndex $gatewayTriggerIndex
     *
     * @return mixed
     */
    public function handleNextServiceResponse(GatewayTriggerIndex $gatewayTriggerIndex)
    {
        // We want data for current conversation to pass to trigger
        /** @var GatewayServiceUpdate $gatewayServiceUpdate */
        $gatewayServiceUpdate = $this->dispatch(new GetGatewayServiceUpdateByIdCommand($gatewayTriggerIndex->update_id));

        $this->setGatewayServiceUpdate($gatewayServiceUpdate);

        // We should set it as parsed request to access data via magic methods
        $this->setRequest(json_decode($gatewayServiceUpdate->payloads));

        // We knew current conversation started with body, So it should have conflict with commandData
        // We set commandData to null to access getter trigger instead of setter
        $this->request->setCommandData(null);

        $nextGatewayTrigger = $this->dispatch(new GetNextGatewayTriggerIndexCommand($gatewayServiceUpdate));

        // If there is no next trigger, we should start workflow and finalize current update
        if (is_null($nextGatewayTrigger)) {
            /** @var IGatewayServiceUpdateRepository $gatewayServiceUpdateRepository */
            $gatewayServiceUpdateRepository = app(IGatewayServiceUpdateRepository::class);

            $gatewayServiceUpdate->status = GatewayServiceUpdate::SENT;

            return $gatewayServiceUpdateRepository->save($gatewayServiceUpdate);
        }

        return $this->trigger($nextGatewayTrigger)->callTrigger();
    }

    /**
     * Detect there is locked request to update body
     *
     * @return boolean
     */
    public function hasAnyLockedRequest()
    {
        return $this->dispatch(new DetectConversationHasLockedUpdateCommand($this->request->getConversationId()));
    }

    /**
     * Create gateway service authentication response
     *
     * @param Gateway $gateway
     * @return mixed
     */
    public function processGatewayServiceUserAuthentication(Gateway $gateway)
    {
        $this->dispatch(new TelegramUserAuthenticationCommand($gateway, $this->request));
    }

    /**
     * Retrieve locked request for conversation
     *
     * @return mixed
     */
    public function getLockedGatewayServiceUpdate()
    {
        return $this->dispatch(new GetLockedGatewayServiceUpdateByConversationIdCommand($this->request->getConversationId()));
    }

    /**
     * @return mixed
     */
    public function createLockedExceptionResponse()
    {
        $this->dispatch(new TelegramCreateLockedExceptionResponse($this->request->getConversationId()));
    }

    /**
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     *
     * @return mixed
     */
    public function handleEndingRequestResponse(GatewayServiceUpdate $gatewayServiceUpdate)
    {
        $this->dispatch(new TelegramCreateEndingRequestResponse($gatewayServiceUpdate));
        $this->dispatch(new TelegramCreateDestinationUpdateCommand($gatewayServiceUpdate));
        $this->dispatch(new DeleteGatewayServiceUpdateJobsCommand($gatewayServiceUpdate));
    }

    /**
     * @return int
     */
    public function getTriggerIndex()
    {
        return $this->triggerIndex;
    }

    public function checkServiceTriggerCommands()
    {
        if ($this->isCommand()) {
            $command = $this->request->getBody();

            if (starts_with($this->request->getBody(), '/')) {
                $command = $this->getCommand();
            }

            if ($command == GatewayTriggerType::COMMAND_CANCEL) {
                $this->triggerIndex = GatewayTriggerType::SET_CANCEL;
                throw new GatewayServiceTriggerCommandException;
            }
        }
    }

    /**
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     *
     * @return mixed|void
     */
    public function setGatewayServiceUpdate(GatewayServiceUpdate $gatewayServiceUpdate)
    {
        $this->serviceUpdate = $gatewayServiceUpdate;
    }

    /**
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     * @param \Illuminate\Support\Collection $destinationData
     *
     * @return mixed|void
     */
    public function publishUpdate(GatewayServiceUpdate $gatewayServiceUpdate, $destinationData)
    {
        $delayTime = Carbon::now();

        if ($destinationData->has(GatewayTriggerType::SET_TIME)) {
            $delayTime = DateTime::createFromFormat(
                'Y-m-d H:i:s',
                $destinationData->get(GatewayTriggerType::SET_TIME),
                new DateTimeZone('Asia/Tehran')
            )->setTimezone(new DateTimeZone('UTC'));
        }

        $job = (new TelegramPublishUpdateCommand($gatewayServiceUpdate, $destinationData))->delay($delayTime);

        $id = $this->dispatch($job);

        $this->dispatch(new SaveGatewayServiceUpdateJobCommand($gatewayServiceUpdate, $id));
    }

    /**
     * @return mixed
     */
    public function getCommand()
    {
        $command = ltrim($this->request->getBody(), '/');

        if (str_contains($command, ' ')) {
            $command = explode(' ', $command)[0];
        }

        if (str_contains($command, '_')) {
            $command = explode('_', $command)[0];
        }

        return $command;
    }
}