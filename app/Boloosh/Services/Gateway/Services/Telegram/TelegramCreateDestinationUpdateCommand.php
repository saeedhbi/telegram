<?php

namespace Boloosh\Services\Gateway\Services\Telegram;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Infrastructures\Interfaces\IGatewayServiceRepository;
use Boloosh\Infrastructures\Interfaces\IGatewayTriggerResultRepository;
use Boloosh\Infrastructures\Models\Gateway;
use Boloosh\Infrastructures\Models\GatewayService;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTriggerResult;
use Boloosh\Infrastructures\Models\GatewayTriggerType;
use Boloosh\Services\Cache\ICacheService;
use Boloosh\Services\Gateway\GatewayServiceManager;
use Boloosh\Services\Gateway\Services\IGatewayService;
use Boloosh\Services\SDK\Telegram\ITelegramService;
use Boloosh\Services\SDK\Telegram\TelegramService;

class TelegramCreateDestinationUpdateCommand extends BaseCommand
{
    /**
     * @var GatewayServiceUpdate
     */
    private $gatewayServiceUpdate;

    /**
     * TelegramPublishUpdateCommand constructor.
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     */
    public function __construct(GatewayServiceUpdate $gatewayServiceUpdate)
    {

        $this->gatewayServiceUpdate = $gatewayServiceUpdate;
    }

    /**
     * @param ICacheService $cacheService
     * @param ITelegramService $telegramService
     * @param IGatewayTriggerResultRepository $gatewayTriggerResultRepository
     * @param IGatewayServiceRepository $gatewayServiceRepository
     * @return mixed
     */
    public function handle(ICacheService $cacheService, ITelegramService $telegramService, IGatewayTriggerResultRepository $gatewayTriggerResultRepository, IGatewayServiceRepository $gatewayServiceRepository)
    {
        try {
            /** @var Gateway $gateway */
            $gateway = $cacheService->get('gateway');

            $telegramService->make($gateway->token);

            $destinationData = collect();

            /** @var GatewayTriggerResult $gatewayTriggerResult */
            $gatewayTriggerResult = $gatewayTriggerResultRepository->findByUpdateId($this->gatewayServiceUpdate->id);

            $gatewayTriggerResultPayload = collect($gatewayTriggerResult->payload);

            if ($this->gatewayServiceUpdate->type == GatewayServiceUpdate::TEXT) {
                $destinationData->put('text', $telegramService->parseHtml($this->gatewayServiceUpdate->body));
            }

            if ($this->gatewayServiceUpdate->type == GatewayServiceUpdate::STICKER) {
                $body = json_decode($this->gatewayServiceUpdate->body, true);
                $destinationData->put('sticker', $body['file_id']);
            }

            if ($this->gatewayServiceUpdate->type == GatewayServiceUpdate::PHOTO) {
                $body = json_decode($this->gatewayServiceUpdate->body, true);
                $file = $telegramService->getFile($body[count($body) - 1]['file_id'])->getFilePath();
                $destinationData->put('path', TelegramService::getFilePath($gateway->token, $file));
                $destinationData->put('fileId', $body[count($body) - 1]['file_id']);
            }

            if ($this->gatewayServiceUpdate->type == GatewayServiceUpdate::AUDIO) {
                $body = json_decode($this->gatewayServiceUpdate->body, true);
                $file = $telegramService->getFile($body['file_id'])->getFilePath();
                $destinationData->put('path', TelegramService::getFilePath($gateway->token, $file));
                $destinationData->put('fileId', $body['file_id']);
            }

            foreach ($gatewayTriggerResultPayload as $item) {
                $destinationData->put($item['index'], $item['body']);
            }

            /** @var GatewayServiceManager $gatewayServiceManager */
            $gatewayServiceManager = app(GatewayServiceManager::class);

            /** @var GatewayService $destinationService */
            $destinationService = $gatewayServiceRepository->find($gateway->destination_id);

            /** @var IGatewayService $service */
            $service = $gatewayServiceManager->getGatewayService($destinationService);

            return $service->publishUpdate($this->gatewayServiceUpdate, $destinationData);
        } catch (AppLogicException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }
    }

}