<?php

namespace Boloosh\Services\Gateway\Services\Telegram;

use Boloosh\Core\Commands\GatewayTrigger\GetGatewayTriggerByGatewayServiceUpdateCommand;
use Boloosh\Core\Commands\GatewayTrigger\SetGatewayTriggerCommand;
use Boloosh\Core\Commands\GatewayTriggerResult\GetNextGatewayTriggerIndexCommand;
use Boloosh\Core\Commands\GatewayTriggerResult\SaveGatewayTriggerResultIndexCommand;
use Boloosh\Core\Commands\GatewayTriggerType\CheckCanTriggerGatewayServiceUpdateCommand;
use Boloosh\Core\Commands\GatewayTriggerType\GetGatewayTriggerByTriggerTypeIndexCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Exceptions\InvalidGatewayServiceTriggerException;
use Boloosh\Exceptions\InvalidRequestException;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTrigger;
use Boloosh\Infrastructures\Models\GatewayTriggerConfig;
use Boloosh\Infrastructures\Models\GatewayTriggerType;
use Boloosh\Services\Gateway\IGatewayServiceCaller;
use Boloosh\Services\Gateway\Services\GatewayTriggerResponse;
use Boloosh\Services\Gateway\Services\IGatewayServiceUpdateParser;
use Boloosh\Services\Gateway\Services\IGatewayTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\LockedGatewayServiceUpdateResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramCancelTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramGetAttachUpdateTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramGetAudioCoverTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramGetAudioCutDurationTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramGetAudioCutStartTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramGetAudioPerformPerformerTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramGetAudioPerformTitleTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramGetButtonLinkTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramGetButtonTitleTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramGetCaptionTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramGetDeleteTimeTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramGetDestinationTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramGetSignTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramGetTimeTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramGetTimeMinuteTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramInputErrorResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramRestrictedTriggerTypeResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramSetAttachUpdateTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramSetAudioCoverTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramSetAudioCutDurationTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramSetAudioCutStartTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramSetAudioPerformPerformerTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramSetAudioPerformTitleTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramSetButtonLinkTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramSetButtonTitleTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramSetCaptionTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramSetDeleteTimeTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramSetDestinationTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramSetSignTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramSetTimeTriggerResponse;
use Boloosh\Services\Gateway\Services\Telegram\TriggerResponse\TelegramSetTimeMinuteTriggerResponse;
use Carbon\Carbon;
use DateTime;
use Illuminate\Foundation\Bus\DispatchesJobs;

class TelegramTriggerResponse extends GatewayTriggerResponse implements IGatewayTriggerResponse
{

    use DispatchesJobs;

    /**
     * @var $triggerTypeIndex
     */
    private $triggerTypeIndex;

    /**
     * @var IGatewayServiceUpdateParser $request
     */
    private $request;
    /**
     * @var GatewayServiceUpdate|null
     */
    private $gatewayServiceUpdate;

    /**
     * @var GatewayTriggerType
     */
    private $gatewayTriggerType;

    /**
     * TelegramTriggerResponse constructor.
     *
     * @param $triggerTypeIndex
     * @param IGatewayServiceUpdateParser $request
     * @param GatewayServiceUpdate|null $gatewayServiceUpdate
     */
    public function __construct($triggerTypeIndex, IGatewayServiceUpdateParser $request, GatewayServiceUpdate $gatewayServiceUpdate = null)
    {
        $this->triggerTypeIndex = $triggerTypeIndex;
        $this->request = $request;
        $this->gatewayServiceUpdate = $gatewayServiceUpdate;
    }

    /**
     * @return mixed
     */
    public function callTrigger()
    {
        try {

            if ($this->triggerTypeIndex == GatewayTriggerType::PICK_GATEWAY_TRIGGER) {
                try {
                    $gatewayTriggerIndex = $this->dispatch(new SetGatewayTriggerCommand($this->gatewayServiceUpdate, $this->request->getBody()));

                    /** @var IGatewayServiceCaller $gatewayServiceCaller */
                    $gatewayServiceCaller = app(IGatewayServiceCaller::class);

                    return $gatewayServiceCaller->telegram()->handleNextServiceResponse($gatewayTriggerIndex);
                } catch (InvalidGatewayServiceTriggerException $ex) {
                    return $this->dispatch(new TelegramInputErrorResponse($this->gatewayServiceUpdate, $this->request->getConversationId()));
                }
            }

            $this->gatewayTriggerType = $this->dispatch(new GetGatewayTriggerByTriggerTypeIndexCommand($this->triggerTypeIndex));

            if (!is_null($this->gatewayServiceUpdate)) {
                try {
                    $this->dispatch(new CheckCanTriggerGatewayServiceUpdateCommand($this->gatewayServiceUpdate, $this->gatewayTriggerType));
                } catch (\Exception $ex) {
                    $this->dispatch(new TelegramRestrictedTriggerTypeResponse($this->gatewayTriggerType, $this->request->getConversationId(), $this->gatewayServiceUpdate->message_id));
                    $this->dispatch(new TelegramCancelTriggerResponse($this->gatewayTriggerType, $this->request->getConversationId(), $this->gatewayServiceUpdate->id));
                    throw new InvalidRequestException;
                }
            }

            if ($this->triggerTypeIndex == GatewayTriggerType::SET_CANCEL) {
                return $this->dispatch(new TelegramCancelTriggerResponse($this->gatewayTriggerType, $this->request->getConversationId(), $this->request->getRequestId()));
            }

            if ($this->triggerTypeIndex == GatewayTriggerType::GET_DESTINATION) {
                $this->dispatch(new TelegramGetDestinationTriggerResponse($this->gatewayTriggerType, $this->request->getConversationId()));

                return $this->saveGatewayTriggerResult($this->gatewayServiceUpdate, GatewayTriggerType::SET_DESTINATION, false);
            }

            if ($this->triggerTypeIndex == GatewayTriggerType::SET_DESTINATION) {
                $this->_validateInput();
                $this->dispatch(new TelegramSetDestinationTriggerResponse($this->gatewayTriggerType, $this->gatewayServiceUpdate, $this->request));

                return $this->saveGatewayTriggerResult($this->gatewayServiceUpdate, $this->triggerTypeIndex, true);
            }

            if ($this->triggerTypeIndex == GatewayTriggerType::GET_CAPTION) {
                $this->dispatch(new TelegramGetCaptionTriggerResponse($this->gatewayTriggerType, $this->request->getConversationId()));

                return $this->saveGatewayTriggerResult($this->gatewayServiceUpdate, GatewayTriggerType::SET_CAPTION, false);
            }

            if ($this->triggerTypeIndex == GatewayTriggerType::SET_CAPTION) {
                $this->_validateInput();
                $this->dispatch(new TelegramSetCaptionTriggerResponse($this->gatewayTriggerType, $this->gatewayServiceUpdate, $this->request));

                return $this->saveGatewayTriggerResult($this->gatewayServiceUpdate, $this->triggerTypeIndex, true);
            }

            if ($this->triggerTypeIndex == GatewayTriggerType::GET_SIGN) {
                $this->dispatch(new TelegramGetSignTriggerResponse($this->gatewayTriggerType, $this->request->getConversationId()));

                return $this->saveGatewayTriggerResult($this->gatewayServiceUpdate, GatewayTriggerType::SET_SIGN, false);
            }

            if ($this->triggerTypeIndex == GatewayTriggerType::SET_SIGN) {
                $this->_validateInput();
                $this->dispatch(new TelegramSetSignTriggerResponse($this->gatewayTriggerType, $this->gatewayServiceUpdate, $this->request));

                return $this->saveGatewayTriggerResult($this->gatewayServiceUpdate, $this->triggerTypeIndex, true);
            }

            if ($this->triggerTypeIndex == GatewayTriggerType::GET_TIME) {
                $this->dispatch(new TelegramGetTimeTriggerResponse($this->gatewayTriggerType, $this->request->getConversationId()));

                return $this->saveGatewayTriggerResult($this->gatewayServiceUpdate, GatewayTriggerType::SET_TIME, false);
            }

            if ($this->triggerTypeIndex == GatewayTriggerType::SET_TIME) {
                $this->_validateInput();
                $this->dispatch(new TelegramSetTimeTriggerResponse($this->gatewayTriggerType, $this->gatewayServiceUpdate, $this->request));

                return $this->saveGatewayTriggerResult($this->gatewayServiceUpdate, $this->triggerTypeIndex, true);
            }

            if ($this->triggerTypeIndex == GatewayTriggerType::GET_DELETE_TIME) {
                $this->dispatch(new TelegramGetDeleteTimeTriggerResponse($this->gatewayTriggerType, $this->request->getConversationId()));

                return $this->saveGatewayTriggerResult($this->gatewayServiceUpdate, GatewayTriggerType::SET_DELETE_TIME, false);
            }

            if ($this->triggerTypeIndex == GatewayTriggerType::SET_DELETE_TIME) {
                $this->_validateInput();
                $this->dispatch(new TelegramSetDeleteTimeTriggerResponse($this->gatewayTriggerType, $this->gatewayServiceUpdate, $this->request));

                return $this->saveGatewayTriggerResult($this->gatewayServiceUpdate, $this->triggerTypeIndex, true);
            }

            if ($this->triggerTypeIndex == GatewayTriggerType::GET_BUTTON_TITLE) {
                $this->dispatch(new TelegramGetButtonTitleTriggerResponse($this->gatewayTriggerType, $this->request->getConversationId()));

                return $this->saveGatewayTriggerResult($this->gatewayServiceUpdate, GatewayTriggerType::SET_BUTTON_TITLE, false);
            }

            if ($this->triggerTypeIndex == GatewayTriggerType::SET_BUTTON_TITLE) {
                $this->_validateInput();
                $this->dispatch(new TelegramSetButtonTitleTriggerResponse($this->gatewayTriggerType, $this->gatewayServiceUpdate, $this->request));

                return $this->saveGatewayTriggerResult($this->gatewayServiceUpdate, $this->triggerTypeIndex, true);
            }

            if ($this->triggerTypeIndex == GatewayTriggerType::GET_BUTTON_LINK) {
                $this->dispatch(new TelegramGetButtonLinkTriggerResponse($this->gatewayTriggerType, $this->request->getConversationId()));

                return $this->saveGatewayTriggerResult($this->gatewayServiceUpdate, GatewayTriggerType::SET_BUTTON_LINK, false);
            }

            if ($this->triggerTypeIndex == GatewayTriggerType::SET_BUTTON_LINK) {
                $this->_validateInput();
                $this->dispatch(new TelegramSetButtonLinkTriggerResponse($this->gatewayTriggerType, $this->gatewayServiceUpdate, $this->request));

                return $this->saveGatewayTriggerResult($this->gatewayServiceUpdate, $this->triggerTypeIndex, true);
            }

            if ($this->triggerTypeIndex == GatewayTriggerType::GET_AUDIO_CUT_START) {
                $this->dispatch(new TelegramGetAudioCutStartTriggerResponse($this->gatewayTriggerType, $this->request->getConversationId()));

                return $this->saveGatewayTriggerResult($this->gatewayServiceUpdate, GatewayTriggerType::SET_AUDIO_CUT_START, false);
            }

            if ($this->triggerTypeIndex == GatewayTriggerType::SET_AUDIO_CUT_START) {
                $this->_validateInput();
                $this->dispatch(new TelegramSetAudioCutStartTriggerResponse($this->gatewayTriggerType, $this->gatewayServiceUpdate, $this->request));

                return $this->saveGatewayTriggerResult($this->gatewayServiceUpdate, $this->triggerTypeIndex, true);
            }

            if ($this->triggerTypeIndex == GatewayTriggerType::GET_AUDIO_CUT_DURATION) {
                $this->dispatch(new TelegramGetAudioCutDurationTriggerResponse($this->gatewayTriggerType, $this->request->getConversationId()));

                return $this->saveGatewayTriggerResult($this->gatewayServiceUpdate, GatewayTriggerType::SET_AUDIO_CUT_DURATION, false);
            }

            if ($this->triggerTypeIndex == GatewayTriggerType::SET_AUDIO_CUT_DURATION) {
                $this->_validateInput();
                $this->dispatch(new TelegramSetAudioCutDurationTriggerResponse($this->gatewayTriggerType, $this->gatewayServiceUpdate, $this->request));

                return $this->saveGatewayTriggerResult($this->gatewayServiceUpdate, $this->triggerTypeIndex, true);
            }

            if ($this->triggerTypeIndex == GatewayTriggerType::GET_AUDIO_PERFORM_TITLE) {
                $this->dispatch(new TelegramGetAudioPerformTitleTriggerResponse($this->gatewayTriggerType, $this->request->getConversationId()));

                return $this->saveGatewayTriggerResult($this->gatewayServiceUpdate, GatewayTriggerType::SET_AUDIO_PERFORM_TITLE, false);
            }

            if ($this->triggerTypeIndex == GatewayTriggerType::SET_AUDIO_PERFORM_TITLE) {
                $this->_validateInput();
                $this->dispatch(new TelegramSetAudioPerformTitleTriggerResponse($this->gatewayTriggerType, $this->gatewayServiceUpdate, $this->request));

                return $this->saveGatewayTriggerResult($this->gatewayServiceUpdate, $this->triggerTypeIndex, true);
            }

            if ($this->triggerTypeIndex == GatewayTriggerType::GET_AUDIO_PERFORM_PERFORMER) {
                $this->dispatch(new TelegramGetAudioPerformPerformerTriggerResponse($this->gatewayTriggerType, $this->request->getConversationId()));

                return $this->saveGatewayTriggerResult($this->gatewayServiceUpdate, GatewayTriggerType::SET_AUDIO_PERFORM_PERFORMER, false);
            }

            if ($this->triggerTypeIndex == GatewayTriggerType::SET_AUDIO_PERFORM_PERFORMER) {
                $this->_validateInput();
                $this->dispatch(new TelegramSetAudioPerformPerformerTriggerResponse($this->gatewayTriggerType, $this->gatewayServiceUpdate, $this->request));

                return $this->saveGatewayTriggerResult($this->gatewayServiceUpdate, $this->triggerTypeIndex, true);
            }

            if ($this->triggerTypeIndex == GatewayTriggerType::GET_AUDIO_COVER) {
                $this->dispatch(new TelegramGetAudioCoverTriggerResponse($this->gatewayTriggerType, $this->request->getConversationId()));

                return $this->saveGatewayTriggerResult($this->gatewayServiceUpdate, GatewayTriggerType::SET_AUDIO_COVER, false);
            }

            if ($this->triggerTypeIndex == GatewayTriggerType::SET_AUDIO_COVER) {
                $this->_validateInput();
                $this->dispatch(new TelegramSetAudioCoverTriggerResponse($this->gatewayTriggerType, $this->gatewayServiceUpdate, $this->request));

                return $this->saveGatewayTriggerResult($this->gatewayServiceUpdate, $this->triggerTypeIndex, true);
            }

            if ($this->triggerTypeIndex == GatewayTriggerType::GET_ATTACH_UPDATE) {
                $this->dispatch(new TelegramGetAttachUpdateTriggerResponse($this->gatewayTriggerType, $this->request->getConversationId()));

                return $this->saveGatewayTriggerResult($this->gatewayServiceUpdate, GatewayTriggerType::SET_ATTACH_UPDATE, false);
            }

            if ($this->triggerTypeIndex == GatewayTriggerType::SET_ATTACH_UPDATE) {
                $this->_validateInput();
                $this->dispatch(new TelegramSetAttachUpdateTriggerResponse($this->gatewayTriggerType, $this->gatewayServiceUpdate, $this->request));

                return $this->saveGatewayTriggerResult($this->gatewayServiceUpdate, $this->triggerTypeIndex, true);
            }
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * Validate inputs
     */
    private function _validateInput()
    {
        $this->processSkipTrigger($this->gatewayTriggerType, $this->request);

        if ($this->canSkip) {
            return true;
        }

        if ($this->triggerTypeIndex == GatewayTriggerType::SET_CAPTION) {
            if (!$this->request->getMessage()->isType(GatewayServiceUpdate::TEXT)) {
                $this->dispatch(new TelegramInputErrorResponse($this->gatewayServiceUpdate, $this->request->getConversationId()));
            }
        }

        if ($this->triggerTypeIndex == GatewayTriggerType::SET_SIGN) {
            if (!$this->request->getMessage()->isType(GatewayServiceUpdate::TEXT)) {
                $this->dispatch(new TelegramInputErrorResponse($this->gatewayServiceUpdate, $this->request->getConversationId()));
            }
        }

        if ($this->triggerTypeIndex == GatewayTriggerType::SET_TIME) {
            $format = (new GatewayTriggerConfig(GatewayTriggerType::SET_TIME))->data;
            if (!$this->request->getMessage()->isType(GatewayServiceUpdate::TEXT)) {
                $this->dispatch(new TelegramInputErrorResponse($this->gatewayServiceUpdate, $this->request->getConversationId(), Carbon::now()->tz('Asia/Tehran')->format($format)));
            }

            $d = DateTime::createFromFormat($format, $this->request->getBody());
            if (!($d && $d->format($format) === $this->request->getBody())) {
                $this->dispatch(new TelegramInputErrorResponse($this->gatewayServiceUpdate, $this->request->getConversationId(), Carbon::now()->tz('Asia/Tehran')->format($format)));
            }
        }

        if ($this->triggerTypeIndex == GatewayTriggerType::SET_DELETE_TIME) {
            $format = (new GatewayTriggerConfig(GatewayTriggerType::SET_DELETE_TIME))->data;
            if (!$this->request->getMessage()->isType(GatewayServiceUpdate::TEXT)) {
                $this->dispatch(new TelegramInputErrorResponse($this->gatewayServiceUpdate, $this->request->getConversationId(), Carbon::now()->tz('Asia/Tehran')->format($format)));
            }

            $d = DateTime::createFromFormat($format, $this->request->getBody());
            if (!($d && $d->format($format) === $this->request->getBody())) {
                $this->dispatch(new TelegramInputErrorResponse($this->gatewayServiceUpdate, $this->request->getConversationId(), Carbon::now()->tz('Asia/Tehran')->format($format)));
            }
        }

        if ($this->triggerTypeIndex == GatewayTriggerType::SET_BUTTON_TITLE) {
            if (!$this->request->getMessage()->isType(GatewayServiceUpdate::TEXT)) {
                $this->dispatch(new TelegramInputErrorResponse($this->gatewayServiceUpdate, $this->request->getConversationId()));
            }
        }

        if ($this->triggerTypeIndex == GatewayTriggerType::SET_BUTTON_LINK) {
            if (!$this->request->getMessage()->isType(GatewayServiceUpdate::TEXT)) {
                $this->dispatch(new TelegramInputErrorResponse($this->gatewayServiceUpdate, $this->request->getConversationId(), 'https://www.boloosh.com'));
            }

            if (!filter_var($this->request->getBody(), FILTER_VALIDATE_URL)) {
                $this->dispatch(new TelegramInputErrorResponse($this->gatewayServiceUpdate, $this->request->getConversationId(), 'https://www.boloosh.com'));
            }
        }

        if ($this->triggerTypeIndex == GatewayTriggerType::SET_AUDIO_CUT_DURATION) {
            $config = new GatewayTriggerConfig(GatewayTriggerType::SET_AUDIO_CUT_DURATION);
            if (!$this->request->getMessage()->isType(GatewayServiceUpdate::TEXT)) {
                $this->dispatch(new TelegramInputErrorResponse($this->gatewayServiceUpdate, $this->request->getConversationId(), 'Minimum: ' . $config->data[0]));
            }

            if (!is_numeric($this->request->getBody())) {
                $this->dispatch(new TelegramInputErrorResponse($this->gatewayServiceUpdate, $this->request->getConversationId(), 'Minimum: ' . $config->data[0]));
            }

            if ($this->request->getBody() < $config->data[0]) {
                $this->dispatch(new TelegramInputErrorResponse($this->gatewayServiceUpdate, $this->request->getConversationId(), 'Minimum: ' . $config->data[0]));
            }

            if ($this->request->getBody() > $config->data[1]) {
                $this->dispatch(new TelegramInputErrorResponse($this->gatewayServiceUpdate, $this->request->getConversationId(), 'Minimum: 15, Maximum: ' . $config->data[1]));
            }
        }

        if ($this->triggerTypeIndex == GatewayTriggerType::SET_AUDIO_CUT_START) {
            $config = (new GatewayTriggerConfig(GatewayTriggerType::SET_AUDIO_CUT_START))->data;
            if (!$this->request->getMessage()->isType(GatewayServiceUpdate::TEXT)) {
                $this->dispatch(new TelegramInputErrorResponse($this->gatewayServiceUpdate, $this->request->getConversationId(), 'Minimum: ' . $config));
            }

            if (!is_numeric($this->request->getBody())) {
                $this->dispatch(new TelegramInputErrorResponse($this->gatewayServiceUpdate, $this->request->getConversationId(), 'Minimum: ' . $config));
            }

            if ($this->request->getBody() < $config) {
                $this->dispatch(new TelegramInputErrorResponse($this->gatewayServiceUpdate, $this->request->getConversationId(), 'Minimum: ' . $config));
            }
        }

        if ($this->triggerTypeIndex == GatewayTriggerType::SET_AUDIO_PERFORM_TITLE) {
            if (!$this->request->getMessage()->isType(GatewayServiceUpdate::TEXT)) {
                $this->dispatch(new TelegramInputErrorResponse($this->gatewayServiceUpdate, $this->request->getConversationId(), 'title'));
            }
        }

        if ($this->triggerTypeIndex == GatewayTriggerType::SET_AUDIO_PERFORM_PERFORMER) {
            if (!$this->request->getMessage()->isType(GatewayServiceUpdate::TEXT)) {
                $this->dispatch(new TelegramInputErrorResponse($this->gatewayServiceUpdate, $this->request->getConversationId(), 'track'));
            }
        }

        if ($this->triggerTypeIndex == GatewayTriggerType::SET_AUDIO_COVER) {
            if (!$this->request->getMessage()->isType(GatewayServiceUpdate::PHOTO)) {
                $this->dispatch(new TelegramInputErrorResponse($this->gatewayServiceUpdate, $this->request->getConversationId()));
            }
        }

        if ($this->triggerTypeIndex == GatewayTriggerType::SET_ATTACH_UPDATE) {
            if (!$this->request->getMessage()->isType(GatewayServiceUpdate::TEXT)) {
                $this->dispatch(new TelegramInputErrorResponse($this->gatewayServiceUpdate, $this->request->getConversationId()));
            }
        }
    }
}