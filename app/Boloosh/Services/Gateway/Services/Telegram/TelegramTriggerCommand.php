<?php

namespace Boloosh\Services\Gateway\Services\Telegram;

use Boloosh\Core\Commands\BaseCommand;
use Boloosh\Infrastructures\Models\GatewayTriggerType;
use Boloosh\Services\Gateway\Services\IGatewayServiceUpdateParser;
use Boloosh\Services\Gateway\Services\IGatewayTriggerCommand;
use Boloosh\Services\SDK\Telegram\ITelegramService;
use Telegram\Bot\Keyboard\Keyboard;

class TelegramTriggerCommand extends BaseCommand implements IGatewayTriggerCommand
{
    const CONTENT = true;
    const CONTENT_EMPTY = false;

    public $body;

    public $status;

    /**
     * @param GatewayTriggerType $gatewayTriggerType
     * @param IGatewayServiceUpdateParser $request
     *
     * @return mixed|string
     */
    public function processBody(GatewayTriggerType $gatewayTriggerType, IGatewayServiceUpdateParser $request)
    {
        $this->body = $request->getBody();
        if ($gatewayTriggerType->can_skip && $request->getBody() == '/' . GatewayTriggerType::COMMAND_SKIP) {
            $this->body = '';

            $this->status = TelegramTriggerCommand::CONTENT_EMPTY;
        } else {
            $this->status = TelegramTriggerCommand::CONTENT;
        }

    }

    /**.36
     * @param GatewayTriggerType $gatewayTriggerType
     * @param ITelegramService $telegramService
     * @return mixed
     */
    public function processKeyboard(GatewayTriggerType $gatewayTriggerType, ITelegramService $telegramService)
    {
        if ($gatewayTriggerType->can_skip) {
            $keyboard = Keyboard::make()->inline()->row(Keyboard::inlineButton(['text' => GatewayTriggerType::COMMAND_SKIP, 'callback_data' => '/' . GatewayTriggerType::COMMAND_SKIP]));
        } else {
            $keyboard = $telegramService->detectKeyboard($gatewayTriggerType);
        }

        return $keyboard;
    }

    /**
     * @return bool
     */
    public function hasBody()
    {
        return $this->status == TelegramTriggerCommand::CONTENT;
    }
}