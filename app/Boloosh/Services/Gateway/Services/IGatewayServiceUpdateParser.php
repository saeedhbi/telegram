<?php

namespace Boloosh\Services\Gateway\Services;

use Telegram\Bot\Objects\Message;
use Telegram\Bot\Objects\Update;

interface IGatewayServiceUpdateParser
{

    /**
     * @return Update
     */
    public function getUpdate();

    /**
     * @return int
     */
    public function getUpdateId();

    /**
     * @return Message|mixed
     */
    public function getMessage();

    /**
     * @return int
     */
    public function getMessageId();

    /**
     * @return int
     */
    public function getConversationId();

    /**
     * @return int
     */
    public function getSenderId();

    /**
     * @return mixed
     */
    public function getSender();

    /**
     * @return mixed
     */
    public function getType();

    /**
     * @return mixed
     */
    public function getBody();

    /**
     * @return mixed
     */
    public function getPayloads();

    /**
     * @return mixed
     */
    public function getRequestId();

    /**
     * @return mixed
     */
    public function getCommandData();

    /**
     * @param $value
     *
     * @return mixed
     */
    public function setCommandData($value);
}