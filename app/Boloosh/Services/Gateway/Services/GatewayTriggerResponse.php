<?php

namespace Boloosh\Services\Gateway\Services;

use Boloosh\Core\Commands\GatewayTrigger\GetGatewayTriggerByGatewayServiceUpdateCommand;
use Boloosh\Core\Commands\GatewayTriggerResult\SaveGatewayTriggerResultIndexCommand;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTrigger;
use Boloosh\Infrastructures\Models\GatewayTriggerType;
use Illuminate\Foundation\Bus\DispatchesJobs;

class GatewayTriggerResponse
{
    use DispatchesJobs;

    public $canSkip = false;

    /**
     * @param GatewayTriggerType $gatewayTriggerType
     * @param IGatewayServiceUpdateParser $request
     */
    public function processSkipTrigger(GatewayTriggerType $gatewayTriggerType, IGatewayServiceUpdateParser $request)
    {
        if ($gatewayTriggerType->can_skip && $request->getBody() == '/' . GatewayTriggerType::COMMAND_SKIP) {
            $this->canSkip = true;
        }
    }

    /**
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     * @param $type
     * @param $shouldTrigger
     * @return mixed
     */
    public function saveGatewayTriggerResult(GatewayServiceUpdate $gatewayServiceUpdate, $type, $shouldTrigger)
    {
        try {
            /** @var GatewayTrigger $gatewayTrigger */
            $gatewayTrigger = $this->dispatch(new GetGatewayTriggerByGatewayServiceUpdateCommand($gatewayServiceUpdate));
            return $this->dispatch(new SaveGatewayTriggerResultIndexCommand($gatewayTrigger->id, $type, $gatewayServiceUpdate, $shouldTrigger));
        } catch (\Exception $ex) {
            throw new $ex;
        }
    }
}