<?php

namespace Boloosh\Services\Gateway\Services;

use Boloosh\Infrastructures\Models\GatewayService;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Infrastructures\Models\GatewayTriggerIndex;
use Illuminate\Support\Collection;

interface IGatewayService
{

    /**
     * @param $update
     *
     * @return IGatewayServiceUpdateParser
     */
    public function parseUpdate($update);

    /**
     * @param $request
     *
     * @return mixed
     */
    public function setRequest($request);

    /**
     * @return mixed
     */
    public function getRequest();

    /**
     * @return bool
     */
    public function shouldCheckAccess();

    /**
     * Create new request response
     *
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     *
     * @return mixed
     */
    public function createNewRequestResponse(GatewayServiceUpdate $gatewayServiceUpdate);

    /**
     *
     * @return mixed
     */
    public function setGatewayTriggerIndex();

    /**
     * @return mixed
     */
    public function isCommand();

    /**
     * @return mixed
     */
    public function getCommand();

    /**
     * @param null $triggerIndex
     * @return bool
     */
    public function trigger($triggerIndex = null);

    /**
     * @return mixed
     */
    public function getGatewayTriggerIndex();

    /**
     * Detect next request response on workflow
     *
     * @param GatewayTriggerIndex $gatewayTriggerIndex
     *
     * @return mixed
     */
    public function handleNextServiceResponse(GatewayTriggerIndex $gatewayTriggerIndex);

    /**
     * Detect there is locked request to update body
     *
     * @return boolean
     */
    public function hasAnyLockedRequest();

    /**
     * Retrieve locked request for conversation
     *
     * @return mixed
     */
    public function getLockedGatewayServiceUpdate();

    /**
     * @return mixed
     */
    public function createLockedExceptionResponse();

    /**
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     *
     * @return mixed
     */
    public function handleEndingRequestResponse(GatewayServiceUpdate $gatewayServiceUpdate);

    /**
     * @return mixed
     */
    public function getTriggerIndex();

    /**
     * @return mixed
     */
    public function checkServiceTriggerCommands();

    /**
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     * @return mixed
     */
    public function setGatewayServiceUpdate(GatewayServiceUpdate $gatewayServiceUpdate);

    /**
     * @param GatewayServiceUpdate $gatewayServiceUpdate
     * @param Collection $destinationData
     * @return mixed
     */
    public function publishUpdate(GatewayServiceUpdate $gatewayServiceUpdate, $destinationData);

    /**
     * @return mixed
     */
    public function checkGatewayServiceUpdateType();
}