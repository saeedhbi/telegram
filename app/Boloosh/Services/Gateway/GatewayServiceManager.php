<?php

namespace Boloosh\Services\Gateway;

use Boloosh\Infrastructures\Models\GatewayService;
use Boloosh\Services\Gateway\Services\Telegram\TelegramGatewayService;

class GatewayServiceManager
{

    /**
     * @var IGatewayServiceCaller
     */
    private $gatewayServiceCaller;


    /**
     * GatewayServiceManager constructor.
     *
     * @param IGatewayServiceCaller $gatewayServiceCaller
     */
    public function __construct(IGatewayServiceCaller $gatewayServiceCaller)
    {
        $this->gatewayServiceCaller = $gatewayServiceCaller;
    }


    /**
     * @param GatewayService $gatewayService
     *
     * @return mixed
     */
    public function getGatewayService(GatewayService $gatewayService)
    {
        if ($gatewayService->id == GatewayService::TELEGRAM) {
            /** @var TelegramGatewayService */
            return $this->gatewayServiceCaller->telegram();
        }
    }
}