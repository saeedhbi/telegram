<?php

namespace Boloosh\Services\Gateway;

use Illuminate\Support\ServiceProvider;

class GatewayServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider
     */
    public function register()
    {
        $this->app->singleton(IGatewayServiceCaller::class, GatewayServiceCaller::class);
    }
}