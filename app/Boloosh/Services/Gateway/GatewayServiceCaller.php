<?php

namespace Boloosh\Services\Gateway;

use Boloosh\Services\Gateway\Services\Telegram\TelegramGatewayService;

class GatewayServiceCaller implements IGatewayServiceCaller
{

    /**
     * @return TelegramGatewayService
     */
    public function telegram()
    {
        /** @var TelegramGatewayService $caller */
        $caller = app(TelegramGatewayService::class);

        return $caller;
    }
}