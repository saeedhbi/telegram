<?php

namespace Boloosh\Services\Gateway;

interface IGatewayServiceCaller
{

    /**
     * @return mixed
     */
    public function telegram();
}