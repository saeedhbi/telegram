<?php

namespace Boloosh\Services\File;

use Boloosh\Services\File\Audio\IAudioFileService;

interface IFileService
{

    /**
     * Retrieve AudioFileService object
     *
     * @return IAudioFileService
     */
    public function audio();
}