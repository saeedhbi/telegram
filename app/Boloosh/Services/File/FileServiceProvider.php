<?php

namespace Boloosh\Services\File;

use Boloosh\Services\File\Audio\AudioFileService;
use Boloosh\Services\File\Audio\IAudioFileService;
use Illuminate\Support\ServiceProvider;

class FileServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider
     */
    public function register()
    {
        $this->app->singleton(IFileService::class, FileService::class);
        $this->app->singleton(IAudioFileService::class, AudioFileService::class);
    }
}