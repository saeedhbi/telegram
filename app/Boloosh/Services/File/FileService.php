<?php

namespace Boloosh\Services\File;

use Boloosh\Services\File\Audio\IAudioFileService;

class FileService implements IFileService
{

    /**
     * Retrieve AudioFileService object
     *
     * @return IAudioFileService
     */
    public function audio()
    {
        /** @var IAudioFileService $audioFileService */
        $audioFileService = app(IAudioFileService::class);

        return $audioFileService;
    }
}