<?php

namespace Boloosh\Services\File\Library;

use FFMpeg\Format\Video\Ogg;

class Voice extends Ogg
{
    public $audioFormat = '.mp3';

    public function __construct($audioCodec = 'libmp3lame', $videoCodec = 'libtheora')
    {
        $this
            ->setAudioCodec($audioCodec)
            ->setVideoCodec($videoCodec);
    }

    /**
     * {@inheritDoc}
     */
    public function supportBFrames()
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function getAvailableAudioCodecs()
    {
        return array('libopus', 'libavcodec', 'libmp3lame');
    }

    /**
     * {@inheritDoc}
     */
    public function getAvailableVideoCodecs()
    {
        return array('libtheora');
    }
}