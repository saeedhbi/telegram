<?php

namespace Boloosh\Services\File\Audio;

interface IAudioFileService
{

    /**
     * Retrieve extracted audio file
     *
     * @param $path
     * @param $from
     * @param $to
     * @return mixed
     */
    public function getExtract($path, $from, $to);

    /**
     * Retrieve ogg file from path
     *
     * @param $path
     * @param $from
     * @param $to
     * @return mixed
     */
    public function voice($path, $from, $to);

    /**
     * @param $path
     * @param $options
     * @return mixed
     */
    public function mp3($path, $options);
}