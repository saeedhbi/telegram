<?php

namespace Boloosh\Services\File\Audio;

use Boloosh\Exceptions\InvalidLogicException;
use Boloosh\Services\File\Library\Voice;
use FFMpeg\Coordinate\Dimension;
use FFMpeg\Coordinate\FrameRate;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\FFMpeg;
use FFMpeg\Format\Audio\Mp3;
use FFMpeg\Format\FormatInterface;
use FFMpeg\Media\Audio;

class AudioFileService implements IAudioFileService
{

    /**
     * Retrieve extracted audio file
     *
     * @param $path
     * @param $from
     * @param $to
     * @param null $type
     * @return string
     */
    public function getExtract($path, $from, $to, $type = null)
    {
        $file = $this->_createFile();

        $type = is_null($type) ? new Mp3() : $type;

        $audio = $file->open($path);

        if (!is_null($to)) {
            $audio->filters()->clip(TimeCode::fromSeconds($from), TimeCode::fromSeconds($to));
        }

        return $this->_save($audio, $type);
    }

    /**
     * @param $path
     * @param $from
     * @param $to
     *
     * @return mixed
     */
    public function voice($path, $from = 0, $to = null)
    {
        $type = new Voice();

        return $this->getExtract($path, $from, $to, $type);
    }

    /**
     * @param $path
     * @param $options
     *
     * @return mixed
     */
    public function mp3($path, $options)
    {
        $file = $this->_createFile();

        $type = new Mp3();

        $audio = $file->open($path);

        $audio->filters()->addMetadata($options);

        return $this->_save($audio, $type, $options);
    }

    /**
     * @return FFMpeg
     */
    private function _createFile()
    {
        return FFMpeg::create();
    }

    /**
     * @param Audio $file
     *
     * @param FormatInterface $format
     * @param null $options
     * @return string
     */
    private function _save($file, FormatInterface $format, $options = null)
    {
        $ex = isset($format->audioFormat) ? $format->audioFormat : '.mp3';

        if (!is_null($options)) {
            $filename = $options['artist'] . ' - ' . $options['title'] . $ex;
        } else {
            $filename = str_random(config('boloosh.file.name_length')) . config('boloosh.file.copyright') . $ex;
        }

        $name = public_path() . '/' . config('boloosh.file.path') . '/' . $filename;

        try {
            $file->save($format, $name);
        } catch (\Exception $ex) {
            throw new InvalidLogicException($ex->getMessage(), $ex->getLine(), null, $ex);
        }

        return config('boloosh.file.path') . '/' . $filename;
    }
}