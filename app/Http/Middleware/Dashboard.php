<?php

namespace App\Http\Middleware;

use Closure;

class Dashboard
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->registerSidebarMenu();

        return $next($request);
    }


    protected function registerSidebarMenu()
    {
        $menu = app('laravel_dashboard')->getSidebarMenu();

        $assetSubMenu = app('menu.manager')->createMenu('Custom assets')->addLink('Assets in the header', [ ], [ 'before' => '<i class="fa fa-circle-o"></i>' ])->addLink('Assets in the footer', [ ],
                [ 'before' => '<i class="fa fa-circle-o"></i>' ]);

        $cusSubMenu = app('menu.manager')->createMenu('View Customization')->addLink('General', [ ], [ 'before' => '<i class="fa fa-circle-o"></i>' ])->addLink('Logo', [ ],
                [ 'before' => '<i class="fa fa-circle-o"></i>' ])->addLink('Top bar', [ ], [ 'before' => '<i class="fa fa-circle-o"></i>' ])->addLink('Sidebar', [ ],
                [ 'before' => '<i class="fa fa-circle-o"></i>' ])->addLink('Control Sidebar', [ ], [ 'before' => '<i class="fa fa-circle-o"></i>' ])->addLink('Footer', [ ],
                [ 'before' => '<i class="fa fa-circle-o"></i>' ])->addSubMenu($assetSubMenu,
                [ 'before' => '<i class="fa fa-asterisk"></i>', 'url_def' => [ 'route_pattern' => 'customise.assets.*' ] ]);

        $menu->setLabel('Main Sidebar')->addLink('Introduction', [ ], [ 'before' => '<i class="fa fa-home"></i>' ])->addLink('Configuration', [ ],
                [ 'before' => '<i class="fa fa-cog"></i>' ])->addLink('Sidebar Menu', [ ], [ 'before' => '<i class="fa fa-book"></i>' ])->addLink('Alert Messages', [ ],
                [ 'before' => '<i class="fa fa-bell"></i>' ])->addLink('Breadcrumbs', [ ], [ 'before' => '<i class="fa fa-bookmark"></i>' ])->addSubMenu($cusSubMenu,
                [ 'before' => '<i class="fa fa-street-view"></i>', 'url_def' => [ 'route_pattern' => 'customise.*' ] ])->addLink('GitHub',
                [ 'to' => 'https://github.com/letrunghieu/laravel-dashboard' ], [ 'before' => '<i class="fa fa-github"></i>' ]);
    }
}