<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;

class PanelController extends Controller
{

    use DispatchesJobs;


    public function getPanelIndex()
    {
        return view('v1.panel.index');
    }
}
