<?php

namespace App\Http\Controllers;

use Boloosh\Core\Commands\Gateway\GetGatewayByTokenCommand;
use Boloosh\Core\Commands\GatewayService\GetGatewayServiceByIdCommand;
use Boloosh\Core\Commands\GatewayServiceUpdate\HandleGatewayServiceUpdateCommand;
use Boloosh\Core\Commands\GatewayServiceUpdate\HandleLockedGatewayServiceUpdate;
use Boloosh\Core\Commands\GatewayServiceUpdate\SaveUserGatewayServiceUpdateCommand;
use Boloosh\Core\Commands\GatewayTrigger\GetAllGatewayTriggersByIdCommand;
use Boloosh\Infrastructures\Models\GatewayServiceUpdate;
use Boloosh\Services\Cache\ICacheService;
use Boloosh\Services\Gateway\Services\IGatewayTriggerResponse;
use Illuminate\Http\Request;

class WebhookController extends Controller
{

    /**
     * @var ICacheService
     */
    private $cacheService;

    /**
     * WebhookController constructor.
     *
     * @param ICacheService $cacheService
     */
    public function __construct(ICacheService $cacheService)
    {
        $this->cacheService = $cacheService;
    }

    /**
     * Get webhook updates and call gateway service based on token
     *
     * @param Request $request
     * @param         $token
     *
     * @return mixed
     */
    public function getUpdates(Request $request, $token)
    {
        try {
            $gateway = $this->dispatch(new GetGatewayByTokenCommand($token));
            $gatewayService = $this->dispatch(new GetGatewayServiceByIdCommand($gateway->service_id));
            $gatewayTriggers = $this->dispatch(new GetAllGatewayTriggersByIdCommand($gateway->id));

            $this->cacheService->put('gateway', $gateway);

            $updateHandler = $this->dispatch(new HandleGatewayServiceUpdateCommand($gateway, $gatewayService, $gatewayTriggers, $request->all()));

            /*
             * Is there any locked request?
             */
            if ($updateHandler instanceof GatewayServiceUpdate) {
                $this->dispatch(new HandleLockedGatewayServiceUpdate($gateway, $updateHandler, $gatewayService, $request->all()));
            } else {
                /*
                 * Is there any command request?
                 */
                if ($updateHandler instanceof IGatewayTriggerResponse) {
                    /** @var IGatewayTriggerResponse $updateHandler */
                    $updateHandler->callTrigger();
                } else {
                    $this->dispatch(new SaveUserGatewayServiceUpdateCommand($gateway, $gatewayService, $request->all()));
                }
            }

        } catch (\Exception $ex) {
            //
        }
    }
}

