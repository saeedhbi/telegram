<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use Boloosh\Core\Commands\Gateway\ChangeUserGatewayStatusCommand;
use Boloosh\Core\Commands\Gateway\DeleteUserGatewayByIdCommand;
use Boloosh\Core\Commands\Gateway\SaveUserGatewayCommand;
use Boloosh\Core\Commands\Gateway\GetUserGatewaysCommand;
use Boloosh\Core\Commands\GatewayService\GetAllGatewayServicesCommand;
use Boloosh\Core\Commands\GatewayService\GetGatewayServiceByIdCommand;
use Boloosh\Exceptions\AppLogicException;
use Boloosh\Services\SDK\Telegram\ITelegramService;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Telegram\Bot\Api;

class GatewayController extends Controller
{

    use DispatchesJobs;


    /**
     * @param Request $request
     *
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPanelGatewayIndex(Request $request)
    {
        try {
            $gateways = $this->dispatch(new GetUserGatewaysCommand($request->user()));
        } catch (AppLogicException $ex) {
            return bAbort($ex->getFailResponse());
        }

        return view('v1.panel.gateway.index')->with(compact('gateways'));
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getNewPanelGateway()
    {
        try {
            $gatewayServices = $this->dispatch(new GetAllGatewayServicesCommand());
        } catch (AppLogicException $ex) {
            return bAbort($ex->getFailResponse());
        }

        return view('v1.panel.gateway.new')->with(compact('gatewayServices'));
    }


    /**
     * @param Request $request
     *
     * @return bool
     */
    public function postNewPanelGateway(Request $request)
    {
        $this->validate($request, [
            'name'    => 'required|max:255',
            'type'    => 'required|in:hook,request',
            'service' => 'required|int',
            'queue'   => 'boolean',
            'delay'   => 'string',
            'token'   => 'required',
        ]);

        try {
            $payload = [ ];

            $this->dispatch(new SaveUserGatewayCommand($request->user(), $request->get('type'), $request->get('service'), $request->get('name'), $request->get('queue'), $request->get('delay'),
                $request->get('token'), $payload));
        } catch (AppLogicException $ex) {
            return redirect()->back()->withErrors([ 'msg' => 'has error' ]);
        }

        return redirect()->route('panel-gateways');
    }


    /**
     * @param Request $request
     * @param         $id
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function deletePanelGateway(Request $request, $id)
    {
        try {
            $this->dispatch(new DeleteUserGatewayByIdCommand($request->user(), $id));
        } catch (AppLogicException $ex) {
            return redirect()->back()->withErrors([ 'msg' => 'has error' ]);
        }

        return redirect()->route('panel-gateways');
    }


    /**
     * @param Request $request
     * @param         $id
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function disablePanelGateway(Request $request, $id)
    {
        try {
            $this->dispatch(new ChangeUserGatewayStatusCommand($request->user(), $id, false));
        } catch (AppLogicException $ex) {
            return redirect()->back()->withErrors([ 'msg' => 'has error' ]);
        }

        return redirect()->route('panel-gateways');
    }


    /**
     * @param Request $request
     * @param         $id
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function enablePanelGateway(Request $request, $id)
    {
        try {
            $this->dispatch(new ChangeUserGatewayStatusCommand($request->user(), $id, true));
        } catch (AppLogicException $ex) {
            return redirect()->back()->withErrors([ 'msg' => 'has error' ]);
        }

        return redirect()->route('panel-gateways');
    }
}

