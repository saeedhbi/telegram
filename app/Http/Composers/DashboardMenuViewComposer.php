<?php namespace App\Http\Composers;

use Illuminate\Contracts\View\View;

class DashboardMenuViewComposer
{

    public function compose(View $view)
    {
        $menu = app('laravel_dashboard')->getSidebarMenu();

        $menu->setLabel('Main Sidebar')->addLink('Home', [ 'route' => 'panel-index' ], [ 'before' => '<i class="fa fa-home"></i>' ])->addLink('Gateways', [ 'route' => 'panel-gateways' ],
            [ 'before' => '<i class="fa fa-chrome"></i>' ]);

        $view->with(compact('menu'));
    }
}