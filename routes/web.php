<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('root');
});

Route::get('/redis', function () {

    $jobs = Redis::connection()->zrange('queues:default:delayed', 0, -1);

//    Redis::connection()->zrem('queues:default:delayed', $jobs[0]);

//    $jobs = Redis::connection()->zrange('queues:default:delayed', 0, -1);

    dd($jobs);
});

Route::get('auth/login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('auth/login', ['as' => 'login-post', 'uses' => 'Auth\LoginController@login']);
Route::get('auth/logout', 'Auth\LoginController@logout');

Route::group(['middleware' => 'auth'], function () {
    /*
     * Panel
     */
    Route::get('/panel', ['as' => 'panel-index', 'uses' => 'PanelController@getPanelIndex']);

    /*
     * Gateway
     */
    Route::get('/panel/gateways', ['as' => 'panel-gateways', 'uses' => 'Panel\GatewayController@getPanelGatewayIndex']);
    Route::get('/panel/gateways/new', ['as' => 'panel-gateways-new', 'uses' => 'Panel\GatewayController@getNewPanelGateway']);
    Route::post('/panel/gateways/new', ['as' => 'panel-gateways-new-post', 'uses' => 'Panel\GatewayController@postNewPanelGateway']);
    Route::get('/panel/gateways/{id}/disable', ['as' => 'panel-gateways-disable', 'uses' => 'Panel\GatewayController@disablePanelGateway']);
    Route::get('/panel/gateways/{id}/enable', ['as' => 'panel-gateways-enable', 'uses' => 'Panel\GatewayController@enablePanelGateway']);
    Route::get('/panel/gateways/{id}/delete', ['as' => 'panel-gateways-delete', 'uses' => 'Panel\GatewayController@deletePanelGateway']);
    Route::get('/panel/gateways/{id}/set', ['as' => 'panel-gateways-set', 'uses' => 'Panel\GatewayController@getSetWebhookGateway']);
});

