<?php
return [
    /**
     * Default paths.
     * In this case:
     *      Boloosh/Infrastructures/Interfaces
     *      Boloosh/Infrastructures/Repositories
     *      Boloosh/Infrastructures/Models
     */
    'paths'     => [
        'contract' => 'Boloosh/Infrastructures/Interfaces',
        'eloquent' => 'Boloosh/Infrastructures/Repositories',
        'model'    => 'Boloosh/Infrastructures/Models',
    ],
    /**
     * Configure the naming convention you wish for your repositories.
     *
     * Example: php artisan make:repository User
     *      - Contract: IUserRepository
     *      - Eloquent: UserRepositoryEloquent
     *      - Model: User
     */
    'fileNames' => [
        'contract' => 'I{name}Repository',
        'eloquent' => '{name}RepositoryEloquent',
        'model'    => '{name}',
    ],
];