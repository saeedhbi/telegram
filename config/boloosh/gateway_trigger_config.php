<?php

return [
    \Boloosh\Infrastructures\Models\GatewayTriggerType::SET_AUDIO_CUT_START => [
        'data' => 1
    ],
    \Boloosh\Infrastructures\Models\GatewayTriggerType::SET_AUDIO_CUT_DURATION => [
        'data' => [15, 60]
    ],
    \Boloosh\Infrastructures\Models\GatewayTriggerType::SET_TIME => [
        'data' => 'Y-m-d H:i:s'
    ],
    \Boloosh\Infrastructures\Models\GatewayTriggerType::SET_DELETE_TIME => [
        'data' => 'Y-m-d H:i:s'
    ],
];

