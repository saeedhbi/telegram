<?php

return [
    \Boloosh\Infrastructures\Models\GatewayTriggerType::PICK_GATEWAY_TRIGGER => [
        'type' => \Boloosh\Infrastructures\Models\GatewayTriggerType::PICK_GATEWAY_TRIGGER,
        'indexes' => [
            \Boloosh\Infrastructures\Models\GatewayTriggerType::PICK_GATEWAY_TRIGGER
        ]
    ],
    \Boloosh\Infrastructures\Models\GatewayTriggerType::CANCEL => [
        'type' => \Boloosh\Infrastructures\Models\GatewayTriggerType::CANCEL,
        'indexes' => [
            \Boloosh\Infrastructures\Models\GatewayTriggerType::SET_CANCEL
        ]
    ],
    \Boloosh\Infrastructures\Models\GatewayTriggerType::DESTINATION => [
        'type' => \Boloosh\Infrastructures\Models\GatewayTriggerType::DESTINATION,
        'indexes' => [
            \Boloosh\Infrastructures\Models\GatewayTriggerType::GET_DESTINATION, \Boloosh\Infrastructures\Models\GatewayTriggerType::SET_DESTINATION
        ]
    ],
    \Boloosh\Infrastructures\Models\GatewayTriggerType::CAPTION => [
        'type' => \Boloosh\Infrastructures\Models\GatewayTriggerType::CAPTION,
        'indexes' => [
            \Boloosh\Infrastructures\Models\GatewayTriggerType::GET_CAPTION, \Boloosh\Infrastructures\Models\GatewayTriggerType::SET_CAPTION
        ]
    ],
    \Boloosh\Infrastructures\Models\GatewayTriggerType::SIGN => [
        'type' => \Boloosh\Infrastructures\Models\GatewayTriggerType::SIGN,
        'indexes' => [
            \Boloosh\Infrastructures\Models\GatewayTriggerType::GET_SIGN, \Boloosh\Infrastructures\Models\GatewayTriggerType::SET_SIGN
        ]
    ],
    \Boloosh\Infrastructures\Models\GatewayTriggerType::BUTTON => [
        'type' => \Boloosh\Infrastructures\Models\GatewayTriggerType::BUTTON,
        'indexes' => [
            \Boloosh\Infrastructures\Models\GatewayTriggerType::GET_BUTTON_TITLE, \Boloosh\Infrastructures\Models\GatewayTriggerType::SET_BUTTON_TITLE, \Boloosh\Infrastructures\Models\GatewayTriggerType::GET_BUTTON_LINK, \Boloosh\Infrastructures\Models\GatewayTriggerType::SET_BUTTON_LINK,
        ]
    ],
    \Boloosh\Infrastructures\Models\GatewayTriggerType::TIME => [
        'type' => \Boloosh\Infrastructures\Models\GatewayTriggerType::TIME,
        'indexes' => [
            \Boloosh\Infrastructures\Models\GatewayTriggerType::GET_TIME, \Boloosh\Infrastructures\Models\GatewayTriggerType::SET_TIME
        ]
    ],
    \Boloosh\Infrastructures\Models\GatewayTriggerType::DELETE_TIME => [
        'type' => \Boloosh\Infrastructures\Models\GatewayTriggerType::DELETE_TIME,
        'indexes' => [
            \Boloosh\Infrastructures\Models\GatewayTriggerType::GET_DELETE_TIME, \Boloosh\Infrastructures\Models\GatewayTriggerType::SET_DELETE_TIME
        ]
    ],
    \Boloosh\Infrastructures\Models\GatewayTriggerType::AUDIO_PERFORM => [
        'type' => \Boloosh\Infrastructures\Models\GatewayTriggerType::AUDIO_PERFORM,
        'indexes' => [
            \Boloosh\Infrastructures\Models\GatewayTriggerType::GET_AUDIO_PERFORM_TITLE, \Boloosh\Infrastructures\Models\GatewayTriggerType::SET_AUDIO_PERFORM_TITLE, \Boloosh\Infrastructures\Models\GatewayTriggerType::GET_AUDIO_PERFORM_PERFORMER, \Boloosh\Infrastructures\Models\GatewayTriggerType::SET_AUDIO_PERFORM_PERFORMER,
        ]
    ],
    \Boloosh\Infrastructures\Models\GatewayTriggerType::AUDIO_CUT => [
        'type' => \Boloosh\Infrastructures\Models\GatewayTriggerType::AUDIO_CUT,
        'indexes' => [
            \Boloosh\Infrastructures\Models\GatewayTriggerType::GET_AUDIO_CUT_START, \Boloosh\Infrastructures\Models\GatewayTriggerType::SET_AUDIO_CUT_START, \Boloosh\Infrastructures\Models\GatewayTriggerType::GET_AUDIO_CUT_DURATION, \Boloosh\Infrastructures\Models\GatewayTriggerType::SET_AUDIO_CUT_DURATION
        ]
    ],
    \Boloosh\Infrastructures\Models\GatewayTriggerType::AUDIO_COVER => [
        'type' => \Boloosh\Infrastructures\Models\GatewayTriggerType::AUDIO_COVER,
        'indexes' => [
            \Boloosh\Infrastructures\Models\GatewayTriggerType::GET_AUDIO_COVER, \Boloosh\Infrastructures\Models\GatewayTriggerType::SET_AUDIO_COVER
        ]
    ],
    \Boloosh\Infrastructures\Models\GatewayTriggerType::ATTACH_UPDATE => [
        'type' => \Boloosh\Infrastructures\Models\GatewayTriggerType::ATTACH_UPDATE,
        'indexes' => [
            \Boloosh\Infrastructures\Models\GatewayTriggerType::GET_ATTACH_UPDATE, \Boloosh\Infrastructures\Models\GatewayTriggerType::SET_ATTACH_UPDATE
        ]
    ],
];