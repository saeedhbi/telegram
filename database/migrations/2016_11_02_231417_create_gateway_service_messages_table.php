<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGatewayServiceMessagesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gateway_service_updates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gateway_id')->unsigned();
            $table->integer('service_id')->unsigned();
            $table->bigInteger('message_id');
            $table->bigInteger('conversation_id');
            $table->integer('from_id')->unsigned();
            $table->integer('type')->unsigned()->default(10);
            $table->text('body');
            $table->longText('payloads');
            $table->timestamps();

            $table->foreign('service_id')->references('id')->on('gateway_services')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('gateway_id')->references('id')->on('gateways')->onUpdate('cascade')->onDelete('cascade');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gateway_service_updates');
    }
}
