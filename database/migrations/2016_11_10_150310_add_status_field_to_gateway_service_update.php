<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusFieldToGatewayServiceUpdate extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gateway_service_updates', function (Blueprint $table) {
            $table->tinyInteger('status')->unsigned()->default(1)->after('payloads');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gateway_service_updates', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
