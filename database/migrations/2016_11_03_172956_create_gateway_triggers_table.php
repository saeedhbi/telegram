<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGatewayTriggersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gateway_triggers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gateway_id')->unsigned();
            $table->string('name');
            $table->integer('order')->unsigned();
            $table->integer('type')->unsigned();
            $table->integer('status')->unsigned();
            $table->timestamps();

            $table->foreign('gateway_id')->references('id')->on('gateways')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('type')->references('id')->on('gateway_trigger_types')->onUpdate('cascade')->onDelete('cascade');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gateway_triggers');
    }
}
