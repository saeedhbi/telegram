<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGatewayTriggerResultsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gateway_trigger_results', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gateway_id')->unsigned();
            $table->integer('trigger_id')->unsigned();
            $table->integer('update_id')->unsigned();
            $table->longText('value')->nullable();
            $table->timestamps();

            $table->foreign('gateway_id')->references('id')->on('gateways')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('trigger_id')->references('id')->on('gateway_triggers')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('update_id')->references('id')->on('gateway_service_updates')->onUpdate('cascade')->onDelete('cascade');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gateway_trigger_results');
    }
}
