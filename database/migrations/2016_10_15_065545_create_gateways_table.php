<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGatewaysTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        Schema::create('gateways', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('service_id')->unsigned();
            $table->integer('destination_id')->unsigned();
            $table->string('name');
            $table->tinyInteger('type')->unsigned()->default(1);
            $table->tinyInteger('queue')->unsigned()->default(0);
            $table->string('delay')->nullable();
            $table->longText('payload')->nullable();
            $table->tinyInteger('status')->default(10);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('service_id')->references('id')->on('gateway_services')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('destination_id')->references('id')->on('gateway_services')->onUpdate('cascade')->onDelete('cascade');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gateways');
    }
}
