<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGatewayTriggerTypesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gateway_trigger_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key');
            $table->smallInteger('has_keyboard')->unsigned()->default(0);
            $table->integer('keyboard_type');
            $table->longText('keyboard_layout');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gateway_trigger_types');
    }
}
